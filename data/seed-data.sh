#!/bin/sh

export PGPASSWORD=$POSTGRES_PASSWORD

mkdir qpv
unzip qpv.zip -d qpv
(cd qpv && shp2pgsql -a -I QP_METROPOLEOUTREMER_WGS84_EPSG4326.shp > ../qpv.sql)

psql -h $POSTGRES_HOST -p $POSTGRES_PORT -U $POSTGRES_USERNAME -d $POSTGRES_DB -c "truncate qp_metropoleoutremer_wgs84_epsg4326"
psql -h $POSTGRES_HOST -p $POSTGRES_PORT -U $POSTGRES_USERNAME -d $POSTGRES_DB < "./qpv.sql"

psql -h $POSTGRES_HOST -p $POSTGRES_PORT -U $POSTGRES_USERNAME -d $POSTGRES_DB -c "truncate epci"
psql -h $POSTGRES_HOST -p $POSTGRES_PORT -U $POSTGRES_USERNAME -d $POSTGRES_DB -c "\copy epci FROM 'epci.csv' DELIMITER ',' CSV HEADER"

psql -h $POSTGRES_HOST -p $POSTGRES_PORT -U $POSTGRES_USERNAME -d $POSTGRES_DB -c "truncate commune"
psql -h $POSTGRES_HOST -p $POSTGRES_PORT -U $POSTGRES_USERNAME -d $POSTGRES_DB -c "\copy commune FROM 'commune.csv' DELIMITER ',' CSV HEADER"

psql -h $POSTGRES_HOST -p $POSTGRES_PORT -U $POSTGRES_USERNAME -d $POSTGRES_DB -c "truncate naf"
psql -h $POSTGRES_HOST -p $POSTGRES_PORT -U $POSTGRES_USERNAME -d $POSTGRES_DB -c "\copy naf FROM 'naf2008-listes-completes-5-niveaux.csv' DELIMITER ',' CSV HEADER"