-- TERRITOIRE
insert into
    territory (territory_type, name, epci_id, commune_id)
values
    ('CC', 'CC du Diois', '242600534', null)
on conflict do nothing;

insert into
    territory (territory_type, name, epci_id, commune_id)
values
    (
        'ME',
        'Métropole du Grand Paris',
        '200054781',
        null
    )
on conflict do nothing;

-- ARMELLE
insert into
    account (email)
values
    ('armelle.brichard@beta.gouv.fr');

insert into
    deveco (account_id, territory_id)
values
    (
        (
            SELECT
                id
            from
                account
            where
                email = 'armelle.brichard@beta.gouv.fr'
        ),
        (
            SELECT
                id
            from
                territory
            where
                epci_id = '200054781'
        )
    );

-- LEA
insert into
    account (email)
values
    ('lea.gislais@anct.gouv.fr');

insert into
    deveco (account_id, territory_id)
values
    (
        (
            SELECT
                id
            from
                account
            where
                email = 'lea.gislais@anct.gouv.fr'
        ),
        (
            SELECT
                id
            from
                territory
            where
                epci_id = '200054781'
        )
    );

-- VERONIQUE
insert into
    account (email)
values
    ('veronique.lacoma@beta.gouv.fr');

insert into
    deveco (account_id, territory_id)
values
    (
        (
            SELECT
                id
            from
                account
            where
                email = 'veronique.lacoma@beta.gouv.fr'
        ),
        (
            SELECT
                id
            from
                territory
            where
                epci_id = '200054781'
        )
    );

-- JEAN-BAPTISTE
insert into
    account (email)
values
    ('jean-baptiste.fernandes@anct.gouv.fr');

insert into
    deveco (account_id, territory_id)
values
    (
        (
            SELECT
                id
            from
                account
            where
                email = 'jean-baptiste.fernandes@anct.gouv.fr'
        ),
        (
            SELECT
                id
            from
                territory
            where
                epci_id = '200054781'
        )
    );

-- KAREL
insert into
    account (email)
values
    ('karel.cloarec@beta.gouv.fr');

insert into
    deveco (account_id, territory_id)
values
    (
        (
            SELECT
                id
            from
                account
            where
                email = 'karel.cloarec@beta.gouv.fr'
        ),
        (
            SELECT
                id
            from
                territory
            where
                epci_id = '200054781'
        )
    );

-- DEVECO
insert into
    account (email)
values
    ('deveco@ccd.fr');

insert into
    deveco (account_id, territory_id)
values
    (
        (
            SELECT
                id
            from
                account
            where
                email = 'deveco@ccd.fr'
        ),
        (
            SELECT
                id
            from
                territory
            where
                name = 'CC du Diois'
        )
    );

-- SUPERADMIN
insert into
    account (email, account_type)
values
    ('deveco@beta.gouv.fr', 'superadmin');
