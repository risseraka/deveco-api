import * as cron from 'node-cron';

import { exportActivityLogsAndSendEmails } from './controller/ExportExcelController';

export function launchCron() {
	console.log('starting cronjob');
	// send logs activity reports every monday night at 1h00
	cron.schedule('0 1 * * 1', () => {
		exportActivityLogsAndSendEmails();
	});
}
