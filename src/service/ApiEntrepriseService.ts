import _fetch from 'cross-fetch';

import { httpGet } from '../lib/httpRequest';
import { isSiren } from '../lib/regexp';

// function subtractMonths(numOfMonths: number, date = new Date()): Date {
//   date.setMonth(date.getMonth() - numOfMonths);
//
//   return date;
// }

export type ApiEntrepriseEntreprise = {
	entreprise: {
		siren: string;
		capital_social: number;
		numero_tva_intracommunautaire: string;
		forme_juridique: string;
		forme_juridique_code: string;
		nom_commercial: string;
		procedure_collective: boolean;
		enseigne: string;
		libelle_naf_entreprise: string;
		naf_entreprise: string;
		raison_sociale: string;
		siret_siege_social: string;
		code_effectif_entreprise: string;
		date_creation: number;
		nom: string;
		prenom: string;
		date_radiation: string;
		categorie_entreprise: string;
		tranche_effectif_salarie_entreprise: {
			de: string;
			a: string;
			code: string;
			date_reference: string;
			intitule: string;
		};
		mandataires_sociaux: [
			{
				nom: string;
				prenom: string;
				fonction: string;
				date_naissance: string;
				date_naissance_timestamp: number;
				dirigeant: boolean;
				raison_sociale: string;
				identifiant: string;
				type: string;
			}
		];
		etat_administratif: {
			value: string;
			date_cessation: number;
		};
	};
	etablissement_siege: {
		siege_social: boolean;
		siret: string;
		naf: string;
		libelle_naf: string;
		date_mise_a_jour: number;
		tranche_effectif_salarie_etablissement: {
			de: string;
			a: string;
			code: string;
			date_reference: number;
			intitule: string;
		};
		date_creation_etablissement: number;
		region_implantation: {
			code: string;
			value: string;
		};
		commune_implantation: {
			code: string;
			value: string;
		};
		pays_implantation: {
			code: string;
			value: string;
		};
		diffusable_commercialement: boolean;
		enseigne: string;
		adresse: {
			l1: string;
			l2: string;
			l3: string;
			l4: string;
			l5: string;
			l6: string;
			l7: string;
			numero_voie: string;
			type_voie: string;
			nom_voie: string;
			complement_adresse: string;
			code_postal: string;
			localite: string;
			code_insee_localite: string;
			cedex: string;
		};
		etat_administratif: {
			value: string;
			date_fermeture: number;
		};
	};
};

export type ApiEntrepriseEtablissement = {
	etablissement: {
		tranche_effectif_salarie_etablissement: {
			de: number;
			a: number;
			code: string;
			date_reference: string;
			intitule: string;
		};
	};
};

export type ApiEntrepriseEffectifs = {
	siret: string;
	annee: string;
	mois: string;
	effectifs_mensuels: string;
};

export type ApiEntrepriseExercice = {
	ca: string;
	date_fin_exercice_timestamp: number;
};

class ApiEntrepriseService {
	private static instance: ApiEntrepriseService;
	private apiEntrepriseEndPoint: string;
	private apiEntrepriseToken: string;

	private constructor() {
		this.apiEntrepriseEndPoint = 'https://entreprise.api.gouv.fr';
		this.apiEntrepriseToken = process.env.API_ENTREPRISE_TOKEN || '';
	}

	public static getInstance(): ApiEntrepriseService {
		if (!ApiEntrepriseService.instance) {
			ApiEntrepriseService.instance = new ApiEntrepriseService();
		}

		return ApiEntrepriseService.instance;
	}

	public async getEntreprise(siret: string): Promise<
		ApiEntrepriseEntreprise & {
			exercices: ApiEntrepriseExercice[];
		}
	> {
		const siren = siret.substring(0, 9);
		if (!isSiren(siren)) {
			throw new Error(`${siren} is not a valid SIREN.`);
		}

		const urlApiEntrepriseEntreprise = `${this.apiEntrepriseEndPoint}/v2/entreprises/${siren}?context=deveco&recipient=deveco&object=deveco`;
		const headers = {
			Authorization: `Bearer ${this.apiEntrepriseToken}`,
		};

		const apiEntrepriseEntreprise = await (<Promise<ApiEntrepriseEntreprise>>(
			httpGet(urlApiEntrepriseEntreprise, headers)
		));

		const exercices = await this.getExercicesForSiret(siret);

		return { ...apiEntrepriseEntreprise, exercices };
	}

	public async getExercicesForSiret(siret: string): Promise<ApiEntrepriseExercice[]> {
		const headers = {
			Authorization: `Bearer ${this.apiEntrepriseToken}`,
		};
		const urlApiEntrepriseExercices = `${this.apiEntrepriseEndPoint}/v2/exercices/${siret}?context=deveco&recipient=deveco&object=deveco`;

		try {
			const response = await (<Promise<{ error?: 'KO'; exercices?: ApiEntrepriseExercice[] }>>(
				httpGet(urlApiEntrepriseExercices, headers)
			));
			if (response.exercices) {
				return response.exercices;
			}
		} catch (e) {
			console.log(`Could not retrieve exercices for entreprise with SIRET ${siret}`);
		}

		return [];
	}
}

const apiEntrepriseService = ApiEntrepriseService.getInstance();

export default apiEntrepriseService;
