import AppDataSource from '../data-source';
import { Qpv } from '../entity/Qpv';

class QpvService {
	private static instance: QpvService;

	public static getInstance(): QpvService {
		if (!QpvService.instance) {
			QpvService.instance = new QpvService();
		}

		return QpvService.instance;
	}

	public async getQpv(longitude: number, latitude: number): Promise<Qpv | null> {
		const qpv = await AppDataSource.createQueryBuilder()
			.select('qpv')
			.from(Qpv, 'qpv')
			.where('ST_Contains(geom, ST_POINT(:longitude, :latitude))', { longitude, latitude })
			.getOne();
		return qpv;
	}
}

const qpvService = QpvService.getInstance();

export default qpvService;
