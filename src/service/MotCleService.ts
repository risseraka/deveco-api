import AppDataSource from '../data-source';
import { MotCle } from '../entity/MotCle';
import { Territory } from '../entity/Territory';

class MotCleService {
	private static instance: MotCleService;

	public static getInstance(): MotCleService {
		if (!MotCleService.instance) {
			MotCleService.instance = new MotCleService();
		}

		return MotCleService.instance;
	}

	public async createIfNecessary(motCles: string[], territory: Territory) {
		if (motCles.length <= 0) {
			return;
		}

		const repository = AppDataSource.getRepository(MotCle);
		const mots = motCles.map((mot) => mot.toLocaleLowerCase());

		const query = AppDataSource.createQueryBuilder()
			.select('motCle')
			.from(MotCle, 'motCle')
			.where('LOWER(motCle.motCle) IN (:...mots)', {
				mots: mots,
			})
			.andWhere('territory_id = :territoryId', { territoryId: territory.id });
		const existingMotCles = await query.getMany();

		for (const motCle of motCles) {
			if (
				!existingMotCles
					.map((existingmotCle) => existingmotCle.motCle.toLocaleLowerCase())
					.includes(motCle.toLocaleLowerCase())
			) {
				await repository.insert({ motCle, territory });
			}
		}
	}
}

const motCleService = MotCleService.getInstance();

export default motCleService;
