import inseeService from './InseeService';
import apiEntrepriseService from './ApiEntrepriseService';

import { Entreprise } from '../entity/Entreprise';
import AppDataSource from '../data-source';
import { isSiret } from '../lib/regexp';
import { Exercice } from '../entity/Exercice';
import { DevEco } from '../entity/DevEco';
import { Entite } from '../entity/Entite';
import { Contact } from '../entity/Contact';
import { Particulier } from '../entity/Particulier';

class EntrepriseService {
	private static instance: EntrepriseService;

	public static getInstance(): EntrepriseService {
		if (!EntrepriseService.instance) {
			EntrepriseService.instance = new EntrepriseService();
		}

		return EntrepriseService.instance;
	}

	public async getOrCreate(fuzzy: string, skipApiEntreprise?: boolean): Promise<Entreprise> {
		if (!isSiret(fuzzy)) {
			throw new Error(`${fuzzy} is not a valid SIRET.`);
		}
		const repository = AppDataSource.getRepository(Entreprise);
		const siret = fuzzy.replaceAll(/\s/g, '');
		const existing = await repository.findOneBy({ siret });
		if (existing) {
			return existing;
		}

		let apiEntrepriseEntreprise = null;
		if (!skipApiEntreprise) {
			apiEntrepriseEntreprise = await apiEntrepriseService.getEntreprise(siret);
		}

		let mandatairesSociaux;
		if (apiEntrepriseEntreprise && apiEntrepriseEntreprise.entreprise) {
			const formeJuridique = apiEntrepriseEntreprise.entreprise.forme_juridique;
			if (formeJuridique === 'Entrepreneur individuel') {
				mandatairesSociaux = [
					{
						nom: apiEntrepriseEntreprise.entreprise.nom,
						prenom: apiEntrepriseEntreprise.entreprise.prenom,
						fonction: 'Entrepreneur',
						type: 'PP',
					},
				];
			} else {
				mandatairesSociaux = apiEntrepriseEntreprise.entreprise.mandataires_sociaux;
			}
		}

		const res = await inseeService.searchBySiret(siret);

		if (!res.length) {
			throw new Error('Could not find SIRET');
		}

		const entreprise = await repository.create({
			...res[0],
			mandatairesSociaux,
		});
		const result = await repository.save(entreprise);

		const exercices = [];
		if (
			apiEntrepriseEntreprise &&
			apiEntrepriseEntreprise.entreprise &&
			apiEntrepriseEntreprise.exercices
		) {
			const exercicesRepository = AppDataSource.getRepository(Exercice);
			for (const ex of apiEntrepriseEntreprise.exercices) {
				const newExercice = exercicesRepository.create({
					entreprise: result,
					ca: parseInt(ex.ca),
					dateCloture: new Date(ex.date_fin_exercice_timestamp * 1000),
				});
				const exercice = await exercicesRepository.save(newExercice);
				exercices.push(exercice);
			}
		}

		return { ...result, exercices };
	}

	public async getOrCreateEntrepriseAndEntite({
		createur,
		siret,
		skipApiEntreprise,
	}: {
		createur: DevEco;
		siret: string;
		skipApiEntreprise?: boolean;
	}): Promise<Entite> {
		const territoire = createur.territory;
		const entiteRepository = AppDataSource.getRepository(Entite);
		const contactRepository = AppDataSource.getRepository(Contact);
		const particulierRepository = AppDataSource.getRepository(Particulier);

		const existingEntite = await entiteRepository.findOne({
			where: { entreprise: { siret }, territoire: { id: territoire.id } },
		});
		if (existingEntite) {
			return existingEntite;
		}

		const entreprise = await entrepriseService.getOrCreate(siret, skipApiEntreprise);
		const contacts: Contact[] = [];
		for (const ms of entreprise.mandatairesSociaux) {
			if (ms.type === 'PP') {
				const newParticulier = particulierRepository.create({
					nom: ms.nom,
					prenom: ms.prenom,
				});
				const particulier = await particulierRepository.save(newParticulier);
				const newContact = contactRepository.create({
					particulier,
					fonction: ms.fonction,
				});
				const contact = await contactRepository.save(newContact);
				contacts.push(contact);
			}
		}
		const newEntite = entiteRepository.create({
			territoire,
			entiteType: 'PM',
			entreprise,
			auteurModification: createur,
			contacts,
		});
		const entite = await entiteRepository.save(newEntite);

		return entite;
	}
}

const entrepriseService = EntrepriseService.getInstance();

export default entrepriseService;
