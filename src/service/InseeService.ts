import _fetch from 'cross-fetch';
import { DeepPartial } from 'typeorm';

import AppDataSource from '../data-source';
import { Entreprise } from '../entity/Entreprise';
import { Etablissement } from '../entity/Etablissement';
import { fullCodeToFormeJuridique } from '../lib/formeJuridique';
import { isSiret } from '../lib/regexp';

type Criteria = {
	recherche: string;
	ferme: string;
	codesNaf: string[];
	communes: string[];
	tranchesEffectifs: string[];
	formes: string;
	territoire: number;
	cp: string;
	rue: string;
};

class InseeApiService {
	private static instance: InseeApiService;

	public static getInstance(): InseeApiService {
		if (!InseeApiService.instance) {
			InseeApiService.instance = new InseeApiService();
		}

		return InseeApiService.instance;
	}

	public async searchBySiret(search: string): Promise<DeepPartial<Entreprise>[]> {
		const siret = search.replaceAll(/\s/g, '');
		if (!isSiret(siret)) {
			// TODO return actual error about invalid SIRET
			return [];
		}

		const etablissement = await AppDataSource.getRepository(Etablissement).findOne({
			where: {
				siret,
			},
		});

		if (!etablissement) {
			return [];
		}

		const {
			nomPublic,
			adresseComplete,
			trancheEffectifs,
			anneeEffectifs,
			codeNaf,
			libelleNaf,
			libelleCategorieNaf,
			etatAdministratif,
			longitude,
			latitude,
			codeCommune,
			dateCreation,
			categorieJuridique,
			enseigne,
		} = etablissement;

		const augmentedEtablissement: DeepPartial<Entreprise> = {
			siret,
			longitude,
			latitude,
			dateCreation,
			etatAdministratif,
			nom: nomPublic?.toLocaleUpperCase() ?? undefined,
			enseigne: enseigne?.toLocaleUpperCase() ?? undefined,
			adresse: adresseComplete?.toLocaleUpperCase() ?? undefined,
			codeCommune: codeCommune ?? undefined,
			codeNaf,
			libelleNaf,
			libelleCategorieNaf,
			effectifs: this.trancheToEffectifs(trancheEffectifs || ''),
			dateEffectifs: this.anneeToDateEffectifs(anneeEffectifs) ?? undefined,
			exercices: [],
			formeJuridique: fullCodeToFormeJuridique(`${categorieJuridique}`) ?? undefined,
		};

		return [augmentedEtablissement];
	}

	private trancheToEffectifs(tranche_effectifs: string): string {
		/*
		 * NN	Unités non employeuses (pas de salarié au cours de l'année de référence et pas d'effectif au 31/12). Cette tranche peut contenir quelques effectifs inconnus
		 * 00	0 salarié
		 * 01	1 ou 2 salariés
		 * 02	3 à 5 salariés
		 * 03	6 à 9 salariés
		 * 11	10 à 19 salariés
		 * 12	20 à 49 salariés
		 * 21	50 à 99 salariés
		 * 22	100 à 199 salariés
		 * 31	200 à 249 salariés
		 * 32	250 à 499 salariés
		 * 41	500 à 999 salariés
		 * 42	1 000 à 1 999 salariés
		 * 51	2 000 à 4 999 salariés
		 * 52	5 000 à 9 999 salariés
		 * 53	10 000 salariés et plus
		 */
		switch (tranche_effectifs) {
			case '00':
				return '0';
			case '01':
				return '1 ou 2';
			case '02':
				return '3 à 5';
			case '03':
				return '6 à 9';
			case '11':
				return '10 à 19';
			case '12':
				return '20 à 49';
			case '21':
				return '50 à 99';
			case '22':
				return '100 à 199';
			case '31':
				return '200 à 249';
			case '32':
				return '250 à 499';
			case '41':
				return '500 à 999';
			case '42':
				return '1 000 à 1 999';
			case '51':
				return '2 000 à 4 999';
			case '52':
				return '5 000 à 9 999';
			case '53':
				return '10 000 et plus';
			case 'NN':
			default:
				return '?';
		}
	}

	private anneeToDateEffectifs(annee_effectifs: string): Date | undefined {
		const annee = parseInt(annee_effectifs);
		if (annee > 2000 && annee < 2050) {
			return new Date(annee, 11, 25, 12, 0, 0, 0); // 25 so that it is always a day in the current month regardless of the timezone. Hackish, yeah.
		}
		return;
	}

	public async search(
		criteria: Criteria,
		page?: number,
		resultsPerPage?: number
	): Promise<[string[], number]> {
		const { recherche, ferme, codesNaf, communes, tranchesEffectifs, formes, territoire, cp, rue } =
			criteria;

		const conditions: string[] = [];

		if (recherche) {
			const or = [];
			or.push(`TER.tsv @@to_tsquery('french', '''${recherche}''')`);
			or.push(`TER.siret::varchar ILIKE '%${recherche}%'`);
			conditions.push('( ' + or.join(' OR ') + ' )');
		}

		if (ferme === 'oui') {
			conditions.push(`TER.etat_administratif = 'F'`);
		} else {
			conditions.push(`TER.etat_administratif = 'A'`);
		}

		if (codesNaf?.length) {
			const or = [];
			for (const codeNaf of codesNaf) {
				or.push(`TER.code_naf = '${codeNaf}'`);
			}
			conditions.push('( ' + or.join(' OR ') + ' )');
		}

		if (communes?.length) {
			const or = [];
			for (const commune of communes) {
				or.push(`TER.code_commune = '${commune}'`);
			}
			conditions.push('( ' + or.join(' OR ') + ' )');
		}

		if (tranchesEffectifs?.length) {
			const or = [];
			for (const trancheEffectifs of tranchesEffectifs) {
				or.push(`TER.tranche_effectifs = '${trancheEffectifs}'`);
			}
			conditions.push('( ' + or.join(' OR ') + ' )');
		}

		if (formes !== 'toutes') {
			conditions.push(`TER.categorie_juridique_courante`);
		}

		if (cp && rue) {
			conditions.push(
				// normalize string to remove accents for search (the data we use is normalized, it would seem)
				`TER.adresse_complete ILIKE '%${rue
					.replace("'", "''")
					.normalize('NFD')
					.replace(/\p{Diacritic}/gu, '')}%'`
			);
			conditions.push(`TER.adresse_complete ILIKE '%${cp}%'`);
		}

		const pagination = [];
		if (page && resultsPerPage) {
			const skip = (page - 1) * resultsPerPage;
			pagination.push(`OFFSET ${skip}`);
			pagination.push(`LIMIT ${resultsPerPage}`);
		}

		const joins = [`TER.territory_id = ${territoire}`];

		const baseQuery = [
			`FROM territory_etablissement_reference TER`,
			`WHERE ${joins.join(' AND ')}`,
			conditions.length ? `AND ${conditions.join(' AND ')}` : '',
		];

		const itemsQuery = [
			'SELECT TER.siret',
			...baseQuery,
			'ORDER BY TER.date_creation DESC NULLS LAST',
			...pagination,
		].join(' ');
		const countQuery = ['SELECT count(id)', ...baseQuery].join(' ');

		const items = await AppDataSource.query(itemsQuery);
		const count = await AppDataSource.query(countQuery);

		return [items.map((it: { siret: string }) => it.siret), Number(count[0].count)];
	}
}

const inseeApiService = InseeApiService.getInstance();

export default inseeApiService;
