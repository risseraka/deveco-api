import AppDataSource from '../data-source';
import { Account } from '../entity/Account';
import { DevEco } from '../entity/DevEco';

class DevEcoService {
	private static instance: DevEcoService;

	public static getInstance(): DevEcoService {
		if (!DevEcoService.instance) {
			DevEcoService.instance = new DevEcoService();
		}

		return DevEcoService.instance;
	}

	public getDevEcoByAccountWithTerritory(account: Account): Promise<DevEco> {
		return AppDataSource.getRepository(DevEco).findOneOrFail({
			where: { account: { id: account.id } },
			relations: { territory: true },
		});
	}
}

const devecoService = DevEcoService.getInstance();

export default devecoService;
