import AppDataSource from '../data-source';
import { ActiviteEntreprise } from '../entity/ActiviteEntreprise';
import { Territory } from '../entity/Territory';

class ActiviteEntrepriseService {
	private static instance: ActiviteEntrepriseService;

	public static getInstance(): ActiviteEntrepriseService {
		if (!ActiviteEntrepriseService.instance) {
			ActiviteEntrepriseService.instance = new ActiviteEntrepriseService();
		}

		return ActiviteEntrepriseService.instance;
	}

	public async createIfNecessary(activites: string[], territory: Territory) {
		if (activites.length <= 0) {
			return;
		}
		const repository = AppDataSource.getRepository(ActiviteEntreprise);
		const acts = activites.map((act) => act.toLocaleLowerCase());

		const query = AppDataSource.createQueryBuilder()
			.select('activite')
			.from(ActiviteEntreprise, 'activite')
			.where('LOWER(activite) IN (:...acts)', {
				acts,
			})
			.andWhere('territory_id = :territoryId', { territoryId: territory.id });
		const result = await query.getMany();

		for (const activite of activites) {
			if (
				!result.map((ae) => ae.activite.toLocaleLowerCase()).includes(activite.toLocaleLowerCase())
			) {
				await repository.insert({ activite, territory });
			}
		}
	}
}

const activiteEntrepriseService = ActiviteEntrepriseService.getInstance();

export default activiteEntrepriseService;
