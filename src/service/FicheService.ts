import { FindOptionsRelations, FindOptionsWhere } from 'typeorm';

import AppDataSource from '../data-source';
import { Demande } from '../entity/Demande';
import { Echange } from '../entity/Echange';
import { Fiche } from '../entity/Fiche';
import { FicheView, toView } from '../view/FicheView';

export type Conditions<T> = FindOptionsWhere<T>[] | FindOptionsWhere<T>;

const relationsLight: FindOptionsRelations<Fiche> = {
	entite: {
		entreprise: true,
		particulier: true,
	},
	createur: { account: true },
	auteurModification: { account: true },
};

const relationsFull: FindOptionsRelations<Fiche> = {
	...relationsLight,
	demandes: true,
	echanges: {
		auteurModification: {
			account: true,
		},
	},
	rappels: true,
	entite: {
		particulier: true,
		entreprise: {
			exercices: true,
		},
		contacts: {
			particulier: true,
		},
		proprietes: true,
	},
};

type Criteria = {
	recherche: string;
	type: string;
	activites: string[];
	zones: string[];
	demandes: string[];
	local: number;
	codesCommune: string[];
	territoire: number;
	mots: string[];
	cp: string;
	rue: string;
	ids?: string[];
};

class FicheService {
	private static instance: FicheService;

	public static getInstance(): FicheService {
		if (!FicheService.instance) {
			FicheService.instance = new FicheService();
		}

		return FicheService.instance;
	}

	public async loadFiche(where: Conditions<Fiche>, light?: boolean): Promise<Fiche> {
		const fiche = await AppDataSource.getRepository(Fiche).findOneOrFail({
			relations: light ? relationsLight : relationsFull,
			where,
		});

		return fiche;
	}

	public async ficheToView(where: Conditions<Fiche>, light?: boolean): Promise<FicheView> {
		const fiche = await this.loadFiche(where, light);
		return toView(fiche);
	}

	public async search(
		criteria: Criteria,
		page: number,
		resultsPerPage: number,
		limit?: number
	): Promise<[Fiche[], number]> {
		const {
			recherche,
			type,
			activites,
			zones,
			demandes,
			local,
			codesCommune,
			territoire,
			mots,
			cp,
			rue,
		} = criteria;
		const conditions: string[] = [];

		conditions.push(`entite.territoire_id = ${territoire}`);

		if (type === 'etablissement') {
			conditions.push(`entite.entite_type = 'PM'`);
		} else if (type === 'particulier') {
			conditions.push(`entite.entite_type = 'PP'`);
		}

		if (codesCommune?.length) {
			const or = [];
			for (const codeCommune of codesCommune) {
				or.push(`entreprise.code_commune = '${codeCommune}'`);
			}
			conditions.push('( ' + or.join(' OR ') + ' )');
		}

		if (recherche) {
			const or = [];
			// search in Entreprise: Nom, SIRET, Code NAF, Libellé NAF, Enseigne, and in any Contact's Nom
			or.push(`entreprise.nom ILIKE '%${recherche}%'`);
			or.push(`entreprise.siret ILIKE '%${recherche}%'`);
			or.push(`entreprise.code_naf ILIKE '%${recherche}%'`);
			or.push(`entreprise.libelle_naf ILIKE '%${recherche}%'`);
			or.push(`entreprise.enseigne ILIKE '%${recherche}%'`);
			or.push(`part.nom ILIKE '%${recherche}%'`);

			// search in Particulier: Nom and Future Enseigne
			or.push(`particulier.nom ILIKE '%${recherche}%'`);
			or.push(`entite.futureEnseigne ILIKE '%${recherche}%'`);
			conditions.push('( ' + or.join(' OR ') + ' )');
		}

		if (local) {
			conditions.push(`local.id IS DISTINCT FROM ${local}`);
		}

		if (activites?.length) {
			const or = [];
			for (const a of activites) {
				or.push(`entite.activites_reelles ilike '%${a}%'`);
			}
			conditions.push('( ' + or.join(' OR ') + ' )');
		}

		if (zones?.length) {
			const or = [];
			for (const z of zones) {
				or.push(`entreprise_localisations ilike '%${z}%'`);
			}
			conditions.push('( ' + or.join(' OR ') + ' )');
		}

		if (demandes?.length) {
			const or = [];
			for (const d of demandes) {
				or.push(`demande.type_demande ilike '%${d}%'`);
			}
			conditions.push(
				`((demande.cloture = false OR demande.cloture is null) AND (${or.join(' OR ')}))`
			);
		}

		if (mots?.length) {
			const or = [];
			for (const m of mots) {
				or.push(`mots_cles ilike '%${m}%'`);
			}
			conditions.push('( ' + or.join(' OR ') + ' )');
		}

		// normalize string to remove accents for search (the data we use is normalized, it would seem)
		const normalizedRue = rue
			.replace("'", ' ')
			.normalize('NFD')
			.replace(/\p{Diacritic}/gu, '');
		if (cp && rue) {
			const or = [];
			or.push(
				`entreprise.adresse ILIKE '%${normalizedRue}%' AND entreprise.adresse ILIKE '%${cp}%'`
			);
			or.push(
				`particulier.adresse ILIKE '%${normalizedRue}%' AND particulier.code_postal ILIKE '%${cp}%'`
			);
			conditions.push('( ' + or.join(' OR ') + ' )');
		}

		const query = AppDataSource.getRepository(Fiche)
			.createQueryBuilder('fiche')
			.leftJoinAndSelect('fiche.entite', 'entite')
			.leftJoinAndSelect('entite.contacts', 'contact')
			.leftJoinAndSelect('contact.particulier', 'part')
			.leftJoinAndSelect('fiche.demandes', 'demande')
			.leftJoinAndSelect('entite.entreprise', 'entreprise')
			.leftJoinAndSelect('entite.particulier', 'particulier')
			.leftJoinAndSelect('fiche.createur', 'createur')
			.leftJoinAndSelect('createur.account', 'createurAccount')
			.leftJoinAndSelect('fiche.auteurModification', 'auteurModification')
			.leftJoinAndSelect('auteurModification.account', 'auteurModificationAccount')
			.leftJoinAndSelect('entite.proprietes', 'propriete')
			.leftJoinAndSelect('propriete.local', 'local')
			.where(conditions.join(' AND '))
			.orderBy('fiche.dateModification', 'DESC');

		if (limit) {
			query.limit(limit);
		}

		const skip = (page - 1) * resultsPerPage;
		return query.skip(skip).take(resultsPerPage).getManyAndCount();
	}

	public async searchAll(criteria: Criteria, type: 'PM' | 'PP'): Promise<Fiche[]> {
		const { ids, recherche, activites, zones, demandes, local, codesCommune, territoire, cp, rue } =
			criteria;

		const conditions = [];

		conditions.push(`entite.territoire_id = ${territoire}`);

		conditions.push(`entite.entite_type = '${type}'`);

		if (ids?.length) {
			conditions.push(`fiche.id IN (${ids.join(',')})`);
		} else {
			if (codesCommune?.length) {
				const or = [];
				for (const codeCommune of codesCommune) {
					or.push(`entreprise.code_commune = '${codeCommune}'`);
				}
				conditions.push('( ' + or.join(' OR ') + ' )');
			}

			if (recherche) {
				const or = [];
				if (type === 'PM') {
					or.push(`entreprise.nom ILIKE '%${recherche}%'`);
					or.push(`entreprise.siret ILIKE '%${recherche}%'`);
					or.push(`entreprise.code_naf ILIKE '%${recherche}%'`);
					or.push(`entreprise.libelle_naf ILIKE '%${recherche}%'`);
				} else if (type === 'PP') {
					or.push(`particulier.nom ILIKE '%${recherche}%'`);
					or.push(`entite.futureEnseigne ILIKE '%${recherche}%'`);
					or.push(`part.nom ILIKE '%${recherche}%'`);
				}
				conditions.push('( ' + or.join(' OR ') + ' )');
			}

			if (local) {
				conditions.push(`local.id IS DISTINCT FROM ${local}`);
			}

			if (activites?.length) {
				const or = [];
				for (const a of activites) {
					or.push(`entite.activites_reelles ilike '%${a}%'`);
				}
				conditions.push('( ' + or.join(' OR ') + ' )');
			}

			if (zones?.length) {
				const or = [];
				for (const z of zones) {
					or.push(`entite.entreprise_localisations ilike '%${z}%'`);
				}
				conditions.push('( ' + or.join(' OR ') + ' )');
			}

			if (demandes?.length) {
				const or = [];
				for (const d of demandes) {
					or.push(`demande.type_demande ilike '%${d}%'`);
				}
				conditions.push(
					`((demande.cloture = false OR demande.cloture is null) AND (${or.join(' OR ')}))`
				);
			}

			if (cp && rue) {
				if (type === 'PM') {
					conditions.push(
						// normalize string to remove accents for search (the data we use is normalized, it would seem)
						`entreprise.adresse ILIKE '%${rue
							.replace("'", ' ')
							.normalize('NFD')
							.replace(/\p{Diacritic}/gu, '')}%'`
					);
					conditions.push(`entreprise.adresse ILIKE '%${cp}%'`);
				} else if (type === 'PP') {
					conditions.push(`particulier.adresse ILIKE '%${rue}%'`);
				}
			}
		}

		const fiches = await AppDataSource.getRepository(Fiche)
			.createQueryBuilder('fiche')
			.leftJoinAndSelect('fiche.entite', 'entite')
			.leftJoinAndSelect('entite.contacts', 'contact')
			.leftJoinAndSelect('contact.particulier', 'part')
			.leftJoinAndSelect('fiche.demandes', 'demande')
			.leftJoinAndSelect('entite.entreprise', 'entreprise')
			.leftJoinAndSelect('entite.particulier', 'particulier')
			.leftJoinAndSelect('entite.proprietes', 'propriete')
			.leftJoinAndSelect('propriete.local', 'local')
			.where(conditions.join(' AND '))
			.orderBy('fiche.dateModification', 'DESC')
			.getMany();
		return fiches;
	}

	public async createDemandesIfNecessary(themes: string[], ficheId: Fiche['id']) {
		if (themes.length <= 0) {
			return;
		}

		const repository = AppDataSource.getRepository(Demande);
		const demandes = themes.map((loc) => loc.toLocaleLowerCase().replace("'", "''"));

		const result = await AppDataSource.createQueryBuilder()
			.select('demande')
			.from(Demande, 'demande')
			.where('LOWER(type_demande) IN (:...demandes)', {
				demandes,
			})
			.andWhere('fiche_id = :ficheId', { ficheId })
			.getMany();

		for (const typeDemande of demandes) {
			if (
				!result
					.map((demande) => demande.typeDemande.toLocaleLowerCase())
					.includes(typeDemande.toLocaleLowerCase())
			) {
				await repository.insert({ typeDemande, fiche: { id: ficheId } });
			}
		}
	}

	public async removeDemandesIfNecessary(ficheId: Fiche['id']) {
		const demandes = await AppDataSource.getRepository(Demande).find({
			where: { fiche: { id: ficheId } },
		});
		const types = demandes.map(({ typeDemande }) => typeDemande);

		const echangesTypes = (
			await AppDataSource.getRepository(Echange).find({ where: { fiche: { id: ficheId } } })
		)
			.map((e) => e.themes)
			.reduce((acc, themes) => [...acc, ...themes], []);

		for (const type_ of types) {
			if (!echangesTypes.includes(type_)) {
				await AppDataSource.getRepository(Demande).delete({
					typeDemande: type_,
					fiche: { id: ficheId },
				});
			}
		}
	}
}

const ficheService = FicheService.getInstance();

export default ficheService;
