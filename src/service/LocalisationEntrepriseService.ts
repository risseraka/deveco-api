import AppDataSource from '../data-source';
import { LocalisationEntreprise } from '../entity/LocalisationEntreprise';
import { Territory } from '../entity/Territory';

class LocalisationEntrepriseService {
	private static instance: LocalisationEntrepriseService;

	public static getInstance(): LocalisationEntrepriseService {
		if (!LocalisationEntrepriseService.instance) {
			LocalisationEntrepriseService.instance = new LocalisationEntrepriseService();
		}

		return LocalisationEntrepriseService.instance;
	}

	public async createIfNecessary(localisations: string[], territory: Territory) {
		if (localisations.length <= 0) {
			return;
		}

		const repository = AppDataSource.getRepository(LocalisationEntreprise);
		const locs = localisations.map((loc) => loc.toLocaleLowerCase());

		const query = AppDataSource.createQueryBuilder()
			.select('localisation')
			.from(LocalisationEntreprise, 'localisation')
			.where('LOWER(localisation.localisation) IN (:...locs)', {
				locs,
			})
			.andWhere('territory_id = :territoryId', { territoryId: territory.id });
		const existingLocalisations = await query.getMany();

		for (const localisation of localisations) {
			if (
				!existingLocalisations
					.map((existingLocalisation) => existingLocalisation.localisation.toLocaleLowerCase())
					.includes(localisation.toLocaleLowerCase())
			) {
				await repository.insert({ localisation, territory });
			}
		}
	}
}

const localisationEntrepriseService = LocalisationEntrepriseService.getInstance();

export default localisationEntrepriseService;
