import { Account } from '../../entity/Account';
import { displayIdentity } from '../utils';

const helpUrl = 'https://deveco.notion.site/Bienvenue-sur-Deveco-64aa970576434b9dbc9299b4f2583005';

type Url = {
	appUrl: string;
	accessKey?: string;
	redirectUrl?: string;
};

function createLink({ appUrl, accessKey, redirectUrl }: Url) {
	const link = `${appUrl}${accessKey ? `/auth/jwt/${accessKey}` : ''}${
		redirectUrl ? `?url=${redirectUrl}` : ''
	}`;
	return link;
}

function createAccessButton(url: Url) {
	return `
    <p>Pour accéder à Deveco, veuillez cliquer sur le lien ci-dessous&nbsp;:</p>
    <p style="padding-left: 20%">
      <a
      rel="nofollow"
      href="${createLink(url)}"
      style="
          background-color: #000091;
          font-size: 14px;
          font-family: Marianne;
          font-weight: bold;
          text-decoration: none;
          padding: 8px 10px;
          color: #ffffff;
          display: inline-block;
        "
      >
      <span>Accédez à Deveco</span>
      </a>
    </p>
  `;
}

function createHelpLink(url: string | undefined) {
	return `<p>
            Vous rencontrez un problème&nbsp;? Vous vous demandez à quoi sert le bouton en haut à gauche&nbsp;? L'équipe deveco met à votre disposition un guide utilisateur à l'adresse suivante&nbsp;:
            <a
              rel="nofollow"
              href="${url}"
            >
              <span>lien vers le notion</span>
            </a>
          </p>`;
}

function greeting(account: Account): string {
	return `<p>
            Bonjour ${displayIdentity(account)},
          </p>`;
}

function greetingAnonyme(): string {
	return `<p>
            Bonjour,
          </p>`;
}

function remerciements() {
	return `<p>
            Merci d’utiliser deveco, la solution numérique pour les développeurs économiques.
          </p>`;
}

function contact(url: string | undefined) {
	return `<p>
            Et bien sûr, si l'information recherchée n'apparaît pas,
            n'hésitez pas à nous écrire&nbsp;: <a href="mailto:${url}">${url}</a>.
          </p>`;
}

function footer() {
	return `<p>
            L'équipe Deveco.
          </p>`;
}

export function loginRequest({ url, account }: { url: Url; account: Account }): string {
	return `
    ${greeting(account)}
    ${createAccessButton(url)}
    ${remerciements()}
    ${createHelpLink(helpUrl)}
    ${contact(process.env.SMTP_FROM)}
    ${footer()}
  `;
}

export function exportWeeklyActivity(): string {
	return `
    ${greetingAnonyme()}
    <p>Vous trouvez dans le PJ de ce mail un compte-rendu sur l'activité hebdomadaire des beta-testeurs deveco.</p>
    ${contact(process.env.SMTP_FROM)}
    ${footer()}
  `;
}

export const templates = {
	loginRequest,
	exportWeeklyActivity,
};

export default templates;
