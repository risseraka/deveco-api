import type Mail from 'nodemailer/lib/mailer';
import type { SentMessageInfo } from 'nodemailer/lib/smtp-transport';

import { send as sender } from './send';
import templates from './emails';

function send<K extends keyof typeof templates>({
	options,
	template,
	params,
}: {
	options: Mail.Options;
	template: K;
	params: Parameters<typeof templates[K]>;
}): Promise<SentMessageInfo> {
	if (!templates[template]) {
		throw new Error(
			`Invalid template name: ${template}. Available templates are: ${Object.keys(templates).join(
				', '
			)}.`
		);
	}
	const html = templates[template].apply(null, params as any);
	return sender({ ...options, html });
}

function isSmtp() {
	if (!process.env.SMTP_HOST || !process.env.SMTP_USER || !process.env.SMTP_PASS) {
		return false;
	}
	return true;
}

export { send, isSmtp };
