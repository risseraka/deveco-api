// type Code =
//   | 'NN'
//   | '00'
//   | '01'
//   | '02'
//   | '03'
//   | '11'
//   | '12'
//   | '21'
//   | '22'
//   | '31'
//   | '32'
//   | '41'
//   | '42'
//   | '51'
//   | '52'
//   | '53';
//
type Label =
	| 'Unité non employeuse'
	| 'Pas de salarié(e)'
	| '1 ou 2 salarié(e)s'
	| '3 à 5 salarié(e)s'
	| '6 à 9 salarié(e)s'
	| '10 à 19 salarié(e)s'
	| '20 à 49 salarié(e)s'
	| '50 à 99 salarié(e)s'
	| '100 à 199 salarié(e)s'
	| '200 à 249 salarié(e)s'
	| '250 à 499 salarié(e)s'
	| '500 à 999 salarié(e)s'
	| '1 000 à 1 999 salarié(e)s'
	| '2 000 à 4 999 salarié(e)s'
	| '5 000 à 9 999 salarié(e)s'
	| '10 000 salarié(e)s et plus';

const map: Record<string, Label> = {
	NN: 'Unité non employeuse',
	'00': 'Pas de salarié(e)',
	'01': '1 ou 2 salarié(e)s',
	'02': '3 à 5 salarié(e)s',
	'03': '6 à 9 salarié(e)s',
	'11': '10 à 19 salarié(e)s',
	'12': '20 à 49 salarié(e)s',
	'21': '50 à 99 salarié(e)s',
	'22': '100 à 199 salarié(e)s',
	'31': '200 à 249 salarié(e)s',
	'32': '250 à 499 salarié(e)s',
	'41': '500 à 999 salarié(e)s',
	'42': '1 000 à 1 999 salarié(e)s',
	'51': '2 000 à 4 999 salarié(e)s',
	'52': '5 000 à 9 999 salarié(e)s',
	'53': '10 000 salarié(e)s et plus',
};

export const codeToTrancheEffectifs = (code: string) => map[code] ?? undefined;
