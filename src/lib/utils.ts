import { Account } from '../entity/Account';
import { EventLogEntityType } from '../entity/EventLog';
import { Fiche } from '../entity/Fiche';

const monthNames = [
	'Janvier',
	'Février',
	'Mars',
	'Avril',
	'May',
	'Juin',
	'Juillet',
	'Aout',
	'Septembre',
	'Octobre',
	'Novembre',
	'Décembre',
];

export function displayIdentity(account: Account | undefined): string {
	if (!account) {
		return '';
	}
	const { email, prenom, nom } = account;
	if (prenom && nom) {
		return `${prenom} ${nom}`;
	}

	if (prenom) {
		return prenom;
	}

	if (nom) {
		return nom;
	}

	return email;
}

export function getLogEntityType(fiche: Fiche): EventLogEntityType {
	return fiche.entite?.particulier?.id
		? EventLogEntityType.PERSONNE_PHYSIQUE
		: EventLogEntityType.PERSONNE_MORALE;
}

export function getNWeeksAgo(today: Date, weeks: number) {
	const newDate = new Date(today.getTime() - 7 * 24 * 60 * 60 * 1000 * weeks);
	newDate.setHours(0, 0, 0, 0);
	return newDate;
}

export function getMonthAgo(today: Date) {
	const newDate = new Date(today.getFullYear(), today.getMonth() - 1, today.getDate());
	newDate.setHours(0, 0, 0, 0);
	return newDate;
}

export function formatJSDateToPostgresDate(d: Date) {
	const year = d.getFullYear();
	const month = d.getMonth() + 1 < 10 ? '0' + (d.getMonth() + 1) : d.getMonth() + 1;
	const day = d.getDate() < 10 ? '0' + d.getDate() : d.getDate();
	const hours = d.getHours() < 10 ? '0' + d.getHours() : d.getHours();
	const minutes = d.getMinutes() < 10 ? '0' + d.getMinutes() : d.getMinutes();
	const seconds = d.getSeconds() < 10 ? '0' + d.getSeconds() : d.getSeconds();
	const milliseconds = d.getMilliseconds() < 10 ? '0' + d.getMilliseconds() : d.getMilliseconds();
	return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}.${milliseconds}`;
}

export function formatJSDateToHumainFriendly(d: Date) {
	const year = d.getFullYear();
	const month = monthNames[d.getMonth()];
	const day = d.getDate();
	return `${day} ${month} ${year}`;
}
