import fetch from 'cross-fetch';

type ErrorResponse = { error: 'KO' };

export async function httpGet<Data>(
	url: string,
	headers?: Record<string, string>
): Promise<Data | ErrorResponse> {
	if (!headers) {
		headers = {
			Accept: 'application/json',
		};
	}
	const response = await fetch(url, {
		method: 'GET',
		headers,
	});

	if (!response.ok) {
		return {
			error: 'KO',
		};
	}
	const res = await response.json();
	return res;
}
