const regExpSiren = '^[0-9]{9}$';
const regExpSiret = '^[0-9]{14}$';

export function isSiret(fuzzy: string): boolean {
	const search = fuzzy.replaceAll(/\s/g, '');
	if (search.match(regExpSiret)) {
		return true;
	}
	return false;
}

export function isSiren(fuzzy: string): boolean {
	const search = fuzzy.replaceAll(/\s/g, '');
	if (search.match(regExpSiren)) {
		return true;
	}
	return false;
}
