import type { SignOptions } from 'jsonwebtoken';
import jwt from 'jsonwebtoken';

import { Account } from '../entity/Account';
import { Territory } from '../entity/Territory';
import devecoService from '../service/DevEcoService';

export type JwtPayload = {
	id: number;
	email: string;
	accountType: string;
	territoire?: Territory;
	nom?: string;
	prenom?: string;
};

export async function createJwt(account: Account): Promise<
	{
		token: string;
	} & JwtPayload
> {
	const { id, email, accountType, nom, prenom } = account;

	let territoire;
	if (accountType === 'deveco') {
		territoire = (await devecoService.getDevEcoByAccountWithTerritory(account)).territory;
	}

	const signOptions: SignOptions = {
		algorithm: 'HS256',
		expiresIn: '30d',
		subject: '' + id,
	};

	const jwtPayload = {
		id,
		email,
		accountType,
		territoire,
		nom,
		prenom,
	};

	const key = process.env.JWT_KEY || '';

	const token = jwt.sign(jwtPayload, key, signOptions);
	return { token, ...jwtPayload };
}

export function verifyJwt(token: unknown) {
	const key = process.env.JWT_KEY || '';

	if (typeof token !== 'string') {
		return null;
	}

	try {
		return jwt.verify(token, key, { complete: false });
	} catch (error) {
		console.log('Error when verifying token', { token, error });
		return null;
	}
}
