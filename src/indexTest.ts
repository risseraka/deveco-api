import 'dotenv/config';
import { launchCron } from './cron';
import AppDataSource from './data-source';
import app from './server';
import testController from './controller/TestController';

app.use(testController.routes()).use(testController.allowedMethods());

AppDataSource.initialize()
	.then(() => {
		app.on('error', console.error);

		const port = process.env.API_PORT ? parseInt(process.env.API_PORT, 10) : 4000;
		const host = process.env.API_HOST ?? '127.0.0.1';

		app.listen(port, host, () => {
			console.log(`Koa Ready on port ${port}`);
		});

		if (process.env.TRACK_LOGS_ACTIVITY) {
			launchCron();
		}
	})
	.catch((error) => console.log(error));
