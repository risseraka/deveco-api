import { Entity, PrimaryGeneratedColumn, Column, OneToOne } from 'typeorm';

import { Contact } from './Contact';
import { Entite } from './Entite';

@Entity({ name: 'particulier' })
export class Particulier {
	@PrimaryGeneratedColumn()
	id?: number;

	@OneToOne(() => Entite, (entite) => entite.particulier)
	entite?: Entite;

	@OneToOne(() => Contact, (contact) => contact.particulier)
	contact?: Contact;

	@Column({ name: 'nom' })
	nom: string;

	@Column({ name: 'prenom' })
	prenom: string;

	@Column({ name: 'email', nullable: true })
	email: string;

	@Column({ name: 'telephone', nullable: true })
	telephone: string;

	@Column({ name: 'adresse', nullable: true })
	adresse: string;

	@Column({ name: 'code_postal', nullable: true })
	codePostal: string;

	@Column({ name: 'ville', nullable: true })
	ville: string;

	@Column({ name: 'geolocation', nullable: true })
	geolocation: string;

	@Column({ name: 'description', nullable: true })
	description: string;
}
