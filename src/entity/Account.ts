import { Entity, PrimaryGeneratedColumn, Column, OneToOne } from 'typeorm';

import { DevEco } from './DevEco';

type AccountType = 'deveco' | 'superadmin';

@Entity({ name: 'account' })
export class Account {
	@PrimaryGeneratedColumn()
	id: number;

	@Column({ unique: true, nullable: false })
	email: string;

	@Column({ type: 'varchar', nullable: true })
	nom?: string;

	@Column({ type: 'varchar', nullable: true })
	prenom?: string;

	@Column({ name: 'account_type', default: 'deveco' })
	accountType: AccountType;

	@OneToOne(() => DevEco)
	deveco?: DevEco;

	@Column({ type: 'varchar', name: 'access_key', nullable: true })
	accessKey: string | null;

	@Column({ type: 'timestamptz', name: 'access_key_date', nullable: true })
	accessKeyDate: Date | null;

	@Column({ type: 'timestamptz', name: 'last_login', nullable: true })
	lastLogin?: Date | null;

	@Column({ type: 'timestamptz', name: 'last_auth', nullable: true })
	lastAuth?: Date | null;
}
