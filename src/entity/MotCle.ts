import { Entity, PrimaryGeneratedColumn, JoinColumn, ManyToOne, Column } from 'typeorm';

import { Territory } from './Territory';

@Entity({ name: 'mot_cle' })
export class MotCle {
	@PrimaryGeneratedColumn()
	id: number;

	@ManyToOne(() => Territory)
	@JoinColumn({ name: 'territory_id' })
	territory: Territory;

	@Column()
	motCle: string;
}
