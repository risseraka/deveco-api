import { Entity, PrimaryGeneratedColumn, Column, JoinColumn, ManyToOne } from 'typeorm';

import { DevEco } from './DevEco';
import { Fiche } from './Fiche';

@Entity({ name: 'rappel' })
export class Rappel {
	@PrimaryGeneratedColumn()
	id?: number;

	@ManyToOne(() => Fiche, (fiche) => fiche.rappels, { onDelete: 'CASCADE' })
	@JoinColumn({ name: 'fiche_id' })
	fiche?: Fiche;

	@ManyToOne(() => DevEco)
	@JoinColumn({ name: 'createur_id' })
	createur?: DevEco;

	@Column({ name: 'date_rappel', type: 'date' })
	dateRappel: Date;

	@Column({ name: 'titre' })
	titre: string;

	@Column({ name: 'date_cloture', type: 'date', nullable: true })
	dateCloture?: Date;

	@ManyToOne(() => DevEco)
	@JoinColumn({ name: 'auteur_cloture_id' })
	auteurCloture?: DevEco;
}
