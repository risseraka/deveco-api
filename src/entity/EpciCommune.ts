import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity({ name: 'epci_commune' })
export class EpciCommune {
	@PrimaryColumn({ name: 'insee_com', nullable: false })
	insee_com: string;

	@Column({ name: 'insee_epci', nullable: false })
	insee_epci: string;

	@Column({ name: 'insee_dep', nullable: false })
	insee_dep: string;

	@Column({ name: 'insee_reg', nullable: false })
	insee_reg: string;
}
