import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity({ name: 'etablissement', synchronize: false })
export class Etablissement {
	@Column({ type: 'varchar', nullable: false, length: 9 })
	siren: string;

	@PrimaryColumn({ nullable: false, length: 14 })
	siret: string;

	@Column({ type: 'varchar', nullable: true, name: 'nom_public' })
	nomPublic: string | null;

	@Column({ type: 'varchar', nullable: true, name: 'nom_recherche' })
	nomRecherche: string | null;

	@Column({ type: 'varchar', nullable: true, name: 'enseigne' })
	enseigne: string | null;

	@Column({ type: 'varchar', nullable: true, name: 'adresse_complete' })
	adresseComplete: string | null;

	@Column({ type: 'varchar', nullable: true, name: 'code_commune' })
	codeCommune: string | null;

	@Column({ name: 'longitude', type: 'float', nullable: true })
	longitude: number;

	@Column({ name: 'latitude', type: 'float', nullable: true })
	latitude: number;

	@Column({ type: 'varchar', length: 2, nullable: true, name: 'tranche_effectifs' })
	trancheEffectifs: string | null;

	@Column({ nullable: true, length: 9, name: 'annee_effectifs' })
	anneeEffectifs: string;

	@Column({ type: 'varchar', nullable: true, name: 'sigle' })
	sigle: string | null;

	@Column({ type: 'varchar', nullable: true, name: 'code_naf' })
	codeNaf: string | null;

	@Column({ type: 'varchar', nullable: true, name: 'libelle_naf' })
	libelleNaf: string | null;

	@Column({ type: 'varchar', nullable: true, name: 'libelle_categorie_naf' })
	libelleCategorieNaf: string | null;

	@Column({ type: 'date', name: 'date_creation', nullable: true })
	dateCreation: Date | null;

	@Column({ type: 'date', name: 'date_creation_entreprise', nullable: true })
	dateCreationEntreprise: Date | null;

	@Column({
		nullable: false,
		name: 'etat_administratif',
	})
	etatAdministratif: string;

	@Column({ name: 'categorie_juridique', nullable: false })
	categorieJuridique: string;

	// personne_physique ou personne_morale
	@Column({ type: 'varchar', nullable: true, name: 'type_etablissement' })
	typeEtablissement: string | null;
}
