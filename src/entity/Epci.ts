import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity({ name: 'epci' })
export class Epci {
	@PrimaryColumn({ name: 'insee_epci' })
	id: string;

	@Column({ name: 'lib_epci', nullable: false })
	libEpci: string;

	@Column({ name: 'nature', nullable: false })
	nature: string;
}
