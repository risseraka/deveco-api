import {
	Entity,
	PrimaryGeneratedColumn,
	Column,
	JoinColumn,
	ManyToOne,
	CreateDateColumn,
	UpdateDateColumn,
} from 'typeorm';

import { DevEco } from './DevEco';
import { Fiche } from './Fiche';

@Entity({ name: 'echange' })
export class Echange {
	@PrimaryGeneratedColumn()
	id?: number;

	@ManyToOne(() => Fiche, (fiche) => fiche.echanges, { onDelete: 'CASCADE' })
	@JoinColumn({ name: 'fiche_id' })
	fiche?: Fiche;

	@Column({ name: 'titre', nullable: true })
	titre?: string;

	@ManyToOne(() => DevEco)
	@JoinColumn({ name: 'createur_id' })
	createur?: DevEco;

	// telephone, email, rencontre, courrier
	@Column({ name: 'type_echange' })
	typeEchange: string;

	@Column({ name: 'date_echange', type: 'date' })
	dateEchange: Date;

	@Column({ name: 'compte_rendu' })
	compteRendu: string;

	@Column('simple-array')
	public themes: string[];

	@ManyToOne(() => DevEco)
	@JoinColumn({ name: 'auteur_modification_id' })
	auteurModification?: DevEco;

	@CreateDateColumn({ name: 'created_at' })
	createdAt: Date;

	@UpdateDateColumn({ name: 'updated_at', nullable: true })
	updatedAt?: Date | null;
}
