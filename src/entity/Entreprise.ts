import { Entity, Column, PrimaryColumn, OneToMany } from 'typeorm';

import { Exercice } from './Exercice';

export type MandataireSocial = {
	nom: string;
	prenom: string;
	fonction: string;
	date_naissance: string;
	date_naissance_timestamp: number;
	dirigeant: boolean;
	raison_sociale: string;
	identifiant: string;
	type: string;
};

@Entity({ name: 'entreprise' })
export class Entreprise {
	@PrimaryColumn({ name: 'siret' })
	siret: string;

	@Column({ name: 'longitude', type: 'float', nullable: true })
	longitude: number;

	@Column({ name: 'latitude', type: 'float', nullable: true })
	latitude: number;

	@Column({ type: 'date', name: 'date_creation', nullable: true })
	dateCreation: Date | null;

	@Column({ name: 'nom', nullable: true })
	nom: string;

	@Column({ type: 'varchar', nullable: true, name: 'enseigne' })
	enseigne: string | null;

	@Column({ type: 'varchar', nullable: true, name: 'code_naf' })
	codeNaf: string | null;

	@Column({ type: 'varchar', nullable: true, name: 'libelle_naf' })
	libelleNaf: string | null;

	@Column({ type: 'varchar', nullable: true, name: 'libelle_categorie_naf' })
	libelleCategorieNaf: string | null;

	@Column({ name: 'adresse', nullable: true })
	adresse: string;

	@Column({ name: 'code_commune', nullable: true })
	codeCommune: string;

	@Column({ name: 'etat_administratif', nullable: true })
	etatAdministratif: string;

	@Column({ name: 'forme_juridique', nullable: true })
	formeJuridique: string;

	@Column({ nullable: true })
	effectifs: string;

	@Column({ name: 'date_effectifs', type: 'date', nullable: true })
	dateEffectifs: Date | null;

	@OneToMany(() => Exercice, (exercice) => exercice.entreprise)
	exercices: Exercice[];

	@Column({
		name: 'mandataires_sociaux',
		type: 'jsonb',
		array: false,
		default: () => "'[]'",
		nullable: true,
	})
	mandatairesSociaux: Array<MandataireSocial>;
}
