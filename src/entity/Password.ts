import {
	Entity,
	PrimaryGeneratedColumn,
	CreateDateColumn,
	JoinColumn,
	OneToOne,
	UpdateDateColumn,
	ValueTransformer,
	Column,
} from 'typeorm';
import { hashSync } from 'bcrypt';

import { Account } from './Account';

const toBcryptHash: ValueTransformer = {
	from: (value: string) => value,
	to: (value: string) => value && hashSync(value, 10),
};

@Entity()
export class Password {
	@PrimaryGeneratedColumn()
	id: number;

	@Column({ transformer: toBcryptHash })
	password: string;

	@OneToOne(() => Account)
	@JoinColumn({ name: 'account_id' })
	account: Account;

	@UpdateDateColumn({ name: 'updated_at' })
	updatedAt: Date;

	@CreateDateColumn({ name: 'created_at' })
	createdAt: Date;
}
