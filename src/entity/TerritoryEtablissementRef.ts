import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';

import { Territory } from './Territory';

@Entity({ name: 'territory_etablissement_reference', synchronize: false })
export class TerritoryEtablissementReference {
	@PrimaryColumn({ name: 'id', primaryKeyConstraintName: 'PK_territory_etablissement_reference' })
	id: string;

	@ManyToOne(() => Territory, { nullable: false })
	@JoinColumn({ name: 'territory_id', foreignKeyConstraintName: 'FK_ter_eta_ref_territory_id' })
	territory: Territory;

	@Column({ name: 'siret', nullable: false })
	siret: number;

	@Column({ name: 'nom_recherche', nullable: false })
	nomRecherche: string;

	@Column({ name: 'etat_administratif', nullable: false })
	etatAdministratif: string;

	@Column({ name: 'libelle_naf', nullable: false })
	libelleNaf: string;

	@Column({ name: 'code_naf', nullable: false })
	codeNaf: string;

	@Column({ name: 'categorie_juridique_courante', nullable: false })
	categorieJuridique: boolean;

	@Column({ name: 'tranche_effectifs', nullable: false })
	trancheEffectifs: string;

	@Column({ name: 'code_commune', nullable: false })
	codeCommune: string;

	@Index('IDX_ter_eta_ref_tsv', { synchronize: false })
	@Column({
		type: 'tsvector',
		generatedType: 'STORED',
		asExpression: "to_tsvector('french', nom_recherche)",
	})
	tsv: string[];

	@Column({ type: 'date', name: 'date_creation', nullable: true })
	dateCreation: Date | null;

	@Column({ type: 'varchar', nullable: true, name: 'adresse_complete' })
	adresseComplete: string | null;
}
