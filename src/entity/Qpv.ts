import { Entity, Column, Index, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'qp_metropoleoutremer_wgs84_epsg4326' })
export class Qpv {
	@PrimaryGeneratedColumn({ name: 'gid' })
	id: string;

	@Column({ name: 'code_qp', length: 8, nullable: true })
	codeQp: string;

	@Column({ name: 'nom_qp', length: 254, nullable: true })
	nomQp: string;

	@Column({ name: 'commune_qp', length: 254, nullable: true })
	communeQp: string;

	@Index('qp_metropoleoutremer_wgs84_epsg4326_geom_idx', { spatial: true })
	@Column({
		type: 'geometry',
		srid: 0,
		spatialFeatureType: 'MultiPolygon',
		name: 'geom',
		nullable: true,
	})
	geom: string;
}
