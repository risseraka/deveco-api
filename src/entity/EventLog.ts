import {
	Entity,
	PrimaryGeneratedColumn,
	JoinColumn,
	ManyToOne,
	CreateDateColumn,
	Column,
} from 'typeorm';

import { DevEco } from './DevEco';

export enum EventLogActionType {
	CREATE = 'CREATE',
	DELETE = 'DELETE',
	UPDATE = 'UPDATE',
	VIEW = 'VIEW',
}

export enum EventLogEntityType {
	PERSONNE_PHYSIQUE = 'FICHE_PP',
	PERSONNE_MORALE = 'FICHE_PM',
	LOCAL = 'LOCAL',
}

@Entity({ name: 'event_log' })
export class EventLog {
	@PrimaryGeneratedColumn()
	id: number;

	@ManyToOne(() => DevEco, (deveco) => deveco.eventLogs, { onDelete: 'CASCADE' })
	@JoinColumn({ name: 'deveco_id' })
	deveco: DevEco;

	@CreateDateColumn({ name: 'date' })
	date: Date;

	@Column({ name: 'entity_type', nullable: false })
	entityType: EventLogEntityType;

	@Column({ name: 'entity_id', nullable: false })
	entityId: number;

	@Column({ name: 'action', nullable: false })
	action: EventLogActionType;
}
