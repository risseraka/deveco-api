import { Entity, PrimaryGeneratedColumn, JoinColumn, ManyToOne, Column } from 'typeorm';

import { Territory } from './Territory';

@Entity({ name: 'localisation_entreprise' })
export class LocalisationEntreprise {
	@PrimaryGeneratedColumn()
	id: number;

	@ManyToOne(() => Territory)
	@JoinColumn({ name: 'territory_id' })
	territory: Territory;

	@Column()
	localisation: string;
}
