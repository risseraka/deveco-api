import {
	Entity,
	PrimaryGeneratedColumn,
	Column,
	OneToOne,
	JoinColumn,
	CreateDateColumn,
	UpdateDateColumn,
	Unique,
} from 'typeorm';

import { Commune } from './Commune';
import { Epci } from './Epci';

@Entity()
@Unique('uniq_territory_epci_id', ['epci'])
@Unique('uniq_territory_commune_id', ['commune'])
export class Territory {
	@PrimaryGeneratedColumn()
	id: number;

	@Column({ nullable: false, name: 'territory_type' })
	territoryType: string;

	@Column({ nullable: false })
	name: string;

	@OneToOne(() => Epci, { createForeignKeyConstraints: false })
	@JoinColumn({ name: 'epci_id' })
	epci: Epci;

	@OneToOne(() => Commune, { createForeignKeyConstraints: false })
	@JoinColumn({ name: 'commune_id' })
	commune: Commune;

	@CreateDateColumn({ name: 'created_at' })
	createdAt: Date;

	@UpdateDateColumn({ name: 'updated_at' })
	updatedAt: Date;
}
