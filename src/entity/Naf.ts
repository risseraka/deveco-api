import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity({ name: 'naf' })
export class Naf {
	@Column({ name: 'id_1', nullable: false })
	id1: string;

	@Column({ name: 'label_1', nullable: false })
	label1: string;

	@Column({ name: 'id_2', nullable: false })
	id2: string;

	@Column({ name: 'label_2', nullable: false })
	label2: string;

	@Column({ name: 'id_3', nullable: false })
	id3: string;

	@Column({ name: 'label_3', nullable: false })
	label3: string;

	@Column({ name: 'id_4', nullable: false })
	id4: string;

	@Column({ name: 'label_4', nullable: false })
	label4: string;

	@PrimaryColumn({ name: 'id_5', nullable: false })
	id5: string;

	@Column({ name: 'label_5', nullable: false })
	label5: string;
}
