import { Entity, PrimaryGeneratedColumn, JoinColumn, ManyToOne, Column } from 'typeorm';

import { Territory } from './Territory';

@Entity({ name: 'activite_entreprise' })
export class ActiviteEntreprise {
	@PrimaryGeneratedColumn()
	id: number;

	@ManyToOne(() => Territory)
	@JoinColumn({ name: 'territory_id' })
	territory: Territory;

	@Column()
	activite: string;
}
