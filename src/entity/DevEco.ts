import {
	Entity,
	PrimaryGeneratedColumn,
	OneToOne,
	JoinColumn,
	ManyToOne,
	CreateDateColumn,
	UpdateDateColumn,
	OneToMany,
} from 'typeorm';

import { Account } from './Account';
import { EventLog } from './EventLog';
import { Territory } from './Territory';

@Entity({ name: 'deveco' })
export class DevEco {
	@PrimaryGeneratedColumn()
	id: number;

	@OneToOne(() => Account)
	@JoinColumn({ name: 'account_id' })
	account: Account;

	@ManyToOne(() => Territory)
	@JoinColumn({ name: 'territory_id' })
	territory: Territory;

	@OneToMany(() => EventLog, (eventLog) => eventLog.deveco, { cascade: true })
	eventLogs?: EventLog[];

	@CreateDateColumn({ name: 'created_at' })
	createdAt: Date;

	@UpdateDateColumn({ name: 'updated_at' })
	updatedAt: Date;
}
