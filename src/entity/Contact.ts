import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, OneToOne } from 'typeorm';

import { Entite } from './Entite';
import { Particulier } from './Particulier';

@Entity({ name: 'contact' })
export class Contact {
	@PrimaryGeneratedColumn()
	id?: number;

	@ManyToOne(() => Entite, (entite) => entite.contacts, { onDelete: 'CASCADE' })
	@JoinColumn({ name: 'entite_id' })
	entite?: Entite;

	@Column({ name: 'fonction', nullable: true })
	fonction: string;

	@OneToOne(() => Particulier)
	@JoinColumn({ name: 'particulier_id' })
	particulier: Particulier;
}
