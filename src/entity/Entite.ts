import {
	Entity,
	PrimaryGeneratedColumn,
	JoinColumn,
	ManyToOne,
	Column,
	OneToOne,
	OneToMany,
	CreateDateColumn,
	UpdateDateColumn,
	Unique,
} from 'typeorm';

import { Contact } from './Contact';
import { DevEco } from './DevEco';
import { Entreprise } from './Entreprise';
import { Fiche } from './Fiche';
import { Particulier } from './Particulier';
import { Proprietaire } from './Proprietaire';
import { Territory } from './Territory';

export type EntiteType = 'PP' | 'PM';

@Entity({ name: 'entite' })
@Unique('UNIQ_entite_territoire_entreprise', ['territoire', 'entreprise'])
export class Entite {
	@PrimaryGeneratedColumn()
	id: number;

	@OneToOne(() => Fiche, (fiche) => fiche.entite)
	fiche?: Fiche;

	@OneToMany(() => Proprietaire, (proprietaire) => proprietaire.entite)
	proprietes?: Proprietaire[];

	@ManyToOne(() => Territory)
	@JoinColumn({ name: 'territoire_id' })
	territoire: Territory;

	@ManyToOne(() => DevEco)
	@JoinColumn({ name: 'deveco_id' })
	createur: DevEco;

	@ManyToOne(() => DevEco)
	@JoinColumn({ name: 'auteur_modification_id' })
	auteurModification: DevEco;

	@OneToMany(() => Contact, (contact) => contact.entite, { cascade: true })
	contacts: Contact[];

	@Column({ name: 'entite_type' })
	entiteType: EntiteType;

	@Column('simple-array', { name: 'activites_reelles', nullable: true })
	activitesReelles?: string[];

	@Column('simple-array', { name: 'entreprise_localisations', nullable: true })
	entrepriseLocalisations?: string[];

	@Column('simple-array', { name: 'mots_cles', nullable: true })
	motsCles?: string[];

	@Column({ name: 'activite_autre', nullable: true })
	activiteAutre?: string;

	@ManyToOne(() => Entreprise)
	@JoinColumn({ name: 'entreprise_id' })
	entreprise?: Entreprise;

	@OneToOne(() => Particulier, (particulier) => particulier.entite, { cascade: true })
	@JoinColumn({ name: 'particulier_id' })
	particulier?: Particulier;

	@CreateDateColumn({ name: 'created_at' })
	createdAt: Date;

	@UpdateDateColumn({ name: 'updated_at' })
	updatedAt: Date;

	@Column({ name: 'future_enseigne', nullable: true })
	futureEnseigne?: string;
}
