import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity({ name: 'commune' })
export class Commune {
	@Column({ name: 'typecom' })
	typecom: string;

	@PrimaryColumn({ name: 'insee_com' })
	id: string;

	@Column({ name: 'insee_dep' })
	inseeDep: string;

	@Column({ name: 'insee_reg' })
	inseeReg: string;

	@Column({ name: 'ctcd' })
	ctcd: string;

	@Column({ name: 'insee_arr' })
	inseeArr: string;

	@Column({ name: 'tncc' })
	tncc: string;

	@Column({ name: 'ncc' })
	ncc: string;

	@Column({ name: 'nccenr' })
	nccenr: string;

	@Column({ name: 'lib_com' })
	libCom: string;

	@Column({ name: 'insee_can' })
	inseeCan: string;

	@Column({ name: 'com_parent' })
	comParent: string;
}
