import Koa from 'koa';
import bodyParser from 'koa-bodyparser';
import json from 'koa-json';
import cookie from 'koa-cookie';
import logger from 'koa-logger';

import ficheController from './controller/FicheController';
import localController from './controller/LocalController';
import communeController from './controller/CommuneController';
import epciController from './controller/EpciController';
import inseeApiController from './controller/InseeApiController';
import authController from './controller/AuthController';
import ErrorHandlerMiddleware from './middleware/ErrorHandlerMiddleware';
import SquareBracketsParams from './middleware/SquareBracketsParams';
import activiteEntrepriseController from './controller/ActiviteEntrepriseController';
import localisationEntrepriseController from './controller/LocalisationEntrepriseController';
import motCleController from './controller/MotCleController';
import contactController from './controller/ContactController';
import echangeController from './controller/EchangeController';
import rappelController from './controller/RappelController';
import demandeController from './controller/DemandeController';
import accountController from './controller/AccountController';
import adminController from './controller/AdminController';
import exportController from './controller/ExportExcelController';
import territoiresController from './controller/TerritoiresController';
import statsController from './controller/StatsController';
import nafController from './controller/NafController';

const app = new Koa();
app.use(logger());
app.use(cookie());

// Generic error handling middleware.
app.use(ErrorHandlerMiddleware);

// Middlewares
app.use(json());
app.use(bodyParser());
app.use(SquareBracketsParams);

app.use(authController.routes()).use(authController.allowedMethods());
app.use(inseeApiController.routes()).use(inseeApiController.allowedMethods());
app.use(epciController.routes()).use(epciController.allowedMethods());
app.use(communeController.routes()).use(communeController.allowedMethods());
app.use(ficheController.routes()).use(ficheController.allowedMethods());
app.use(localController.routes()).use(localController.allowedMethods());
app.use(activiteEntrepriseController.routes()).use(activiteEntrepriseController.allowedMethods());
app
	.use(localisationEntrepriseController.routes())
	.use(localisationEntrepriseController.allowedMethods());
app.use(motCleController.routes()).use(motCleController.allowedMethods());
app.use(contactController.routes()).use(contactController.allowedMethods());
app.use(echangeController.routes()).use(echangeController.allowedMethods());
app.use(rappelController.routes()).use(rappelController.allowedMethods());
app.use(demandeController.routes()).use(demandeController.allowedMethods());
app.use(accountController.routes()).use(accountController.allowedMethods());
app.use(adminController.routes()).use(adminController.allowedMethods());
app.use(exportController.routes()).use(exportController.allowedMethods());
app.use(territoiresController.routes()).use(territoiresController.allowedMethods());
app.use(statsController.routes()).use(statsController.allowedMethods());
app.use(nafController.routes()).use(nafController.allowedMethods());

// Application error logging.
app.on('error', console.error);

export default app;
