import Koa from 'koa';
import jwt from 'jsonwebtoken';

import { Account } from '../entity/Account';
import { JwtPayload } from '../lib/getJwt';
import AppDataSource from '../data-source';
import devecoService from '../service/DevEcoService';
import { DevEco } from '../entity/DevEco';

export default async (
	ctx: Koa.ParameterizedContext<{ account: Account; deveco?: DevEco }>,
	next: (ctx: Koa.ParameterizedContext<{ account: Account; deveco?: DevEco }>) => Promise<unknown>
) => {
	const cookies = ctx.cookie;
	if (!cookies) {
		ctx.throw(401);
	}

	const token = cookies.jwt;
	if (!token) {
		ctx.throw(401);
	}

	const { id } = jwt.verify(token, process.env.JWT_KEY || '') as JwtPayload;
	const account = await AppDataSource.getRepository(Account).findOneBy({ id });
	if (!account) {
		ctx.throw(401);
	}

	const lastAuth = new Date();
	await AppDataSource.getRepository(Account).update(id, { lastAuth });

	ctx.state.account = { ...account, lastAuth };
	if (account.accountType === 'deveco') {
		ctx.state.deveco = await devecoService.getDevEcoByAccountWithTerritory(account);
	}

	return next(ctx);
};
