import Koa from 'koa';

export default (role: string) =>
	async (ctx: Koa.Context, next: (ctx: Koa.Context) => Promise<unknown>) => {
		const account = ctx.state.account;
		if (!account) {
			ctx.throw(401);
		}

		if (role !== account.accountType) {
			ctx.throw(401);
		}

		return next(ctx);
	};
