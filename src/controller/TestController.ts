import Router from 'koa-router';
import * as Koa from 'koa';
import { Repository } from 'typeorm';
import { SqlReader } from 'node-sql-reader';
import { StatusCodes } from 'http-status-codes';

import * as path from 'path';
import { ParsedUrlQuery } from 'querystring';

import { Fiche } from '../entity/Fiche';
import { Account } from '../entity/Account';
import { Particulier } from '../entity/Particulier';
import { Entreprise } from '../entity/Entreprise';
import { DevEco } from '../entity/DevEco';
import { Territory } from '../entity/Territory';
import { Password } from '../entity/Password';
import AppDataSource from '../data-source';

const seedSql = '../../data/dev/seed.sql';

const routerOpts: Router.IRouterOptions = {
	prefix: '/test',
};

const TestMiddleware = async (ctx: Koa.Context, next: (ctx: Koa.Context) => Promise<unknown>) => {
	if (process.env.NODE_ENV !== 'test') {
		ctx.throw(401);
	}

	return next(ctx);
};

const router: Router = new Router(routerOpts);
router.use(TestMiddleware);

const entityMap = {
	account: Account,
	deveco: DevEco,
	entreprise: Entreprise,
	particulier: Particulier,
	fiche: Fiche,
	superadmin: Account,
} as const;

const createDevecoAccount = async (payload: any, query: ParsedUrlQuery): Promise<Account> => {
	const territoryRepository = AppDataSource.getRepository(Territory);
	const territory = await territoryRepository.findOneOrFail({
		where: { epci: { id: '200054781' } },
	});

	const accountRepository = AppDataSource.getRepository(Account);
	const account = await accountRepository.save(payload);

	const devecoRepository = AppDataSource.getRepository(DevEco);
	await devecoRepository.insert({ territory, account });

	const password = query.password;
	if (password && typeof password === 'string') {
		console.log('inserting', { password });
		await AppDataSource.getRepository(Password).insert({ account, password });
	}

	return account;
};

const createSuperAdminAccount = async (payload: any, query: ParsedUrlQuery): Promise<Account> => {
	const accountRepository = AppDataSource.getRepository(Account);
	const account = await accountRepository.save({ ...payload, accountType: 'superadmin' });

	const password = query.password;
	if (password && typeof password === 'string') {
		await AppDataSource.getRepository(Password).insert({ account, password });
	}

	return account;
};

const upsert = async <K extends keyof typeof entityMap>(
	entityName: K,
	entityId: string | null,
	payload: Record<string, unknown>,
	query: ParsedUrlQuery
): Promise<any> => {
	if (Object.keys(entityMap).includes(entityName)) {
		const entity = entityMap[entityName];
		const repository = AppDataSource.getRepository(entity) as Repository<any>;
		if (entityId) {
			await repository.update(Number(entityId), payload as any);
			return await repository.findOneBy({ id: entityId });
		} else if (entityName === 'account') {
			return await createDevecoAccount(payload, query);
		} else if (entityName === 'superadmin') {
			return await createSuperAdminAccount(payload, query);
		} else {
			return await repository.save(payload);
		}
	}
	throw new Error(`Unsupported entity name ${entityName}`);
};

const cleanAllTables = async () => {
	const seededTables = [
		'commune',
		'epci',
		'epci_commune',
		'naf',
		'etablissement',
		'territory',
		'territory_etablissement_reference',
	];
	const entities: Array<{ name: string; tableName: string }> = [];
	(await AppDataSource.entityMetadatas).forEach((x) =>
		entities.push({ name: x.name, tableName: x.tableName })
	);
	try {
		for (const entity of entities) {
			const repository = await AppDataSource.getRepository(entity.name);
			if (!seededTables.includes(entity.tableName)) {
				await repository.query(`TRUNCATE TABLE ${entity.tableName} RESTART IDENTITY CASCADE;`);
			}
		}
	} catch (error) {
		throw new Error(`ERROR: Cleaning test db: ${error}`);
	}
};

const initFromSeed = async () => {
	const manager = AppDataSource.manager;
	const queries = SqlReader.readSqlFile(path.join(__dirname, seedSql));
	for (const query of queries) {
		await manager.query(query);
	}
};

router.get('/reset', async (ctx: Koa.Context) => {
	await cleanAllTables();
	return (ctx.status = StatusCodes.OK);
});

router.get('/seed', async (ctx: Koa.Context) => {
	await initFromSeed();
	return (ctx.status = StatusCodes.OK);
});

router.get('/resetAndSeed', async (ctx: Koa.Context) => {
	await cleanAllTables();
	await initFromSeed();
	return (ctx.status = StatusCodes.OK);
});

router.post('/upsert/:entity/:id?', async (ctx: Koa.Context) => {
	const entity = ctx.params.entity;
	const id = ctx.params.id;
	const payload = ctx.request.body;
	const query = ctx.request.query;
	ctx.body = await upsert(entity, id, payload, query);
});

export default router;
