import * as Koa from 'koa';
import Router from 'koa-router';
import { ILike } from 'typeorm';

import { MotCle } from '../entity/MotCle';
import { DevEco } from '../entity/DevEco';
import AuthMiddleware from '../middleware/AuthMiddleware';
import AppDataSource from '../data-source';

const routerOpts: Router.IRouterOptions = {
	prefix: '/mots',
};

const router: Router = new Router(routerOpts);
router.use(AuthMiddleware);

router.get('/', async (ctx: Koa.Context) => {
	const { territory } = ctx.state.deveco as DevEco;
	const query = ctx.query;
	const search = query.search as string;

	const motsCles = await AppDataSource.getRepository(MotCle).find({
		order: { motCle: 'ASC' },
		where: {
			territory: { id: territory.id },
			motCle: ILike(`%${search}%`),
		},
		take: 5,
	});

	ctx.body = motsCles;
});

export default router;
