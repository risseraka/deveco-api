import { StatusCodes } from 'http-status-codes';
import * as Koa from 'koa';
import Router from 'koa-router';
import { FindOptionsWhere } from 'typeorm';

import AppDataSource from '../data-source';
import { Contact } from '../entity/Contact';
import { DevEco } from '../entity/DevEco';
import { EventLogActionType } from '../entity/EventLog';
import { Fiche } from '../entity/Fiche';
import { getLogEntityType } from '../lib/utils';
import { Particulier } from '../entity/Particulier';
import AuthMiddleware from '../middleware/AuthMiddleware';
import eventLogService from '../service/EventLogService';
import ficheService from '../service/FicheService';
import { FicheView, toView } from '../view/FicheView';

const routerOpts: Router.IRouterOptions = {
	prefix: '/contacts',
};

const router: Router = new Router(routerOpts);
router.use(AuthMiddleware);

async function ficheToView(where: FindOptionsWhere<Fiche>): Promise<FicheView> {
	const fiche = await ficheService.loadFiche(where);
	return toView(fiche);
}

router.post('/creer', async (ctx: Koa.Context) => {
	const deveco = ctx.state.deveco as DevEco;

	const ficheIdAsString = (ctx.query.ficheId as string) || '';
	const ficheId = parseInt(ficheIdAsString);
	if (!ficheId) {
		ctx.throw(StatusCodes.BAD_REQUEST, 'ficheId is required');
	}

	const fiche = await AppDataSource.getRepository(Fiche).findOneOrFail({
		where: {
			id: ficheId,
			territoire: { id: deveco.territory.id },
		},
		relations: { entite: true },
	});
	const { nom, prenom, fonction, telephone, email } = ctx.request.body;

	const particulier = AppDataSource.getRepository(Particulier).create({
		nom,
		prenom,
		telephone,
		email,
	});
	const { identifiers: particuliers } = await AppDataSource.getRepository(Particulier).insert(
		particulier
	);
	const contact = AppDataSource.getRepository(Contact).create({
		fonction,
		particulier: particuliers[0],
		entite: fiche.entite,
	});
	await AppDataSource.getRepository(Contact).save(contact);
	await AppDataSource.getRepository(Fiche).update(ficheId, {
		updatedAt: new Date(),
		auteurModification: deveco,
	});

	await eventLogService.addEventLog({
		action: EventLogActionType.UPDATE,
		deveco,
		entityType: getLogEntityType(fiche),
		entityId: ficheId,
	});

	ctx.body = await ficheToView({ id: ficheId });
	ctx.status = StatusCodes.OK;
});

router.del('/:id', async (ctx: Koa.Context) => {
	const deveco = ctx.state.deveco as DevEco;

	const ficheIdAsString = (ctx.query.ficheId as string) || '';
	const ficheId = parseInt(ficheIdAsString);
	if (!ficheId) {
		ctx.throw(StatusCodes.BAD_REQUEST, 'ficheId is required');
	}

	const fiche = await AppDataSource.getRepository(Fiche).findOneOrFail({
		where: {
			id: ficheId,
			territoire: { id: deveco.territory.id },
		},
		relations: { entite: true },
	});

	const id = parseInt(ctx.params.id);
	if (!id) {
		ctx.throw(StatusCodes.BAD_REQUEST, 'id is required');
	}

	await AppDataSource.getRepository(Contact).delete({ id: id, entite: { id: fiche.entite.id } });
	await AppDataSource.getRepository(Fiche).update(ficheId, {
		updatedAt: new Date(),
		auteurModification: deveco,
	});

	await eventLogService.addEventLog({
		action: EventLogActionType.UPDATE,
		deveco,
		entityType: getLogEntityType(fiche),
		entityId: ficheId,
	});

	ctx.status = StatusCodes.OK;
	ctx.body = await ficheToView({ id: ficheId });
});

router.post('/:id/modifier', async (ctx: Koa.Context) => {
	const { contact } = ctx.request.body;
	const deveco = ctx.state.deveco as DevEco;

	const ficheIdAsString = (ctx.query.ficheId as string) || '';
	const ficheId = parseInt(ficheIdAsString);
	if (!ficheId) {
		ctx.throw(StatusCodes.BAD_REQUEST, 'ficheId is required');
	}

	const fiche = await AppDataSource.getRepository(Fiche).findOneOrFail({
		where: {
			id: ficheId,
			territoire: { id: deveco.territory.id },
		},
	});

	const id = parseInt(ctx.params.id);
	if (!id) {
		ctx.throw(StatusCodes.BAD_REQUEST, 'id is required');
	}

	const { fonction, ...particulier } = contact;
	await AppDataSource.getRepository(Contact).update(id, { fonction });

	/* for some reason, updating using an object like `{ contact: { id } }`
	 * yields an error, so we need to retrieve the entity beforehand and use its id
	 */
	const part = await AppDataSource.getRepository(Particulier).findOne({
		where: { contact: { id } },
	});
	if (part?.id) {
		await AppDataSource.getRepository(Particulier).update(part.id, particulier);
	}

	await AppDataSource.getRepository(Fiche).update(ficheId, {
		updatedAt: new Date(),
		auteurModification: deveco,
	});

	await eventLogService.addEventLog({
		action: EventLogActionType.UPDATE,
		deveco,
		entityType: getLogEntityType(fiche),
		entityId: ficheId,
	});

	ctx.status = StatusCodes.OK;
	ctx.body = await ficheToView({ id: ficheId });
});

export default router;
