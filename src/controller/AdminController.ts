import { StatusCodes } from 'http-status-codes';
import * as Koa from 'koa';
import Router from 'koa-router';
import { SelectQueryBuilder } from 'typeorm';

import { Demande } from '../entity/Demande';
import { Echange } from '../entity/Echange';
import { Fiche } from '../entity/Fiche';
import { Account } from '../entity/Account';
import { Commune } from '../entity/Commune';
import { DevEco } from '../entity/DevEco';
import { Epci } from '../entity/Epci';
import { Password } from '../entity/Password';
import { Territory } from '../entity/Territory';
import AuthMiddleware from '../middleware/AuthMiddleware';
import RoleMiddleware from '../middleware/RoleMiddleware';
import AppDataSource from '../data-source';

const routerOpts: Router.IRouterOptions = {
	prefix: '/admin',
};

const router: Router = new Router(routerOpts);
router.use(AuthMiddleware);
router.use(RoleMiddleware('superadmin'));

async function returnAllDevecos() {
	const devecoRepository = AppDataSource.getRepository(DevEco);
	const devecos = await devecoRepository.find({
		relations: { account: true, territory: true },
	});

	const appUrl = process.env.APP_URL || '';
	const res = devecos.map((user) => ({
		...user.account,
		territoire: user.territory,
		accessUrl: user.account.accessKey ? `${appUrl}/auth/jwt/${user.account.accessKey}` : '',
	}));

	return res;
}

router.get('/utilisateurs', async (ctx: Koa.Context) => {
	ctx.body = await returnAllDevecos();
});

const isCorrectTypeTerritoire = (s?: string): s is 'epci' | 'commune' => {
	return !!s && ['epci', 'commune'].includes(s);
};

router.post('/utilisateurs', async (ctx: Koa.Context) => {
	const { nom, prenom, email, typeTerritoire, nameTerritoire, idTerritoire } = ctx.request.body;

	if (!isCorrectTypeTerritoire(typeTerritoire)) {
		ctx.body = { error: `bad territory type: ${typeTerritoire}` };
		return;
	}
	const accountRepository = AppDataSource.getRepository(Account);
	const devecoRepository = AppDataSource.getRepository(DevEco);
	const epciRepository = AppDataSource.getRepository(Epci);
	const communeRepository = AppDataSource.getRepository(Commune);
	const territoryRepository = AppDataSource.getRepository(Territory);

	const existingAccount = await accountRepository.findOne({ where: { email } });
	if (existingAccount) {
		ctx.body = { error: 'email exists' };
		return;
	}

	const territoryType: { territoryType?: string } = {};
	let found: Epci | Commune | null;
	if (typeTerritoire === 'epci') {
		found = await epciRepository.findOne({ where: { id: idTerritoire } });

		if (!found) {
			ctx.body = { error: 'epci does not exist' };
			return;
		}

		territoryType.territoryType = found.nature;
	} else {
		found = await communeRepository.findOne({ where: { id: idTerritoire } });

		if (!found) {
			ctx.body = { error: 'commune does not exist' };
			return;
		}

		territoryType.territoryType = found.typecom;
	}

	let territory = await territoryRepository.findOne({
		where: { [typeTerritoire]: { id: found.id } },
	});
	if (!territory) {
		territory = await territoryRepository.save({
			name: nameTerritoire,
			...territoryType,
			[typeTerritoire]: found,
		});
	}
	const account = await accountRepository.save({ email, nom, prenom, accountType: 'deveco' });
	await devecoRepository.insert({ territory, account });

	ctx.body = await returnAllDevecos();
});

router.post('/password', async (ctx: Koa.Context) => {
	const { id, password } = ctx.request.body ?? {};

	if (!id || !password) {
		ctx.status = StatusCodes.BAD_REQUEST;
		ctx.body = {};
		return;
	}

	const account = await AppDataSource.getRepository(Account).findOneOrFail({ where: { id } });
	const currentPassword = await AppDataSource.getRepository(Password).findOneBy({
		account: { id: account.id },
	});

	if (currentPassword) {
		await AppDataSource.getRepository(Password).update(
			{ account: { id: account.id } },
			{ password }
		);
	} else {
		await AppDataSource.getRepository(Password).insert({ account, password });
	}

	ctx.body = {};
});

const formatCommune = (commune: Commune) => ({
	id: commune.id,
	type: 'commune',
	name: `${commune.libCom} (${commune.inseeDep})`,
});

const formatEpci = (epci: Epci) => ({
	id: epci.id,
	type: 'epci',
	name: epci.libEpci,
});

router.get('/stats/territoires', async (ctx: Koa.Context) => {
	const epcis = (
		await AppDataSource.createQueryBuilder()
			.select('territoire')
			.from(Territory, 'territoire')
			.innerJoinAndSelect('territoire.epci', 'epci')
			.getMany()
	)
		.map(({ epci }) => epci)
		.map(formatEpci);
	const communes = (
		await AppDataSource.createQueryBuilder()
			.select('territoire')
			.from(Territory, 'territoire')
			.innerJoinAndSelect('territoire.commune', 'commune')
			.getMany()
	)
		.map(({ commune }) => commune)
		.map(formatCommune);
	ctx.body = epcis.concat(communes).sort((a, b) => a.name.localeCompare(b.name));
});

const days = (amount: number) => 1000 * 60 * 60 * 24 * amount;

router.get('/stats', async (ctx: Koa.Context) => {
	const periode = ctx.query.periode;
	const territoire = ctx.query.territoire;
	const type = ctx.query.type;
	const now = new Date();

	let periodStart: Date;
	switch (periode) {
		case 'mois':
			periodStart = new Date(now.getTime() - days(30));
			break;
		case 'semaine':
			periodStart = new Date(now.getTime() - days(7));
			break;
		case 'tout':
		default:
			periodStart = new Date(0);
			break;
	}

	const periodFilter =
		(key: string) =>
		<T>(query: SelectQueryBuilder<T>) =>
			query.andWhere(`${key} >= :periodStart`, { key, periodStart });

	let territoireFilter = <T>(query: SelectQueryBuilder<T>) => query;
	if (type && territoire) {
		if (type === 'epci') {
			territoireFilter = <T>(query: SelectQueryBuilder<T>) =>
				query.andWhere('territoire.epci_id = :epciId', { epciId: territoire });
		}
		if (type === 'commune') {
			territoireFilter = <T>(query: SelectQueryBuilder<T>) =>
				query.andWhere('territoire.commune_id = :communeId', { communeId: territoire });
		}
	}

	/***************************/
	/* Requêtes sur l'ensemble */
	/***************************/

	const territoiresTotal = await AppDataSource.getRepository(Territory).count();

	const territoiresNouveauxQuery =
		AppDataSource.getRepository(Territory).createQueryBuilder('territoire');
	periodFilter('territoire.createdAt')(territoiresNouveauxQuery);
	const territoiresNouveaux = await territoiresNouveauxQuery.getCount();

	const territoiresActifsQuery = AppDataSource.getRepository(DevEco)
		.createQueryBuilder('deveco')
		.leftJoin('deveco.territory', 'territoire')
		.leftJoin('deveco.account', 'account')
		.select('COUNT(DISTINCT(territoire.id))');
	periodFilter('account.lastLogin')(territoiresActifsQuery);
	const territoiresActifs = Number((await territoiresActifsQuery.getRawOne()).count);
	const territoires = {
		total: territoiresTotal,
		nouveaux: territoiresNouveaux,
		actifs: territoiresActifs,
	};

	const devecosTotal = await AppDataSource.getRepository(DevEco).count();

	const devecosNouveauxQuery = AppDataSource.getRepository(DevEco).createQueryBuilder('deveco');
	periodFilter('deveco.createdAt')(devecosNouveauxQuery);
	const devecosNouveaux = await devecosNouveauxQuery.getCount();

	const devecosActifsQuery = AppDataSource.getRepository(DevEco)
		.createQueryBuilder('deveco')
		.leftJoin('deveco.territory', 'territoire')
		.leftJoin('deveco.account', 'account');
	periodFilter('account.lastLogin')(devecosActifsQuery);
	const devecosActifs = await devecosActifsQuery.getCount();
	const devecos = { total: devecosTotal, nouveaux: devecosNouveaux, actifs: devecosActifs };

	/**************************/
	/* Requêtes sur les zones */
	/**************************/

	const devecosTotalZoneQuery = AppDataSource.getRepository(DevEco)
		.createQueryBuilder('deveco')
		.leftJoin('deveco.territory', 'territoire');
	territoireFilter(devecosTotalZoneQuery);
	const devecosTotalZone = await devecosTotalZoneQuery.getCount();

	const devecosNouveauxZoneQuery = AppDataSource.getRepository(DevEco)
		.createQueryBuilder('deveco')
		.leftJoin('deveco.territory', 'territoire');
	periodFilter('deveco.createdAt')(devecosNouveauxZoneQuery);
	territoireFilter(devecosNouveauxZoneQuery);
	const devecosNouveauxZone = await devecosNouveauxZoneQuery.getCount();

	const devecosActifsZoneQuery = AppDataSource.getRepository(DevEco)
		.createQueryBuilder('deveco')
		.leftJoin('deveco.account', 'account')
		.leftJoin('deveco.territory', 'territoire');
	territoireFilter(devecosActifsZoneQuery);
	periodFilter('account.lastLogin')(devecosActifsZoneQuery);
	const devecosActifsZone = await devecosActifsZoneQuery.getCount();

	const fichesTotalZoneQuery = AppDataSource.getRepository(Fiche)
		.createQueryBuilder('fiche')
		.leftJoin('fiche.territoire', 'territoire');
	territoireFilter(fichesTotalZoneQuery);
	const fichesTotalZone = await fichesTotalZoneQuery.getCount();

	const fichesNouvellesZoneQuery = AppDataSource.getRepository(Fiche)
		.createQueryBuilder('fiche')
		.leftJoin('fiche.territoire', 'territoire');
	territoireFilter(fichesNouvellesZoneQuery);
	periodFilter('fiche.createdAt')(fichesNouvellesZoneQuery);
	const fichesNouvellesZone = await fichesNouvellesZoneQuery.getCount();

	const echangesTotalZoneQuery = AppDataSource.getRepository(Echange)
		.createQueryBuilder('echange')
		.leftJoin('echange.fiche', 'fiche')
		.leftJoin('fiche.territoire', 'territoire');
	territoireFilter(echangesTotalZoneQuery);
	const echangesTotalZone = await echangesTotalZoneQuery.getCount();

	const echangesNouveauxZoneQuery = AppDataSource.getRepository(Echange)
		.createQueryBuilder('echange')
		.leftJoin('echange.fiche', 'fiche')
		.leftJoin('fiche.territoire', 'territoire');
	territoireFilter(echangesNouveauxZoneQuery);
	periodFilter('echange.createdAt')(echangesNouveauxZoneQuery);
	const echangesNouveauxZone = await echangesNouveauxZoneQuery.getCount();

	const demandesTotalZoneQuery = AppDataSource.getRepository(Demande)
		.createQueryBuilder('demande')
		.leftJoin('demande.fiche', 'fiche')
		.leftJoin('fiche.territoire', 'territoire');
	territoireFilter(demandesTotalZoneQuery);
	const demandesTotalZone = await demandesTotalZoneQuery.getCount();

	const demandesNouvellesZoneQuery = AppDataSource.getRepository(Demande)
		.createQueryBuilder('demande')
		.leftJoin('demande.fiche', 'fiche')
		.leftJoin('fiche.territoire', 'territoire');
	territoireFilter(demandesNouvellesZoneQuery);
	periodFilter('demande.createdAt')(demandesNouvellesZoneQuery);
	const demandesNouvellesZone = await demandesNouvellesZoneQuery.getCount();

	const demandesFermeesZoneQuery = AppDataSource.getRepository(Demande)
		.createQueryBuilder('demande')
		.leftJoin('demande.fiche', 'fiche')
		.leftJoin('fiche.territoire', 'territoire');
	territoireFilter(demandesFermeesZoneQuery);
	periodFilter('demande.dateCloture')(demandesFermeesZoneQuery);
	const demandesFermeesZone = await demandesFermeesZoneQuery.getCount();

	const zone = {
		devecos: { total: devecosTotalZone, nouveaux: devecosNouveauxZone, actifs: devecosActifsZone },
		fiches: {
			total: fichesTotalZone,
			nouvelles: fichesNouvellesZone,
			portefeuille: fichesTotalZone / devecosTotalZone,
		},
		echanges: { total: echangesTotalZone, nouveaux: echangesNouveauxZone },
		demandes: {
			total: demandesTotalZone,
			nouvelles: demandesNouvellesZone,
			fermees: demandesFermeesZone,
		},
	};
	ctx.body = { territoires, devecos, zone };
});

export default router;
