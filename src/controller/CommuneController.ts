import * as Koa from 'koa';
import Router from 'koa-router';

import AppDataSource from '../data-source';
import { Commune } from '../entity/Commune';
import AuthMiddleware from '../middleware/AuthMiddleware';

const routerOpts: Router.IRouterOptions = {
	prefix: '/communes',
};

const router: Router = new Router(routerOpts);
router.use(AuthMiddleware);

router.get('/', async (ctx: Koa.Context) => {
	const communes = await AppDataSource.getRepository(Commune).find();
	ctx.body = communes;
});

router.get('/:id', async (ctx: Koa.Context) => {
	const id = ctx.params.id;
	const commune = await AppDataSource.getRepository(Commune).findOne({ where: { id } });
	ctx.body = commune;
});

export default router;
