import { compareSync } from 'bcrypt';
import { StatusCodes } from 'http-status-codes';
import * as Koa from 'koa';
import Router from 'koa-router';
import { ILike } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';

import AppDataSource from '../data-source';
import { Account } from '../entity/Account';
import { Connexion } from '../entity/Connexion';
import { Password } from '../entity/Password';
import { cookieName, respondWithAccountCookie, respondWithDeletedCookie } from '../lib/cookie';
import { send, isSmtp } from '../lib/emailing';
import { verifyJwt } from '../lib/getJwt';

const routerOpts: Router.IRouterOptions = {
	prefix: '/auth',
};

const router: Router = new Router(routerOpts);

router.post('/jwt/:accessKey', async (ctx: Koa.Context) => {
	const accessKey = ctx.params.accessKey;
	const accountRepository = AppDataSource.getRepository(Account);

	const account = await accountRepository.findOneBy({
		accessKey,
	});

	if (!account) {
		ctx.status = StatusCodes.NOT_FOUND;
		ctx.body = {
			error: `no account for accessKey ${accessKey}`,
		};
		return;
	}

	accountRepository.update(account.id, {
		accessKey: null,
		accessKeyDate: null,
		lastLogin: new Date(),
	});
	await AppDataSource.getRepository(Connexion).insert({ account });

	await respondWithAccountCookie({ account, ctx });
});

router.get('/status', async (ctx: Koa.Context) => {
	const token = ctx.cookies.get(cookieName);
	if (token !== 'deleted') {
		const decoded = verifyJwt(token);
		if (decoded && typeof decoded !== 'string') {
			ctx.status = StatusCodes.OK;
			ctx.body = {
				token,
				...decoded,
			};
		} else {
			ctx.status = StatusCodes.UNAUTHORIZED;
		}
	} else {
		ctx.status = StatusCodes.NOT_FOUND;
	}
});

router.post('/logout', async (ctx: Koa.Context) => {
	await respondWithDeletedCookie({ ctx });
});

router.post('/login', async (ctx: Koa.Context) => {
	const body = ctx.request.body;
	const email = body.email;

	if (!email) {
		ctx.status = StatusCodes.BAD_REQUEST;
		ctx.body = {
			error: 'email is required',
		};
		return;
	}

	const accountRepository = AppDataSource.getRepository(Account);

	const account = await accountRepository.findOne({
		where: {
			email: ILike(`%${email}%`),
		},
	});

	if (!account) {
		ctx.status = StatusCodes.NOT_FOUND;
		ctx.body = {
			error: `no account for email ${email}`,
		};
		return;
	}

	const password = body.password;

	if (password) {
		const { password: currentEncryptedPassword } =
			(await AppDataSource.getRepository(Password).findOneBy({
				account: { id: account.id },
			})) ?? {};

		if (!currentEncryptedPassword) {
			ctx.status = StatusCodes.UNAUTHORIZED;
			return;
		}

		if (!compareSync(password, currentEncryptedPassword)) {
			ctx.status = StatusCodes.UNAUTHORIZED;
			return;
		}

		ctx.status = StatusCodes.OK;
		await AppDataSource.getRepository(Connexion).insert({ account });

		await respondWithAccountCookie({ account, ctx });
	} else {
		const accessKey = uuidv4();

		await accountRepository.update(account.id, {
			accessKey,
			accessKeyDate: new Date(),
		});

		const appUrl = process.env.APP_URL || '';

		// send email
		if (isSmtp()) {
			send({
				options: {
					to: email,
					subject: 'Accédez à votre espace Deveco',
				},
				template: 'loginRequest',
				params: [
					{
						url: {
							accessKey,
							appUrl,
						},
						account,
					},
				],
			}).catch((emailError) => {
				console.error(emailError);
			});
		} else {
			console.log('No SMTP config found');
		}

		ctx.status = StatusCodes.OK;
		ctx.body = {
			email,
			accessUrl: process.env.SANDBOX_LOGIN ? `/auth/jwt/${accessKey}` : '',
		};
	}
});

export default router;
