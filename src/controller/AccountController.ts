import { StatusCodes } from 'http-status-codes';
import * as Koa from 'koa';
import Router from 'koa-router';
import spacetime from 'spacetime';

import AppDataSource from '../data-source';
import { Account } from '../entity/Account';
import { DevEco } from '../entity/DevEco';
import { Fiche } from '../entity/Fiche';
import { Local } from '../entity/Local';
import { Password } from '../entity/Password';
import { Rappel } from '../entity/Rappel';
import { respondWithAccountCookie } from '../lib/cookie';
import AuthMiddleware from '../middleware/AuthMiddleware';

const routerOpts: Router.IRouterOptions = {
	prefix: '/compte',
};

const router: Router = new Router(routerOpts);
router.use(AuthMiddleware);

router.post('/', async (ctx: Koa.Context) => {
	const { nom, prenom } = ctx.request.body;

	const accountRepository = AppDataSource.getRepository(Account);

	await accountRepository.update(ctx.state.account.id, {
		nom,
		prenom,
	});

	const account = await accountRepository.findOneOrFail({ where: { id: ctx.state.account.id } });

	await respondWithAccountCookie({ account, ctx });
});

router.get('/collaborateurs', async (ctx: Koa.Context) => {
	const account = ctx.state.account as Account;
	const { territory } = ctx.state.deveco as DevEco;

	const devecoRepository = AppDataSource.getRepository(DevEco);

	const devecos = await devecoRepository.find({
		where: { territory: { id: territory.id } },
		relations: { account: true, territory: true },
	});

	const res = devecos
		.map((user) => ({ ...user.account, territoire: user.territory }))
		.filter(({ email }) => email !== account.email);

	ctx.body = res;
});

router.get('/stats', async (ctx: Koa.Context) => {
	const deveco = ctx.state.deveco as DevEco;

	const territoire = deveco.territory;

	const fichesRepository = AppDataSource.getRepository(Fiche);
	const fiches = await fichesRepository.find({
		where: { territoire: { id: territoire.id } },
		relations: { entite: true },
	});

	const bilanFiches = fiches.reduce(
		({ entreprises, personnes }, fiche) => {
			const e = fiche.entite.entiteType === 'PM' ? 1 : 0;
			const p = fiche.entite.entiteType === 'PP' ? 1 : 0;
			return { entreprises: entreprises + e, personnes: personnes + p };
		},
		{
			entreprises: 0,
			personnes: 0,
		}
	);

	const rappels = await AppDataSource.createQueryBuilder()
		.select('rappels')
		.from(Rappel, 'rappels')
		.leftJoin(Fiche, 'fiche', 'fiche.id = rappels.fiche_id')
		.where('fiche.territoire_id = :territoryId', { territoryId: territoire.id })
		.getMany();
	const now = spacetime.now().startOf('day');
	const bilanRappels = rappels.reduce(
		({ actifs, retard, fermes }, rappel) => {
			const a = !rappel.dateCloture ? 1 : 0;
			const r = !rappel.dateCloture && now.isAfter(new Date(rappel.dateRappel)) ? 1 : 0;
			const f = rappel.dateCloture ? 1 : 0;
			return { actifs: actifs + a, retard: retard + r, fermes: fermes + f };
		},
		{ actifs: 0, retard: 0, fermes: 0 }
	);

	const localRepository = AppDataSource.getRepository(Local);
	const locaux = await localRepository.find({
		where: {
			territoire: { id: territoire.id },
		},
	});
	const bilanLocaux = locaux.reduce(
		({ occupes, vacants }, local) => ({
			occupes: occupes + (local.localStatut === 'occupe' ? 1 : 0),
			vacants: vacants + (local.localStatut === 'vacant' ? 1 : 0),
		}),
		{
			occupes: 0,
			vacants: 0,
		}
	);

	const res = {
		fiches: bilanFiches,
		rappels: bilanRappels,
		locaux: bilanLocaux,
	};

	ctx.body = res;
});

router.post('/password', async (ctx: Koa.Context) => {
	const account: Account = ctx.state.account as Account;
	const { password } = ctx.request.body ?? {};

	if (!password) {
		ctx.status = StatusCodes.BAD_REQUEST;
		ctx.body = {};
		return;
	}

	const currentPassword = await AppDataSource.getRepository(Password).findOneBy({
		account: { id: account.id },
	});

	if (currentPassword) {
		await AppDataSource.getRepository(Password).update(
			{ account: { id: account.id } },
			{ password }
		);
	} else {
		await AppDataSource.getRepository(Password).insert({ account, password });
	}

	ctx.body = {};
});

export default router;
