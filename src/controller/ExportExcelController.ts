import * as Koa from 'koa';
import Router from 'koa-router';
import Excel, { Cell, Row, Worksheet } from 'exceljs';
import { In } from 'typeorm';

import AuthMiddleware from '../middleware/AuthMiddleware';
import AppDataSource from '../data-source';
import { DevEco } from '../entity/DevEco';
import { EventLog } from '../entity/EventLog';
import { Territory } from '../entity/Territory';
import { send } from '../lib/emailing';
import {
	formatJSDateToHumainFriendly,
	formatJSDateToPostgresDate,
	getMonthAgo,
	getNWeeksAgo,
} from '../lib/utils';
import ficheService from '../service/FicheService';
import inseeApiService from '../service/InseeService';
import { Etablissement } from '../entity/Etablissement';
import { fullCodeToFormeJuridique } from '../lib/formeJuridique';
import { codeToTrancheEffectifs } from '../lib/trancheEffectifs';

const routerOpts: Router.IRouterOptions = {
	prefix: '/export',
};

const router: Router = new Router(routerOpts);
router.use(AuthMiddleware);

router.post('/fiches', async (ctx: Koa.Context) => {
	const recherche = (ctx.query.recherche as string) || '';
	const type = (ctx.query.type as string) || ''; // can be "etablissement" or "particulier", needs to be mapped to "PM" and "PP"
	const activites = (ctx.query.activites as string[]) || [];
	const zones = (ctx.query.zones as string[]) || [];
	const demandes = (ctx.query.demandes as string[]) || [];
	const rawLocal = ctx.query.local as string;
	const local = Number.isNaN(Number(rawLocal)) ? 0 : Number(rawLocal);
	const codesCommune = (ctx.query.communes as string[]) || [];
	const mots = (ctx.query.mots as string[]) || [];
	const ids = ctx.request.body.ids?.split(',').filter(Boolean);
	const cp = (ctx.query.cp as string) || '';
	const rue = (ctx.query.rue as string) || '';

	const deveco = ctx.state.deveco as DevEco;

	const criteria = {
		recherche,
		type,
		activites,
		zones,
		demandes,
		local,
		codesCommune,
		territoire: deveco.territory.id,
		mots,
		cp,
		rue,
		ids,
	};

	const workbook = new Excel.Workbook();
	const wsContacts = workbook.addWorksheet('Contacts');
	wsContacts.columns = [
		{ header: 'Identifiant', key: 'id' },
		{ header: 'Siret', key: 'siret' },
		{ header: 'Dénomination', key: 'nomEntreprise' },
		{ header: 'Prénom', key: 'prenom' },
		{ header: 'Nom', key: 'nom' },
		{ header: 'Fonction', key: 'fonction' },
		{ header: 'Email', key: 'email' },
		{ header: 'Téléphone', key: 'telephone' },
	];

	if (type !== 'particulier') {
		const fiches = await ficheService.searchAll(criteria, 'PM');

		const wsEntreprise = workbook.addWorksheet('Entreprises');
		const wsDemandesEntreprise = workbook.addWorksheet('Demandes Entreprise');

		wsEntreprise.columns = [
			{ header: 'Siret', key: 'siret' },
			{ header: 'Forme juridique', key: 'formeJuridique' },
			{ header: 'Dénomination', key: 'nom' },
			{ header: 'Adresse', key: 'adresse' },
			{ header: 'Etat administratif', key: 'etatAdministratif' },
			{ header: 'Code NAF', key: 'codeNaf' },
			{ header: 'Libellé NAF', key: 'libelleNaf' },
			{ header: 'Activités réelles', key: 'activitesReelles' },
		];

		wsDemandesEntreprise.columns = [
			{ header: 'Siret', key: 'siret' },
			{ header: 'Dénomination', key: 'nomEntreprise' },
			{ header: 'Type de demande', key: 'typeDemande' },
			{ header: 'Cloture', key: 'cloture' },
			{ header: 'Motif', key: 'motif' },
			{ header: 'Date de cloture', key: 'dateCloture' },
		];

		fiches.forEach((fiche) => {
			wsEntreprise.addRow({
				...fiche.entite.entreprise,
				activitesReelles: fiche.entite.activitesReelles
					? fiche.entite.activitesReelles.join(', ')
					: '',
			});
			const contacts = (fiche.entite.contacts || []).map((contact) => ({
				...contact,
				...contact.particulier,
				siret: fiche.entite.entreprise?.siret,
				nomEntreprise: fiche.entite.entreprise?.nom,
			}));
			wsContacts.addRows(contacts);

			const demandes = (fiche.demandes || []).map((demande) => ({
				...demande,
				siret: fiche.entite.entreprise?.siret,
				nomEntreprise: fiche.entite.entreprise?.nom,
			}));
			wsDemandesEntreprise.addRows(demandes);
		});
	}

	if (type !== 'etablissement') {
		const fiches = await ficheService.searchAll(criteria, 'PP');

		const wsParticulier = workbook.addWorksheet('Particuliers');
		const wsDemandesParticulier = workbook.addWorksheet('Demandes Particulier');

		wsParticulier.columns = [
			{ header: 'Identifiant', key: 'id' },
			{ header: 'Nom', key: 'nom' },
			{ header: 'Prénom', key: 'prenom' },
			{ header: 'Email', key: 'email' },
			{ header: 'Téléphone', key: 'telephone' },
			{ header: 'Adresse', key: 'adresse' },
			{ header: 'Code postal', key: 'codePostal' },
			{ header: 'Commune', key: 'ville' },
			{ header: 'Activités réelles', key: 'activitesReelles' },
		];

		wsDemandesParticulier.columns = [
			{ header: 'Identifiant', key: 'id' },
			{ header: 'Particulier', key: 'particulier' },
			{ header: 'Type de demande', key: 'typeDemande' },
			{ header: 'Cloture', key: 'cloture' },
			{ header: 'Motif', key: 'motif' },
			{ header: 'Date de cloture', key: 'dateCloture' },
		];

		fiches.forEach((fiche) => {
			wsParticulier.addRow({
				...fiche.entite.particulier,
				activitesReelles: fiche.entite.activitesReelles
					? fiche.entite.activitesReelles.join(', ')
					: '',
			});
			const contacts = (fiche.entite.contacts || []).map((contact) => ({
				...contact,
				nomEntreprise: `${fiche.entite.particulier?.nom} ${fiche.entite.particulier?.prenom}`,
			}));
			wsContacts.addRows(contacts);

			const demandes = (fiche.demandes || []).map((demande) => ({
				...demande,
				particulier: `${fiche.entite.particulier?.nom} ${fiche.entite.particulier?.prenom}`,
			}));
			wsDemandesParticulier.addRows(demandes);
		});
	}

	const date = new Date();
	const filename = `deveco_export_${date.getTime()}`;

	ctx.set('Content-disposition', `attachment; filename=${filename}.xlsx`);
	ctx.set('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	ctx.status = 200;
	await workbook.xlsx.write(ctx.res);
	ctx.res.end();
});

router.get('/entreprises', async (ctx: Koa.Context) => {
	const deveco = ctx.state.deveco as DevEco;

	const recherche = ((ctx.query.recherche as string) || '').replaceAll("'", "''");
	const ferme = (ctx.query.ferme as string) || '';
	const codesNaf = (ctx.query.codesNaf as string[]) || [];
	const communes = (ctx.query.communes as string[]) || [];
	const tranchesEffectifs = (ctx.query.effectifs as string[]) || [];
	const formes = (ctx.query.formes as string) || '';
	const cp = (ctx.query.cp as string) || '';
	const rue = (ctx.query.rue as string) || '';

	const criteria = {
		recherche,
		ferme,
		codesNaf,
		communes,
		tranchesEffectifs,
		formes,
		territoire: deveco.territory.id,
		cp,
		rue,
	};

	const workbook = new Excel.Workbook();

	const [sirets] = await inseeApiService.search(criteria);
	const etablissements = [];

	while (sirets.length > 0) {
		const s = sirets.splice(0, 100);
		const es = await AppDataSource.getRepository(Etablissement).find({
			where: { siret: In(s) },
			order: { dateCreation: 'DESC' },
		});
		etablissements.push(...es);
	}

	const ws = workbook.addWorksheet('Entreprises');

	ws.columns = [
		{ header: 'SIRET', key: 'siret' },
		{ header: 'SIREN', key: 'siren' },
		{ header: 'Nom', key: 'nomPublic' },
		{ header: 'Sigle', key: 'sigle' },
		{ header: 'Adresse', key: 'adresseComplete' },
		{ header: 'Code postal', key: 'codePostal' },
		{ header: 'Effectifs', key: 'effectifs' },
		{ header: 'Date des effectifs', key: 'anneeEffectifs' },
		{ header: 'Code NAF', key: 'codeNaf' },
		{ header: 'Libellé NAF', key: 'libelleNaf' },
		{ header: 'Catégorie NAF', key: 'libelleCategorieNaf' },
		{ header: 'Date création', key: 'dateCreation' },
		{ header: 'État administratif', key: 'etatAdministratif' },
		{ header: 'Forme juridique', key: 'formeJuridique' },
	];

	etablissements.forEach((etablissement) => {
		ws.addRow({
			...etablissement,
			effectifs: codeToTrancheEffectifs(etablissement.trancheEffectifs ?? ''),
			codePostal: etablissement.adresseComplete?.slice(-5),
			formeJuridique: fullCodeToFormeJuridique(`${etablissement.categorieJuridique}`),
		});
	});

	const date = new Date();
	const filename = `deveco_export_entreprises_${date.getTime()}`;

	ctx.set('Content-disposition', `attachment; filename=${filename}.xlsx`);
	ctx.set('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	ctx.status = 200;
	await workbook.xlsx.write(ctx.res);
	ctx.res.end();
});

enum DEVECO_TYPE {
	INACTIF = 'inactif',
	DEMISSIONAIRE = 'démissionnaire',
	TESTEUR = 'testeur',
	REGULIER = 'régulier',
}

type DEVECO_STATS = {
	devecoId: number;
	nom: string;
	email: string;
	territory: string;
	territory_id: number;
	w1: number;
	w2: number;
	w3: number;
	w4: number;
	m1: number;
	total: number;
	type_utilisateur: DEVECO_TYPE;
};

type TERRITORY_STATS = {
	territory: string;
	total_nb_deveco: number;
	nb_inactif: number;
	nb_demissionaire: number;
	nb_testeur: number;
	nb_regulier: number;
	total_actions: number;
	m1: number;
	w1: number;
	actions_PP?: number;
	actions_PM?: number;
	actions_LOCAL?: number;
};

type DevecoActivityRaw = {
	total_stats: string;
	m1: string;
	nom: string;
	email: string;
	deveco_id: number;
	territory: string;
	territory_id: number;
	w1: string;
	w2: string;
	w3: string;
	w4: string;
};

async function getDevecoActivity(
	today: string,
	w1: string,
	w2: string,
	w3: string,
	w4: string,
	m1: string
): Promise<DevecoActivityRaw[]> {
	return await AppDataSource.getRepository(DevEco)
		.createQueryBuilder('D')
		.select('D.id', 'deveco_id')
		.addSelect('min(territory.id)', 'territory_id')
		.addSelect('min(territory.name)', 'territory')
		.addSelect("COALESCE((min(account.prenom)),'') ||' ' || COALESCE((min(account.nom)),'')", 'nom')
		.addSelect('COUNT(logs.action)', 'total_stats')
		.addSelect("COALESCE((min(account.email)),'')", 'email')
		.addSelect((subQuery) => {
			return subQuery
				.select('count(L.action)')
				.from(EventLog, 'L')
				.where(`L.date >= '${w1}' and L.date < '${today}' and L.deveco_id = D.id`)
				.groupBy('L.deveco_id');
		}, 'w1')
		.addSelect((subQuery) => {
			return subQuery
				.select('count(L.action)')
				.from(EventLog, 'L')
				.where(`L.date >= '${w2}' and L.date < '${w1}' and L.deveco_id = D.id`)
				.groupBy('L.deveco_id');
		}, 'w2')
		.addSelect((subQuery) => {
			return subQuery
				.select('count(L.action)')
				.from(EventLog, 'L')
				.where(`L.date >= '${w3}' and L.date < '${w2}' and L.deveco_id = D.id`)
				.groupBy('L.deveco_id');
		}, 'w3')
		.addSelect((subQuery) => {
			return subQuery
				.select('count(L.action)')
				.from(EventLog, 'L')
				.where(`L.date >= '${w4}' and L.date < '${w3}' and L.deveco_id = D.id`)
				.groupBy('L.deveco_id');
		}, 'w4')
		.addSelect((subQuery) => {
			return subQuery
				.select('count(L.action)')
				.from(EventLog, 'L')
				.where(`L.date >= '${m1}' and L.date < '${today}' and L.deveco_id = D.id`)
				.groupBy('L.deveco_id');
		}, 'm1')
		.innerJoin('D.account', 'account')
		.leftJoin('D.territory', 'territory')
		.leftJoin('event_log', 'logs', 'D.id=logs.deveco_id')
		.groupBy('D.id')
		.orderBy('D.id', 'ASC')
		.getRawMany();
}

const activeForTheWeek = (weeklyActivity: number): boolean => weeklyActivity > 5;

const activeForFourWeeks = ({
	w1,
	w2,
	w3,
	w4,
}: {
	w1: number;
	w2: number;
	w3: number;
	w4: number;
}): boolean => {
	const [a1, a2, a3, a4] = [w1, w2, w3, w4].map(activeForTheWeek);

	return (a1 && !a2 && !a3 && !a4) || (a1 && a2 && !a3 && !a4) || (a1 && a2 && a3);
};

function getTypeUtilisateur({
	total,
	m1,
	w1,
	w2,
	w3,
	w4,
}: {
	total?: number;
	m1?: number;
	w1: number;
	w2: number;
	w3: number;
	w4: number;
}) {
	if (total && total > 2) {
		if (activeForFourWeeks({ w1, w2, w3, w4 })) {
			return DEVECO_TYPE.REGULIER;
		}
		if (total >= 2 && (m1 === undefined || m1 === 0)) {
			return DEVECO_TYPE.DEMISSIONAIRE;
		}
		return DEVECO_TYPE.TESTEUR;
	}
	return DEVECO_TYPE.INACTIF;
}

async function combineDevecoStats(): Promise<DEVECO_STATS[]> {
	const now = new Date();
	const oneWeekAgo = formatJSDateToPostgresDate(getNWeeksAgo(now, 1));
	const twoWeeksAgo = formatJSDateToPostgresDate(getNWeeksAgo(now, 2));
	const threeWeeksAgo = formatJSDateToPostgresDate(getNWeeksAgo(now, 3));
	const fourWeeksAgo = formatJSDateToPostgresDate(getNWeeksAgo(now, 4));
	const monthAgo = formatJSDateToPostgresDate(getMonthAgo(now));
	const today = formatJSDateToPostgresDate(now);

	const devecoActivity = await getDevecoActivity(
		today,
		oneWeekAgo,
		twoWeeksAgo,
		threeWeeksAgo,
		fourWeeksAgo,
		monthAgo
	);

	const devecoStats = devecoActivity.map((item: DevecoActivityRaw): DEVECO_STATS => {
		const total = item.total_stats ? parseInt(item.total_stats) : 0;
		const m1 = item.m1 ? parseInt(item.m1) : 0;
		const email = item.email;
		const nom = item.nom && item.nom.replace(/ /g, '') !== '' ? item.nom : '-';
		const w1 = item.w1 ? parseInt(item.w1) : 0;
		const w2 = item.w2 ? parseInt(item.w2) : 0;
		const w3 = item.w3 ? parseInt(item.w3) : 0;
		const w4 = item.w4 ? parseInt(item.w4) : 0;

		return {
			devecoId: item.deveco_id,
			email,
			nom,
			territory: item.territory,
			territory_id: item.territory_id,
			w1,
			w2,
			w3,
			w4,
			m1,
			total,
			type_utilisateur: getTypeUtilisateur({ total, m1, w1, w2, w3, w4 }),
		};
	});
	return devecoStats;
}

function combineTerritoryStats(
	deveco_page: DEVECO_STATS[],
	territoryPPPMLOCALstats: TerritoryStatsRaw[]
) {
	const result1: Record<number, TERRITORY_STATS> = {};
	deveco_page.forEach((item: DEVECO_STATS) => {
		if (Object.prototype.hasOwnProperty.call(result1, item.territory_id)) {
			const t = { ...result1[item.territory_id] };
			result1[item.territory_id] = {
				territory: t.territory,
				total_nb_deveco: t.total_nb_deveco + 1,
				nb_inactif: item.type_utilisateur === DEVECO_TYPE.INACTIF ? t.nb_inactif + 1 : t.nb_inactif,
				nb_demissionaire:
					item.type_utilisateur === DEVECO_TYPE.DEMISSIONAIRE
						? t.nb_demissionaire + 1
						: t.nb_demissionaire,
				nb_testeur: item.type_utilisateur === DEVECO_TYPE.TESTEUR ? t.nb_testeur + 1 : t.nb_testeur,
				nb_regulier:
					item.type_utilisateur === DEVECO_TYPE.REGULIER ? t.nb_regulier + 1 : t.nb_regulier,
				total_actions: t.total_actions + item.total,
				m1: t.m1 + item.m1,
				w1: t.w1 + item.w1,
			};
		} else {
			result1[item.territory_id] = {
				territory: item.territory,
				total_nb_deveco: 1,
				nb_inactif: item.type_utilisateur === DEVECO_TYPE.INACTIF ? 1 : 0,
				nb_demissionaire: item.type_utilisateur === DEVECO_TYPE.DEMISSIONAIRE ? 1 : 0,
				nb_testeur: item.type_utilisateur === DEVECO_TYPE.TESTEUR ? 1 : 0,
				nb_regulier: item.type_utilisateur === DEVECO_TYPE.REGULIER ? 1 : 0,
				total_actions: item.total,
				m1: item.m1,
				w1: item.w1,
			};
		}
	});

	const result: TERRITORY_STATS[] = [];

	territoryPPPMLOCALstats.forEach((item) => {
		const t = result1[item.tid];
		if (t) {
			result.push({
				...t,
				actions_PP: item.actions_PP ? item.actions_PP : 0,
				actions_PM: item.actions_PM ? item.actions_PM : 0,
				actions_LOCAL: item.actions_LOCAL ? item.actions_LOCAL : 0,
			});
		} else {
			result.push({
				territory: item.tname,
				total_nb_deveco: 0,
				nb_inactif: 0,
				nb_demissionaire: 0,
				nb_testeur: 0,
				nb_regulier: 0,
				total_actions: 0,
				m1: 0,
				w1: 0,
				actions_PP: 0,
				actions_PM: 0,
				actions_LOCAL: 0,
			});
		}
	});

	return result;
}

type TerritoryStatsRaw = {
	tid: number;
	tname: string;
	actions_PM: number;
	actions_PP: number;
	actions_LOCAL: number;
};

async function getTerritoryPPPMLOCALstats(): Promise<TerritoryStatsRaw[]> {
	return await AppDataSource.getRepository(Territory)
		.createQueryBuilder('T')
		.select('T.id', 'tid')
		.addSelect('T.name', 'tname')
		.addSelect((subQuery) => {
			return subQuery
				.select('count(L.entity_type)')
				.from(EventLog, 'L')
				.leftJoin('L.deveco', 'D')
				.where("L.entity_type='FICHE_PM' and D.territory_id = T.id")
				.groupBy('D.territory_id');
		}, 'actions_PM')
		.addSelect((subQuery) => {
			return subQuery
				.select('count(L.entity_type)')
				.from(EventLog, 'L')
				.leftJoin('L.deveco', 'D')
				.where("L.entity_type='FICHE_PP' and D.territory_id = T.id")
				.groupBy('D.territory_id');
		}, 'actions_PP')
		.addSelect((subQuery) => {
			return subQuery
				.select('count(L.entity_type)')
				.from(EventLog, 'L')
				.leftJoin('L.deveco', 'D')
				.where("L.entity_type='LOCAL' and D.territory_id = T.id")
				.groupBy('D.territory_id');
		}, 'actions_LOCAL')
		.getRawMany();
}

function applyStylesToWorksheet(w: Worksheet) {
	w.eachRow(function (Row: Row, rowNum: number) {
		Row.eachCell(function (Cell: Cell, cellNum: number) {
			if (rowNum === 1) {
				Cell.alignment = {
					vertical: 'middle',
					horizontal: 'center',
				};
			} else if (cellNum === 1) {
				Cell.alignment = {
					vertical: 'middle',
					horizontal: 'left',
				};
			} else {
				Cell.alignment = {
					vertical: 'middle',
					horizontal: 'right',
				};
			}
		});
	});
}

function createExcelFile(deveco_page: DEVECO_STATS[], territory_page: TERRITORY_STATS[]) {
	const workbook = new Excel.Workbook();
	const wsDeveco = workbook.addWorksheet('Deveco');
	const wsTerritory = workbook.addWorksheet('Territory');

	wsDeveco.columns = [
		{ header: 'Id', key: 'id', width: 5 },
		{ header: 'Email', key: 'email', width: 25 },
		{ header: 'Nom', key: 'nom', width: 25 },
		{ header: 'Territoire', key: 'territory', width: 25 },
		{ header: 'Nb actions S-4', key: 'w4', width: 15 },
		{ header: 'Nb actions S-3', key: 'w3', width: 15 },
		{ header: 'Nb actions S-2', key: 'w2', width: 15 },
		{ header: 'Nb actions S-1', key: 'w1', width: 15 },
		{ header: 'Total Nb action M-1', key: 'm1', width: 15 },
		{ header: 'Type utilisateur', key: 'type_utilisateur', width: 15 },
	];

	wsTerritory.columns = [
		{ header: 'Territoire', key: 'territory', width: 25 },
		{ header: 'Nb développeurs:', key: 'total_nb_deveco', width: 15 },
		{ header: 'Nb inactif', key: 'nb_inactif', width: 15 },
		{ header: 'Nb testeur', key: 'nb_testeur', width: 15 },
		{ header: 'Nb démissionnaire', key: 'nb_demissionaire', width: 15 },
		{ header: 'Nb réguliers', key: 'nb_regulier', width: 15 },
		{ header: 'Total Nb actions depuis début', key: 'total_actions', width: 15 },
		{ header: 'Total Nb actions M-1', key: 'm1', width: 15 },
		{ header: 'Total Nb actions S-1', key: 'w1', width: 15 },
		{ header: 'Nb actions fiches PM depuis le début', key: 'actions_PP', width: 15 },
		{ header: 'Nb actions fiches PP depuis le début', key: 'actions_PM', width: 15 },
		{ header: 'Nb actions fiches locaux depuis le début', key: 'actions_LOCAL', width: 15 },
	];

	wsDeveco.addRows(deveco_page);
	wsTerritory.addRows(territory_page);

	applyStylesToWorksheet(wsDeveco);
	applyStylesToWorksheet(wsTerritory);

	return workbook;
}

/* Si tu as fait plus de 5 actions par semaine pendant 4 semaines sur deveco, tu es actif */
export async function exportActivityLogs() {
	const deveco_page = await combineDevecoStats();
	const territoryPPPMLOCALstats = await getTerritoryPPPMLOCALstats();

	const territory_page = combineTerritoryStats(deveco_page, territoryPPPMLOCALstats);
	return createExcelFile(deveco_page, territory_page);
}

export async function exportActivityLogsAndSendEmails() {
	const emails = process.env.LOGS_ACTIVITY_DESTINATION;
	if (emails) {
		const workbook = await exportActivityLogs();
		const date = new Date();
		const filename = `deveco_activity_export_${date.getTime()}`;
		(workbook.xlsx.writeBuffer() as Promise<Buffer>).then((data) => {
			send({
				options: {
					to: emails,
					subject: `Activité beta testeurs hebdomadaire ${formatJSDateToHumainFriendly(date)}`,
					attachments: [
						{
							filename: `${filename}.xlsx`,
							contentType: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
							content: data,
						},
					],
				},
				template: 'exportWeeklyActivity',
				params: [],
			})
				.catch((emailError) => {
					console.error(emailError);
				})
				.finally(() => {
					console.log('successfully sent to ', process.env.LOGS_ACTIVITY_DESTINATION);
				});
		});
	} else {
		console.log('[ExportExcelController] LOGS_ACTIVITY_DESTINATION is not defined');
	}
}

router.get('/logs', async (ctx: Koa.Context) => {
	const workbook = await exportActivityLogs();
	const date = new Date();
	const filename = `deveco_activity_export_${date.getTime()}`;
	ctx.set('Content-disposition', 'attachment; filename=' + filename + '.xlsx');
	ctx.set('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	ctx.status = 200;
	await workbook.xlsx.write(ctx.res);
	ctx.res.end();
});

export default router;
