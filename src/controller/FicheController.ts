import { StatusCodes } from 'http-status-codes';
import * as Koa from 'koa';
import Router from 'koa-router';
import { FindOptionsWhere } from 'typeorm';

import { Fiche } from '../entity/Fiche';
import { Particulier } from '../entity/Particulier';
import AuthMiddleware from '../middleware/AuthMiddleware';
import entrepriseService from '../service/EntrepriseService';
import { Echange } from '../entity/Echange';
import activiteEntrepriseService from '../service/ActiviteEntrepriseService';
import ficheService from '../service/FicheService';
import { FicheView, toView } from '../view/FicheView';
import qpvService from '../service/QpvService';
import { Qpv } from '../entity/Qpv';
import localisationEntrepriseService from '../service/LocalisationEntrepriseService';
import motCleService from '../service/MotCleService';
import { Demande } from '../entity/Demande';
import { ActiviteEntreprise } from '../entity/ActiviteEntreprise';
import { LocalisationEntreprise } from '../entity/LocalisationEntreprise';
import { FicheArchive } from '../entity/FicheArchive';
import AppDataSource from '../data-source';
import eventLogService from '../service/EventLogService';
import { DevEco } from '../entity/DevEco';
import { EventLogActionType, EventLogEntityType } from '../entity/EventLog';
import { getLogEntityType } from '../lib/utils';
import { Entite } from '../entity/Entite';
import { MotCle } from '../entity/MotCle';

const routerOpts: Router.IRouterOptions = {
	prefix: '/fiches',
};

const router: Router = new Router(routerOpts);
router.use(AuthMiddleware);

async function ficheToView(where: FindOptionsWhere<Fiche>): Promise<[FicheView, Fiche]> {
	const fiche = await ficheService.loadFiche(where);
	let qpv: Qpv | null = null;
	if (fiche.entite.entreprise) {
		const { longitude, latitude } = fiche.entite.entreprise;
		qpv = await qpvService.getQpv(longitude, latitude);
	}

	return [toView(fiche, qpv ? [qpv] : []), fiche];
}

const resultsPerPage = 20;

router.post('/batch', async (ctx: Koa.Context) => {
	const deveco = ctx.state.deveco as DevEco;

	const activites = ctx.request.body?.activites ?? [];
	const localisations = ctx.request.body?.localisations ?? [];
	const mots = ctx.request.body?.mots ?? [];
	const fiches = ctx.request.body?.fiches ?? [];

	await activiteEntrepriseService.createIfNecessary(activites, deveco.territory);
	await localisationEntrepriseService.createIfNecessary(localisations, deveco.territory);
	await motCleService.createIfNecessary(mots, deveco.territory);

	const batchResults = [];
	for (const ficheId of fiches) {
		const entite = await AppDataSource.getRepository(Entite).findOne({
			where: { fiche: { id: ficheId } },
		});
		if (!entite) {
			continue;
		}
		let statut;
		try {
			await AppDataSource.getRepository(Entite).update(entite.id, {
				activitesReelles: [...(entite.activitesReelles ?? []), ...activites],
				entrepriseLocalisations: [...(entite.entrepriseLocalisations ?? []), ...localisations],
				motsCles: [...(entite.motsCles ?? []), ...mots],
			});
			statut = true;
		} catch (e) {
			console.log('Error updating Entite', e);
			statut = false;
		}

		const fiche = await ficheService.ficheToView({ id: ficheId });

		batchResults.push({ fiche, statut });
	}
	const searchResults = await doSearch(ctx);
	ctx.body = { ...searchResults, batchResults };
});

const doSearch = async (ctx: Koa.Context) => {
	const rawPage = ctx.query.page as unknown;
	let page = Number.isNaN(Number(rawPage)) ? 1 : Number(rawPage);
	const recherche = ((ctx.query.recherche as string) || '').replace(`'`, `''`);
	const type = (ctx.query.type as string) || ''; // can be "etablissement" or "particulier", needs to be mapped to "PM" and "PP"
	const activites = (ctx.query.activites as string[]) || [];
	const zones = (ctx.query.zones as string[]) || [];
	const demandes = (ctx.query.demandes as string[]) || [];
	const rawLimit = ctx.query.limit as string;
	const limit = Number.isNaN(Number(rawLimit)) ? 0 : Number(rawLimit);
	const rawLocal = ctx.query.local as string;
	const local = Number.isNaN(Number(rawLocal)) ? 0 : Number(rawLocal);
	const codesCommune = (ctx.query.communes as string[]) || [];
	const mots = (ctx.query.mots as string[]) || [];
	const cp = (ctx.query.cp as string) || '';
	const rue = (ctx.query.rue as string) || '';

	const deveco = ctx.state.deveco as DevEco;

	const criteria = {
		recherche,
		type,
		activites,
		zones,
		demandes,
		local,
		codesCommune,
		territoire: deveco.territory.id,
		mots,
		cp,
		rue,
	};

	let [fiches, totalResults] = await ficheService.search(criteria, page, resultsPerPage, limit);

	if (fiches.length == 0 && totalResults > 0) {
		page = 1;
		[fiches, totalResults] = await ficheService.search(criteria, page, resultsPerPage, limit);
	}

	const fullPages = Math.floor(totalResults / resultsPerPage);
	const pages = fullPages + (totalResults - fullPages * resultsPerPage > 0 ? 1 : 0);

	return { page, pages, results: totalResults, data: fiches.map((fiche) => toView(fiche)) };
};

// Search for Fiches
router.get('/', async (ctx: Koa.Context) => {
	const searchResult = await doSearch(ctx);
	ctx.body = searchResult;
});

// Update Fiche
router.post('/:id/modifier', async (ctx: Koa.Context) => {
	const id = ctx.params.id;
	const { particulier } = ctx.request.body;
	const activitesReelles = ctx.request.body?.activites ?? [];
	const localisations = ctx.request.body?.localisations ?? [];
	const motsCles = ctx.request.body?.mots ?? [];
	const activiteAutre = ctx.request.body?.autre ?? [];
	const deveco = ctx.state.deveco as DevEco;

	const { futureEnseigne, ...part } = particulier ?? {};

	const ficheRepository = AppDataSource.getRepository(Fiche);
	const entiteRepository = AppDataSource.getRepository(Entite);

	const fiche = await ficheRepository.findOneOrFail({
		relations: {
			entite: {
				particulier: true,
				entreprise: true,
			},
		},
		where: { id },
	});

	if (fiche.entite.particulier?.id) {
		// Personne Physique
		const particulierRepository = AppDataSource.getRepository(Particulier);
		await particulierRepository.update(fiche.entite.particulier.id, part);
	}

	await activiteEntrepriseService.createIfNecessary(activitesReelles, deveco.territory);
	await motCleService.createIfNecessary(motsCles, deveco.territory);
	await localisationEntrepriseService.createIfNecessary(localisations, deveco.territory);

	const now = new Date();

	await ficheRepository.update(id, {
		dateModification: now,
		updatedAt: now,
		auteurModification: deveco,
	});

	await entiteRepository.update(fiche.entite.id, {
		futureEnseigne,
		activitesReelles,
		activiteAutre,
		entrepriseLocalisations: localisations,
		motsCles,
		updatedAt: now,
		auteurModification: deveco,
	});

	await eventLogService.addEventLog({
		action: EventLogActionType.UPDATE,
		deveco,
		entityType: getLogEntityType(fiche),
		entityId: id,
	});

	const [ficheView] = await ficheToView({ id });
	ctx.body = ficheView;
});

router.get('/filtersSource', async (ctx: Koa.Context) => {
	const deveco = ctx.state.deveco as DevEco;

	const territoire = deveco.territory;

	const activitesReelles = await AppDataSource.getRepository(ActiviteEntreprise)
		.createQueryBuilder('activite')
		.where(`activite.territory_id = ${territoire.id}`)
		.orderBy('activite.activite', 'ASC')
		.getMany();

	const activiteTypes = activitesReelles.map((a) => a.activite);

	const demandes = await AppDataSource.getRepository(Demande)
		.createQueryBuilder('demande')
		.innerJoin('demande.fiche', 'fiche')
		.where(`fiche.territoire_id = ${territoire.id}`)
		.andWhere(`(demande.cloture = false or demande.cloture is null)`)
		.select('distinct(demande.type_demande)')
		.orderBy('demande.type_demande', 'ASC')
		.getRawMany();

	const demandeTypes = demandes.map((d) => d.type_demande);

	const zones = await AppDataSource.getRepository(LocalisationEntreprise)
		.createQueryBuilder('zone')
		.where(`zone.territory_id = ${territoire.id}`)
		.orderBy('zone.localisation', 'ASC')
		.getMany();

	const zoneTypes = zones.map((zone) => zone.localisation);

	const mots = await AppDataSource.getRepository(MotCle)
		.createQueryBuilder('mot')
		.where(`mot.territory_id = ${territoire.id}`)
		.orderBy('mot.motCle', 'ASC')
		.getMany();

	const motTypes = mots.map((mot) => mot.motCle);

	ctx.body = {
		activites: activiteTypes,
		demandes: demandeTypes,
		zones: zoneTypes,
		mots: motTypes,
	};
});

const createFicheForSiret = async ({
	createur,
	siret,
	skipApiEntreprise,
}: {
	createur: DevEco;
	siret: string;
	skipApiEntreprise?: boolean;
}): Promise<number | undefined> => {
	const territoire = createur.territory;
	const ficheRepository = AppDataSource.getRepository(Fiche);
	const now = new Date();
	const entite = await entrepriseService.getOrCreateEntrepriseAndEntite({
		createur,
		siret,
		skipApiEntreprise,
	});
	const existing = await ficheRepository.findOne({
		where: { territoire: { id: territoire.id }, entite: { id: entite.id } },
	});
	if (existing) {
		return existing.id;
	}
	const fiche = ficheRepository.create({
		territoire,
		createur,
		entite,
		dateModification: now,
		auteurModification: createur,
		createdAt: now,
		updatedAt: now,
	});
	const { id } = await ficheRepository.save(fiche);
	return id;
};

router.post('/creerBatch', async (ctx: Koa.Context) => {
	const { email, sirets } = ctx.request.body;

	if (!email) {
		ctx.status = StatusCodes.BAD_REQUEST;
		ctx.body = {
			error: "La clef 'email' contenant l'email du Deveco créateur des fiches est obligatoire.",
		};
		return;
	}

	if (!sirets || !Array.isArray(sirets) || sirets.length === 0) {
		ctx.status = StatusCodes.BAD_REQUEST;
		ctx.body = { error: "La clef 'sirets' doit contenir une liste de SIRETs" };
		return;
	}

	const createur = await AppDataSource.getRepository(DevEco).findOne({
		where: { account: { email } },
		relations: { territory: true },
	});

	if (!createur) {
		ctx.status = StatusCodes.BAD_REQUEST;
		ctx.body = {
			error: 'Aucun DevEco trouvé avec cet email',
		};
		return;
	}

	const fiches = [];
	const failed = [];

	for (const siret of sirets) {
		try {
			const id = await createFicheForSiret({ createur, siret, skipApiEntreprise: true });
			fiches.push(id);
		} catch (e) {
			failed.push(siret);
		}
	}

	console.log('Failed inserting the following SIRETs', failed.join(', '));

	ctx.body = { fiches };
});

// Create Fiche
router.post('/creer', async (ctx: Koa.Context) => {
	const {
		particulier: particulierData,
		entreprise: entrepriseData,
		ficheType: entiteType,
		echange: echangeData,
		activites: activitesReelles,
	} = ctx.request.body;

	const createur = ctx.state.deveco as DevEco;
	const territoire = createur.territory;

	const ficheRepository = AppDataSource.getRepository(Fiche);
	const entiteRepository = AppDataSource.getRepository(Entite);

	let ficheId: number | undefined = undefined;

	const now = new Date();
	if (entiteType == 'PM') {
		const { siret } = entrepriseData;
		ficheId = await createFicheForSiret({ createur, siret });
		const [ficheView] = await ficheToView({ id: ficheId });
		ctx.body = ficheView;
	} else if (entiteType == 'PP') {
		const {
			nom,
			prenom,
			email,
			telephone,
			adresse,
			ville,
			codePostal,
			geolocation,
			description,
			futureEnseigne,
		} = particulierData;
		const { compteRendu, date, type, themes, titre } = echangeData;
		const echange: Echange = {
			createur,
			titre: titre as string,
			typeEchange: type as string,
			dateEchange: date as Date,
			compteRendu: compteRendu as string,
			themes: (themes as string[]) || [],
			createdAt: now,
			updatedAt: null,
		};
		const newEntite = entiteRepository.create({
			territoire,
			createur,
			entiteType,
			particulier: {
				nom: nom as string,
				prenom: prenom as string,
				email: email as string,
				telephone: telephone as string,
				adresse: adresse as string,
				ville: ville as string,
				codePostal: codePostal as string,
				geolocation: geolocation as string,
				description: description as string,
			},
			futureEnseigne: futureEnseigne as string,
			activitesReelles,
			auteurModification: createur,
			createdAt: now,
			updatedAt: now,
		});
		const entite = await entiteRepository.save(newEntite);
		const fiche = ficheRepository.create({
			territoire,
			createur,
			entite,
			demandes: (themes || []).map((demande: string) => ({
				typeDemande: demande,
			})),
			echanges: [echange],
			dateModification: now,
			auteurModification: createur,
			createdAt: now,
			updatedAt: now,
		});

		await activiteEntrepriseService.createIfNecessary(activitesReelles, createur.territory);
		const { id } = await ficheRepository.save(fiche);
		ficheId = id;
		const [ficheView] = await ficheToView({ id, territoire: { id: territoire.id } });
		ctx.body = ficheView;
	} else {
		ctx.throw(StatusCodes.BAD_REQUEST, 'bad fiche type');
	}

	if (ficheId) {
		await eventLogService.addEventLog({
			action: EventLogActionType.CREATE,
			deveco: createur,
			entityType:
				entiteType === 'PP'
					? EventLogEntityType.PERSONNE_PHYSIQUE
					: EventLogEntityType.PERSONNE_MORALE,
			entityId: ficheId,
		});
	}
});

// Consulting Fiche
router.get('/:id', async (ctx: Koa.Context) => {
	const id = ctx.params.id;
	const deveco = ctx.state.deveco as DevEco;
	const territoire = deveco.territory;
	const [ficheView, fiche] = await ficheToView({ id, territoire: { id: territoire.id } });
	await eventLogService.addEventLog({
		action: EventLogActionType.VIEW,
		deveco,
		entityType: getLogEntityType(fiche),
		entityId: id,
	});
	ctx.body = ficheView;
});

// Remove Fiche
router.del('/:id', async (ctx: Koa.Context) => {
	const id = ctx.params.id;
	const deveco = ctx.state.deveco as DevEco;
	const territoire = deveco.territory;

	const fiche = await AppDataSource.getRepository(Fiche).findOneOrFail({
		where: { id, territoire: { id: territoire.id } },
		relations: {
			entite: { contacts: true },
			demandes: true,
			echanges: true,
			rappels: true,
		},
	});

	await AppDataSource.getRepository(FicheArchive).insert({ content: JSON.stringify(fiche) });
	await AppDataSource.getRepository(Fiche).delete({ id, territoire });

	await eventLogService.addEventLog({
		action: EventLogActionType.DELETE,
		deveco,
		entityType: getLogEntityType(fiche),
		entityId: id,
	});

	ctx.body = {};
});

export default router;
