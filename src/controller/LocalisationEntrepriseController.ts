import * as Koa from 'koa';
import Router from 'koa-router';
import { ILike } from 'typeorm';

import { LocalisationEntreprise } from '../entity/LocalisationEntreprise';
import { DevEco } from '../entity/DevEco';
import AuthMiddleware from '../middleware/AuthMiddleware';
import AppDataSource from '../data-source';

const routerOpts: Router.IRouterOptions = {
	prefix: '/localisations',
};

const router: Router = new Router(routerOpts);
router.use(AuthMiddleware);

router.get('/', async (ctx: Koa.Context) => {
	const { territory } = ctx.state.deveco as DevEco;
	const query = ctx.query;
	const search = query.search as string;

	const localisations = await AppDataSource.getRepository(LocalisationEntreprise).find({
		order: { localisation: 'ASC' },
		where: {
			territory: { id: territory.id },
			localisation: ILike(`%${search}%`),
		},
		take: 5,
	});

	ctx.body = localisations;
});

export default router;
