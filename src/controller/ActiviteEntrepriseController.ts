import * as Koa from 'koa';
import Router from 'koa-router';
import { ILike } from 'typeorm';

import AppDataSource from '../data-source';
import { ActiviteEntreprise } from '../entity/ActiviteEntreprise';
import { DevEco } from '../entity/DevEco';
import AuthMiddleware from '../middleware/AuthMiddleware';

const routerOpts: Router.IRouterOptions = {
	prefix: '/activites',
};

const router: Router = new Router(routerOpts);
router.use(AuthMiddleware);

router.get('/', async (ctx: Koa.Context) => {
	const { territory } = ctx.state.deveco as DevEco;
	const query = ctx.query;
	const search = query.search as string;

	const activites = await AppDataSource.getRepository(ActiviteEntreprise).find({
		order: { activite: 'ASC' },
		where: {
			territory: { id: territory.id },
			activite: ILike(`%${search}%`),
		},
	});

	ctx.body = activites;
});

export default router;
