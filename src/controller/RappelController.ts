import { StatusCodes } from 'http-status-codes';
import * as Koa from 'koa';
import Router from 'koa-router';
import { In } from 'typeorm';

import AuthMiddleware from '../middleware/AuthMiddleware';
import { DevEco } from '../entity/DevEco';
import { Rappel } from '../entity/Rappel';
import { Fiche } from '../entity/Fiche';
import ficheService from '../service/FicheService';
import eventLogService from '../service/EventLogService';
import AppDataSource from '../data-source';
import { getLogEntityType } from '../lib/utils';
import { EventLogActionType, EventLogEntityType } from '../entity/EventLog';

const routerOpts: Router.IRouterOptions = {
	prefix: '/rappels',
};

const router: Router = new Router(routerOpts);
router.use(AuthMiddleware);

async function getRappelsForDeveco(deveco: DevEco): Promise<{ rappel: Rappel; fiche: Fiche }[]> {
	const territoire = deveco.territory;

	const entityManager = AppDataSource.manager;
	const stuff: { fiche_id: number; rappel_id: number }[] = await entityManager.query(
		`SELECT rappel.id as rappel_id, fiche.id as fiche_id FROM "rappel" JOIN "fiche" ON fiche.id = rappel.fiche_id WHERE fiche.territoire_id = $1`,
		[territoire.id]
	);

	const ficheIds = stuff.map(({ fiche_id }) => fiche_id);
	const fiches = await AppDataSource.getRepository(Fiche).find({
		where: { id: In(ficheIds) },
		relations: {
			entite: {
				particulier: true,
				entreprise: true,
			},
		},
	});
	const fichesById = fiches.reduce(
		(acc, fiche) => ({ ...acc, [fiche.id || '']: fiche }),
		<Record<number, Fiche>>{}
	);
	const rappelIds = stuff.map(({ rappel_id }) => rappel_id);
	const rappels = await AppDataSource.getRepository(Rappel).findBy({
		id: In(rappelIds),
	});
	const rappelsById = rappels.reduce(
		(acc, rappel) => ({ ...acc, [rappel.id || '']: rappel }),
		<Record<number, Rappel>>{}
	);

	const response = stuff.map(({ rappel_id, fiche_id }) => ({
		rappel: rappelsById[rappel_id],
		fiche: fichesById[fiche_id],
	}));

	return response;
}

async function ensureRappelIsInDevecoTerritory({
	rappelId,
	deveco,
	ctx,
}: {
	rappelId: number;
	deveco: DevEco;
	ctx: Koa.Context;
}): Promise<Rappel> {
	const rappelRepository = await AppDataSource.getRepository(Rappel);

	const rap = await rappelRepository.findOneOrFail({
		where: { id: rappelId },
		relations: { fiche: true },
	});

	const fiche = await AppDataSource.getRepository(Fiche).findOne({
		where: { id: rap?.fiche?.id },
		relations: { territoire: true },
	});

	if (fiche && fiche.territoire.id !== deveco.territory.id) {
		ctx.throw(StatusCodes.BAD_REQUEST, 'fiche is not in your territory');
	}

	return rap;
}

router.get('/', async (ctx: Koa.Context) => {
	const deveco = ctx.state.deveco as DevEco;
	ctx.body = await getRappelsForDeveco(deveco);
});

router.post('/creer', async (ctx: Koa.Context) => {
	const { dateRappel, titre } = ctx.request.body;

	const createur = ctx.state.deveco as DevEco;

	const ficheIdAsString = (ctx.query.ficheId as string) || '';
	const ficheId = parseInt(ficheIdAsString);
	if (!ficheId) {
		ctx.throw(StatusCodes.BAD_REQUEST, 'ficheId is required');
	}
	const fiche = await AppDataSource.getRepository(Fiche).findOneOrFail({
		where: {
			id: ficheId,
			territoire: { id: createur.territory.id },
		},
	});

	const rappelRepository = AppDataSource.getRepository(Rappel);

	const rappel: Rappel = {
		createur,
		titre: titre as string,
		dateRappel: dateRappel as Date,
		fiche,
	};
	await rappelRepository.save(rappel);
	await AppDataSource.getRepository(Fiche).update(ficheId, {
		updatedAt: new Date(),
		auteurModification: createur,
	});

	await eventLogService.addEventLog({
		action: EventLogActionType.UPDATE,
		deveco: createur,
		entityType: getLogEntityType(fiche),
		entityId: ficheId,
	});

	ctx.body = await ficheService.ficheToView({ id: ficheId });
});

router.post('/:id/modifier', async (ctx: Koa.Context) => {
	const deveco = ctx.state.deveco as DevEco;

	const id = parseInt(ctx.params.id);
	if (!id) {
		ctx.throw(StatusCodes.BAD_REQUEST, 'id is required');
	}

	const { rappel } = ctx.request.body;
	if (rappel.dateCloture) {
		rappel.auteurCloture = deveco;
	} else {
		rappel.auteurCloture = null;
	}

	const rappelRepository = AppDataSource.getRepository(Rappel);
	const rap = await ensureRappelIsInDevecoTerritory({ rappelId: id, deveco, ctx });

	const ficheIdAsString = (ctx.query.ficheId as string) || '';
	let ficheId = parseInt(ficheIdAsString);
	let ficheType: EventLogEntityType | undefined = undefined;
	if (ficheId) {
		const fiche = await AppDataSource.getRepository(Fiche).findOneOrFail({
			where: {
				id: ficheId,
				territoire: { id: deveco.territory.id },
			},
		});
		await rappelRepository.findOneOrFail({ where: { id, fiche: { id: ficheId } } });

		await rappelRepository.update(id, rappel);
		await AppDataSource.getRepository(Fiche).update(ficheId, {
			updatedAt: new Date(),
			auteurModification: deveco,
		});

		ctx.body = await ficheService.ficheToView({ id: ficheId });
		ficheType = getLogEntityType(fiche);
	} else if (rap.fiche?.id) {
		ficheId = rap.fiche?.id;
		await rappelRepository.update(id, rappel);
		await AppDataSource.getRepository(Fiche).update(rap.fiche.id, {
			updatedAt: new Date(),
			auteurModification: deveco,
		});

		ctx.body = await getRappelsForDeveco(deveco);
		ficheType = getLogEntityType(rap.fiche);
	}

	if (ficheType) {
		await eventLogService.addEventLog({
			action: EventLogActionType.UPDATE,
			deveco,
			entityType: ficheType,
			entityId: ficheId,
		});
	}
});

router.del('/:id', async (ctx: Koa.Context) => {
	const deveco = ctx.state.deveco as DevEco;

	const id = parseInt(ctx.params.id);
	if (!id) {
		ctx.throw(StatusCodes.BAD_REQUEST, 'id is required');
	}

	const rappelRepository = AppDataSource.getRepository(Rappel);
	await ensureRappelIsInDevecoTerritory({ rappelId: id, deveco, ctx });

	const ficheIdAsString = (ctx.query.ficheId as string) || '';
	let ficheId: number | undefined = parseInt(ficheIdAsString);
	let ficheType: EventLogEntityType | undefined = undefined;
	if (ficheId) {
		const fiche = await AppDataSource.getRepository(Fiche).findOneOrFail({
			where: {
				id: ficheId,
				territoire: { id: deveco.territory.id },
			},
		});

		await rappelRepository.delete({ id, fiche: { id: ficheId } });
		await AppDataSource.getRepository(Fiche).update(ficheId, {
			updatedAt: new Date(),
			auteurModification: deveco,
		});

		ctx.status = StatusCodes.OK;
		ctx.body = await ficheService.ficheToView({ id: ficheId });
		ficheType = getLogEntityType(fiche);
	} else {
		const rappel = await rappelRepository.findOneOrFail({ where: { id, fiche: { id: ficheId } } });
		ficheId = rappel.fiche?.id;
		await rappelRepository.delete({ id });
		if (ficheId) {
			await AppDataSource.getRepository(Fiche).update(ficheId, {
				updatedAt: new Date(),
				auteurModification: deveco,
			});
			ficheType = getLogEntityType(rappel.fiche as Fiche);
		}

		ctx.status = StatusCodes.OK;
		ctx.body = await getRappelsForDeveco(deveco);
	}

	if (ficheType) {
		await eventLogService.addEventLog({
			action: EventLogActionType.UPDATE,
			deveco,
			entityType: ficheType,
			entityId: ficheId,
		});
	}
});

export default router;
