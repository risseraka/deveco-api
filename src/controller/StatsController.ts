import * as Koa from 'koa';
import Router from 'koa-router';
import { SelectQueryBuilder } from 'typeorm';

import AppDataSource from '../data-source';
import { Account } from '../entity/Account';
import { EventLog } from '../entity/EventLog';
import { Demande } from '../entity/Demande';
import { DevEco } from '../entity/DevEco';
import { Echange } from '../entity/Echange';
import { Fiche } from '../entity/Fiche';
import { Territory } from '../entity/Territory';

const prefix = '/stats';
const routerOpts: Router.IRouterOptions = {
	prefix,
};

const router: Router = new Router(routerOpts);

const days = (amount: number) => 1000 * 60 * 60 * 24 * amount;

router.get('/', async (ctx: Koa.Context) => {
	const periode = ctx.query.periode;
	const type = ctx.query.type;
	const now = new Date();

	let periodStart: Date;
	switch (periode) {
		case 'mois':
			periodStart = new Date(now.getTime() - days(30));
			break;
		case 'semaine':
			periodStart = new Date(now.getTime() - days(7));
			break;
		case 'tout':
		default:
			periodStart = new Date(0);
			break;
	}

	const periodFilter =
		(key: string) =>
		<T>(query: SelectQueryBuilder<T>) =>
			query.andWhere(`${key} >= :periodStart`, { key, periodStart });

	let territoireFilter = <T>(query: SelectQueryBuilder<T>) => query;
	if (type && !Array.isArray(type)) {
		if (type === 'epci') {
			territoireFilter = <T>(query: SelectQueryBuilder<T>) =>
				query.andWhere('territoire.epci_id IS NOT NULL');
		}
		if (type === 'commune') {
			territoireFilter = <T>(query: SelectQueryBuilder<T>) =>
				query.andWhere('territoire.commune_id IS NOT NULL');
		}
	}

	/***************************/
	/* Requêtes sur l'ensemble */
	/***************************/
	const baseTerritoryQuery = AppDataSource.getRepository(Territory)
		.createQueryBuilder('territoire')
		.innerJoin(DevEco, 'deveco', 'deveco.territory_id = territoire.id')
		.innerJoin(Account, 'account', 'deveco.account_id = account.id')
		.where('account.lastLogin IS NOT NULL');
	const territoiresTotal = await baseTerritoryQuery.clone().getCount();

	const territoiresNouveauxQuery = baseTerritoryQuery.clone();
	periodFilter('territoire.createdAt')(territoiresNouveauxQuery);
	const territoiresNouveaux = await territoiresNouveauxQuery.getCount();

	const baseDevecoQuery = AppDataSource.getRepository(DevEco)
		.createQueryBuilder('deveco')
		.leftJoin('deveco.account', 'account')
		.where('account.lastLogin IS NOT NULL');

	const devecosTotal = await baseDevecoQuery.clone().getCount();

	const devecosNouveauxQuery = baseDevecoQuery.clone();
	periodFilter('deveco.createdAt')(devecosNouveauxQuery);
	const devecosNouveaux = await devecosNouveauxQuery.getCount();

	const devecosActifsQuery = baseDevecoQuery
		.clone()
		.leftJoinAndSelect('deveco.territory', 'territoire')
		.leftJoin(EventLog, 'eventLog', 'eventLog.deveco_id = deveco.id');
	periodFilter('eventLog.date')(devecosActifsQuery);
	const devecosActifs = await devecosActifsQuery.clone().getCount();

	const territoiresActifsArray = (await devecosActifsQuery.clone().getMany())
		.map((d) => {
			return d.territory.id;
		})
		.reduce(
			(acc, territoryId) => ({
				...acc,
				[territoryId]: '',
			}),
			{}
		);
	const territoiresActifs = Object.keys(territoiresActifsArray).length;

	const devecos = { total: devecosTotal, nouveaux: devecosNouveaux, actifs: devecosActifs };
	const territoires = {
		total: territoiresTotal,
		nouveaux: territoiresNouveaux,
		actifs: territoiresActifs,
	};

	/**************************/
	/* Requêtes sur les zones */
	/**************************/

	const devecosTotalZoneQuery = baseDevecoQuery.clone().leftJoin('deveco.territory', 'territoire');
	territoireFilter(devecosTotalZoneQuery);
	const devecosTotalZone = await devecosTotalZoneQuery.getCount();

	const devecosNouveauxZoneQuery = baseDevecoQuery
		.clone()
		.leftJoin('deveco.territory', 'territoire');
	periodFilter('deveco.createdAt')(devecosNouveauxZoneQuery);
	territoireFilter(devecosNouveauxZoneQuery);
	const devecosNouveauxZone = await devecosNouveauxZoneQuery.getCount();

	const devecosActifsZoneQuery = baseDevecoQuery
		.clone()
		.leftJoin('deveco.territory', 'territoire')
		.leftJoin(EventLog, 'eventLog', 'eventLog.deveco_id = deveco.id');
	territoireFilter(devecosActifsZoneQuery);
	periodFilter('eventLog.date')(devecosActifsZoneQuery);
	periodFilter('account.lastLogin')(devecosActifsZoneQuery);
	const devecosActifsZone = await devecosActifsZoneQuery.getCount();

	const fichesTotalZoneQuery = AppDataSource.getRepository(Fiche)
		.createQueryBuilder('fiche')
		.leftJoin('fiche.territoire', 'territoire');
	territoireFilter(fichesTotalZoneQuery);
	const fichesTotalZone = await fichesTotalZoneQuery.getCount();

	const fichesNouvellesZoneQuery = AppDataSource.getRepository(Fiche)
		.createQueryBuilder('fiche')
		.leftJoin('fiche.territoire', 'territoire');
	territoireFilter(fichesNouvellesZoneQuery);
	periodFilter('fiche.createdAt')(fichesNouvellesZoneQuery);
	const fichesNouvellesZone = await fichesNouvellesZoneQuery.getCount();

	const fichesModifieesZoneQuery = AppDataSource.getRepository(Fiche)
		.createQueryBuilder('fiche')
		.andWhere("fiche.updated_at >= (fiche.created_at + '1 second'::interval)")
		.leftJoin('fiche.territoire', 'territoire');
	territoireFilter(fichesModifieesZoneQuery);
	periodFilter('fiche.updated_at')(fichesModifieesZoneQuery);
	const fichesModifieesZone = await fichesModifieesZoneQuery.getCount();

	const echangesTotalZoneQuery = AppDataSource.getRepository(Echange)
		.createQueryBuilder('echange')
		.leftJoin('echange.fiche', 'fiche')
		.leftJoin('fiche.territoire', 'territoire');
	territoireFilter(echangesTotalZoneQuery);
	const echangesTotalZone = await echangesTotalZoneQuery.getCount();

	const echangesNouveauxZoneQuery = AppDataSource.getRepository(Echange)
		.createQueryBuilder('echange')
		.leftJoin('echange.fiche', 'fiche')
		.leftJoin('fiche.territoire', 'territoire');
	territoireFilter(echangesNouveauxZoneQuery);
	periodFilter('echange.createdAt')(echangesNouveauxZoneQuery);
	const echangesNouveauxZone = await echangesNouveauxZoneQuery.getCount();

	const demandesTotalZoneQuery = AppDataSource.getRepository(Demande)
		.createQueryBuilder('demande')
		.leftJoin('demande.fiche', 'fiche')
		.leftJoin('fiche.territoire', 'territoire');
	territoireFilter(demandesTotalZoneQuery);
	const demandesTotalZone = await demandesTotalZoneQuery.getCount();

	const demandesNouvellesZoneQuery = AppDataSource.getRepository(Demande)
		.createQueryBuilder('demande')
		.leftJoin('demande.fiche', 'fiche')
		.leftJoin('fiche.territoire', 'territoire');
	territoireFilter(demandesNouvellesZoneQuery);
	periodFilter('demande.createdAt')(demandesNouvellesZoneQuery);
	const demandesNouvellesZone = await demandesNouvellesZoneQuery.getCount();

	const demandesFermeesZoneQuery = AppDataSource.getRepository(Demande)
		.createQueryBuilder('demande')
		.leftJoin('demande.fiche', 'fiche')
		.leftJoin('fiche.territoire', 'territoire');
	territoireFilter(demandesFermeesZoneQuery);
	periodFilter('demande.dateCloture')(demandesFermeesZoneQuery);
	const demandesFermeesZone = await demandesFermeesZoneQuery.getCount();

	const zone = {
		devecos: { total: devecosTotalZone, nouveaux: devecosNouveauxZone, actifs: devecosActifsZone },
		fiches: {
			total: fichesTotalZone,
			nouvelles: fichesNouvellesZone,
			modifiees: fichesModifieesZone,
		},
		echanges: { total: echangesTotalZone, nouveaux: echangesNouveauxZone },
		demandes: {
			total: demandesTotalZone,
			nouvelles: demandesNouvellesZone,
			fermees: demandesFermeesZone,
		},
	};
	ctx.body = { territoires, devecos, zone };
});

export default router;
