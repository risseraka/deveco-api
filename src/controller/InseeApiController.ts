import { StatusCodes } from 'http-status-codes';
import { DefaultContext, DefaultState, ParameterizedContext } from 'koa';
import Router from 'koa-router';
import { DeepPartial, In } from 'typeorm';

import AppDataSource from '../data-source';
import { DevEco } from '../entity/DevEco';
import { Entreprise } from '../entity/Entreprise';
import { Etablissement } from '../entity/Etablissement';
import { Fiche } from '../entity/Fiche';
import { isSiret } from '../lib/regexp';
import AuthMiddleware from '../middleware/AuthMiddleware';
import ficheService from '../service/FicheService';
import inseeService from '../service/InseeService';
import { FicheView } from '../view/FicheView';

const routerOpts: Router.IRouterOptions = {
	prefix: '/insee',
};

const router: Router = new Router(routerOpts);
router.use(AuthMiddleware);

const resultsPerPage = 20;

type EntreprisesPayload =
	| {
			ficheId?: number;
			fiche: FicheView;
	  }
	| DeepPartial<Entreprise>[]
	| { error: string };

router.get(
	'/siret',
	async (ctx: ParameterizedContext<DefaultState, DefaultContext, EntreprisesPayload>) => {
		const query = ctx.query;
		const search = (query.search || query.recherche) as string;

		if (!search) {
			ctx.status = StatusCodes.BAD_REQUEST;
			ctx.body = { error: 'Cannot search for empty string.' };
			return;
		}

		if (!isSiret(search)) {
			ctx.status = StatusCodes.BAD_REQUEST;
			ctx.body = { error: `Invalid SIRET: ${search}` };
			return;
		}

		const deveco = ctx.state.deveco as DevEco;
		const territoire = deveco.territory;
		const ficheRepository = AppDataSource.getRepository(Fiche);
		const fiche = await ficheRepository.findOne({
			where: {
				entite: {
					territoire: {
						id: territoire.id,
					},
					entreprise: { siret: search },
				},
			},
			relations: { entite: { entreprise: true } },
		});

		if (fiche) {
			ctx.body = { ficheId: fiche.id, fiche: await ficheService.ficheToView({ id: fiche.id }) };
		} else {
			const data = await inseeService.searchBySiret(search);
			ctx.body = data;
		}
	}
);

router.get('/entreprises', async (ctx) => {
	const deveco = ctx.state.deveco as DevEco;

	const rawPage = ctx.query.page as unknown;
	let page = Number.isNaN(Number(rawPage)) ? 1 : Number(rawPage);
	const recherche = ((ctx.query.recherche as string) || '').replaceAll("'", "''");
	const ferme = (ctx.query.ferme as string) || '';
	const codesNaf = (ctx.query.codesNaf as string[]) || [];
	const communes = (ctx.query.communes as string[]) || [];
	const tranchesEffectifs = (ctx.query.effectifs as string[]) || [];
	const formes = (ctx.query.formes as string) || '';
	const cp = (ctx.query.cp as string) || '';
	const rue = (ctx.query.rue as string) || '';

	const criteria = {
		recherche,
		ferme,
		codesNaf,
		communes,
		tranchesEffectifs,
		formes,
		territoire: deveco.territory.id,
		cp,
		rue,
	};

	let [sirets, totalResults] = await inseeService.search(criteria, page, resultsPerPage);

	if (sirets.length == 0 && totalResults > 0) {
		page = Math.floor(totalResults / resultsPerPage) + 1;
		[sirets, totalResults] = await inseeService.search(criteria, page, resultsPerPage);
	}

	const fullPages = Math.floor(totalResults / resultsPerPage);
	const pages = fullPages + (totalResults - fullPages * resultsPerPage > 0 ? 1 : 0);

	const etablissements = await AppDataSource.getRepository(Etablissement).find({
		where: { siret: In(sirets) },
		order: { dateCreation: 'DESC' },
	});
	const fiches = await AppDataSource.getRepository(Fiche).find({
		where: { entite: { entreprise: { siret: In(sirets.map((s) => s.padStart(14, '0'))) } } },
		relations: { entite: { entreprise: true } },
	});

	const siretToFiche = fiches.reduce(
		(mapping, fiche) => ({
			...mapping,
			[fiche.entite.entreprise?.siret ?? '']: fiche.id,
		}),
		{}
	);

	ctx.body = {
		page,
		pages,
		results: totalResults,
		data: etablissements
			.map((etablissement) => etablissementToView(etablissement))
			.map((etablissement) => addFicheId(siretToFiche)(etablissement)),
	};
});

function addFicheId(
	etablissementToFiche: Record<string, string>
): (
	etablissement: ReturnType<typeof etablissementToView>
) => ReturnType<typeof etablissementToView> & { ficheId?: string } {
	return function (
		etablissement: ReturnType<typeof etablissementToView>
	): ReturnType<typeof etablissementToView> & { ficheId?: string } {
		const ficheId = etablissement.siret
			? etablissementToFiche[etablissement.siret.padStart(14, '0')]
			: undefined;
		return { ...etablissement, ficheId };
	};
}

function etablissementToView(
	etablissement: Etablissement
): DeepPartial<Entreprise> & { siren: string } {
	return {
		siret: etablissement.siret,
		siren: etablissement.siren,
		nom: etablissement.nomPublic?.toLocaleUpperCase() ?? undefined,
		adresse: etablissement.adresseComplete?.toLocaleUpperCase() ?? undefined,
		etatAdministratif: etablissement.etatAdministratif,
		dateCreation: etablissement.dateCreation,
		codeNaf: etablissement.codeNaf,
		libelleNaf: etablissement.libelleNaf,
		libelleCategorieNaf: etablissement.libelleCategorieNaf,
		enseigne: etablissement.enseigne?.toLocaleUpperCase() ?? undefined,
	};
}

export default router;
