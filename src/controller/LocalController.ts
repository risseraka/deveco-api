import * as Koa from 'koa';
import Router from 'koa-router';

import AppDataSource from '../data-source';
import { DevEco } from '../entity/DevEco';
import { Entite } from '../entity/Entite';
import { EventLogActionType, EventLogEntityType } from '../entity/EventLog';
import { Fiche } from '../entity/Fiche';
import { Local, LocalStatut, LocalType } from '../entity/Local';
import { LocalArchive } from '../entity/LocalArchive';
import { Particulier } from '../entity/Particulier';
import { Proprietaire } from '../entity/Proprietaire';
import AuthMiddleware from '../middleware/AuthMiddleware';
import eventLogService from '../service/EventLogService';
import localisationEntrepriseService from '../service/LocalisationEntrepriseService';
import entrepriseService from '../service/EntrepriseService';
import { loadView, LocalView, toView } from '../view/LocalView';

const routerOpts: Router.IRouterOptions = {
	prefix: '/locaux',
};

const router: Router = new Router(routerOpts);
router.use(AuthMiddleware);

const resultsPerPage = 20;

router.get(
	'/',
	async (
		ctx: Koa.ParameterizedContext<
			Koa.DefaultState,
			Koa.DefaultContext,
			{ page: number; pages: number; results: number; data: LocalView[] }
		>
	) => {
		const rawPage = ctx.query.page as unknown;
		let page = Number.isNaN(Number(rawPage)) ? 1 : Number(rawPage);
		const recherche = (ctx.query.recherche as string) || '';
		const statut = (ctx.query.statut as LocalStatut) || '';
		const types = (ctx.query.types as LocalType[]) || [];
		const deveco = ctx.state.deveco as DevEco;
		const conditions = [];

		conditions.push(`local.territoire_id = ${deveco.territory.id}`);

		if (recherche) {
			conditions.push(`titre ILIKE '%${recherche}%'`);
		}

		if (statut === 'occupe') {
			conditions.push(`local_statut = 'occupe'`);
		} else if (statut === 'vacant') {
			conditions.push(`local_statut = 'vacant'`);
		}

		if (types?.length) {
			const or = [];
			for (const t of types) {
				or.push(`local_types ilike '%${t}%'`);
			}
			conditions.push('( ' + or.join(' OR ') + ' )');
		}

		let [data, totalResults] = await search(conditions, page);

		if (data.length == 0 && totalResults > 0) {
			page = 1;
			[data, totalResults] = await search(conditions, page);
		}

		const fullPages = Math.floor(totalResults / resultsPerPage);
		const pages = fullPages + (totalResults - fullPages * resultsPerPage > 0 ? 1 : 0);

		const locaux = data.map((local) => toView(local));

		ctx.body = { page, pages, results: totalResults, data: locaux };
	}
);

async function search(conditions: string[], page: number): Promise<[Local[], number]> {
	const skip = (page - 1) * resultsPerPage;
	return AppDataSource.getRepository(Local)
		.createQueryBuilder('local')
		.leftJoinAndSelect('local.auteurModification', 'auteurModification')
		.leftJoinAndSelect('auteurModification.account', 'auteurModificationAccount')
		.where(conditions.join(' AND '))
		.skip(skip)
		.take(resultsPerPage)
		.getManyAndCount();
}

router.post(
	'/:id/modifier',
	async (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, LocalView>) => {
		const { id } = ctx.params;
		const { local } = ctx.request.body;

		const createur = ctx.state.deveco as DevEco;
		const territoire = createur.territory;

		await localisationEntrepriseService.createIfNecessary(local.localisations, territoire);

		const now = new Date();
		const localRepository = AppDataSource.getRepository(Local);

		await localRepository.update(id, {
			...local,
			dateModification: now,
			auteurModification: createur,
			createdAt: now,
			updatedAt: now,
		});

		const updated = await loadView(id);

		await eventLogService.addEventLog({
			action: EventLogActionType.UPDATE,
			deveco: createur,
			entityType: EventLogEntityType.LOCAL,
			entityId: id,
		});

		ctx.body = toView(updated);
	}
);

type NewProprietaireType = 'manual' | 'portefeuille' | 'siret';

function isCorrectType(s: string): s is NewProprietaireType {
	return true;
}
router.post(
	'/:id/proprietaire/remove',
	async (
		ctx: Koa.ParameterizedContext<
			Koa.DefaultState,
			Koa.DefaultContext,
			LocalView | { error: string }
		>
	) => {
		const { id } = ctx.params;
		const { proprietaireId } = ctx.request.body;

		const proprietaire = await AppDataSource.getRepository(Proprietaire).findOne({
			where: { id: proprietaireId, local: { id } },
		});
		if (!proprietaire) {
			ctx.body = { error: 'Nein' };
			ctx.statuts = 404;
			return;
		}
		await AppDataSource.getRepository(Proprietaire).remove(proprietaire);

		const viewLocal = await loadView(id);
		ctx.body = viewLocal;
		return;
	}
);

router.post(
	'/:id/proprietaire',
	async (
		ctx: Koa.ParameterizedContext<
			Koa.DefaultState,
			Koa.DefaultContext,
			LocalView | { error: string }
		>
	) => {
		const { id } = ctx.params;
		const { type } = ctx.request.body;

		if (!isCorrectType(type)) {
			ctx.status = 400;
			ctx.body = { error: 'Bad owner type' };
			return;
		}

		const local = await AppDataSource.getRepository(Local).findOne({ where: { id } });

		if (!local) {
			ctx.status = 400;
			ctx.body = { error: 'Local not found' };
			return;
		}

		const createur = ctx.state.deveco as DevEco;
		const territoire = createur.territory;
		const now = new Date();

		let entiteId: Entite['id'];

		switch (type) {
			case 'portefeuille': {
				const { ficheId } = ctx.request.body;
				const fiche = await AppDataSource.getRepository(Fiche).findOneOrFail({
					where: { id: ficheId },
					relations: { entite: true },
				});

				entiteId = fiche.entite.id;
				break;
			}
			case 'siret': {
				const { siret } = ctx.request.body;
				const existingFiche = await AppDataSource.getRepository(Fiche).findOne({
					where: {
						entite: { entreprise: { siret } },
						territoire: { id: territoire.id },
					},
					relations: { entite: true },
				});
				if (existingFiche) {
					entiteId = existingFiche.entite.id;
				} else {
					const entite = await entrepriseService.getOrCreateEntrepriseAndEntite({
						siret,
						createur,
					});
					entiteId = entite.id;
				}
				break;
			}
			case 'manual': {
				const { prenom, nom, email, telephone } = ctx.request.body;
				const { identifiers: particuliers } = await AppDataSource.getRepository(Particulier).insert(
					{
						prenom,
						nom,
						email,
						telephone,
					}
				);
				const { identifiers: entites } = await AppDataSource.getRepository(Entite).insert({
					territoire: { id: territoire.id },
					particulier: { id: particuliers[0].id },
					entiteType: 'PP',
				});

				entiteId = entites[0].id;
				break;
			}
		}
		try {
			await AppDataSource.getRepository(Proprietaire).insert({
				local: { id: local.id },
				entite: { id: entiteId },
			});
			const localRepository = AppDataSource.getRepository(Local);

			await localRepository.update(id, {
				...local,
				dateModification: now,
				auteurModification: createur,
				updatedAt: now,
			});

			await eventLogService.addEventLog({
				action: EventLogActionType.UPDATE,
				deveco: createur,
				entityType: EventLogEntityType.LOCAL,
				entityId: id,
			});
		} catch (_) {
			console.log('Tried adding the same entite as an owner twice, skipping', { local, entiteId });
		}

		const viewLocal = await loadView(id);
		ctx.body = viewLocal;
	}
);

router.get(
	'/:id',
	async (
		ctx: Koa.ParameterizedContext<
			Koa.DefaultState,
			Koa.DefaultContext,
			LocalView | { error: string }
		>
	) => {
		const { id } = ctx.params;
		await eventLogService.addEventLog({
			action: EventLogActionType.VIEW,
			deveco: ctx.state.deveco as DevEco,
			entityType: EventLogEntityType.LOCAL,
			entityId: id,
		});

		const viewLocal = await loadView(id);

		ctx.body = viewLocal;
	}
);

router.post(
	'/creer',
	async (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, LocalView>) => {
		const {
			titre,
			adresse,
			ville,
			codePostal,
			geolocation,
			localStatut,
			localTypes,
			surface,
			loyer,
			localisations,
			commentaire,
		} = ctx.request.body;

		const createur = ctx.state.deveco as DevEco;
		const territoire = createur.territory;

		await localisationEntrepriseService.createIfNecessary(localisations, territoire);

		const now = new Date();
		const localRepository = AppDataSource.getRepository(Local);
		const local: Local = {
			titre,
			adresse,
			ville,
			codePostal,
			geolocation,
			localStatut,
			localTypes,
			surface,
			loyer,
			localisations,
			commentaire,
			territoire,
			createur,
			dateModification: now,
			auteurModification: createur,
			createdAt: now,
			updatedAt: now,
		};

		const savedLocal = await localRepository.save(local);

		await eventLogService.addEventLog({
			action: EventLogActionType.CREATE,
			deveco: createur,
			entityType: EventLogEntityType.LOCAL,
			entityId: savedLocal.id,
		});

		ctx.body = toView(savedLocal);
	}
);

router.del(
	'/:id',
	async (
		ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, Record<never, never>>
	) => {
		const id = ctx.params.id;
		const deveco = ctx.state.deveco as DevEco;
		const territoire = deveco.territory;

		const local = await AppDataSource.getRepository(Local).findOneOrFail({
			where: { id, territoire: { id: territoire.id } },
		});
		await AppDataSource.getRepository(LocalArchive).insert({ content: JSON.stringify(local) });
		await AppDataSource.getRepository(Local).delete({ id, territoire });

		await eventLogService.addEventLog({
			action: EventLogActionType.DELETE,
			deveco,
			entityType: EventLogEntityType.LOCAL,
			entityId: id,
		});

		ctx.body = {};
	}
);

export default router;
