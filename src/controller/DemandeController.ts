import { StatusCodes } from 'http-status-codes';
import * as Koa from 'koa';
import Router from 'koa-router';

import AuthMiddleware from '../middleware/AuthMiddleware';
import { Demande } from '../entity/Demande';
import { Fiche } from '../entity/Fiche';
import ficheService from '../service/FicheService';
import AppDataSource from '../data-source';
import eventLogService from '../service/EventLogService';
import { DevEco } from '../entity/DevEco';
import { EventLogActionType } from '../entity/EventLog';
import { getLogEntityType } from '../lib/utils';

const routerOpts: Router.IRouterOptions = {
	prefix: '/demandes',
};

const router: Router = new Router(routerOpts);
router.use(AuthMiddleware);

router.post('/cloturer', async (ctx: Koa.Context) => {
	const deveco = ctx.state.deveco as DevEco;

	const ficheIdAsString = (ctx.query.ficheId as string) || '';
	const ficheId = parseInt(ficheIdAsString);
	if (!ficheId) {
		ctx.throw(StatusCodes.BAD_REQUEST, 'ficheId is required');
	}

	const fiche = await AppDataSource.getRepository(Fiche).findOneOrFail({
		where: {
			id: ficheId,
			territoire: { id: deveco.territory.id },
		},
	});

	const { demandeId, motif } = ctx.request.body;

	const demandeRepository = AppDataSource.getRepository(Demande);

	await demandeRepository.findOneOrFail({
		where: {
			id: demandeId,
			fiche: {
				id: ficheId,
			},
		},
	});

	await demandeRepository.update(demandeId, {
		motif,
		cloture: true,
		dateCloture: new Date().toISOString().slice(0, 10),
	});
	await AppDataSource.getRepository(Fiche).update(ficheId, {
		updatedAt: new Date(),
		auteurModification: deveco,
	});

	await eventLogService.addEventLog({
		action: EventLogActionType.UPDATE,
		deveco,
		entityType: getLogEntityType(fiche),
		entityId: ficheId,
	});

	ctx.status = StatusCodes.OK;
	ctx.body = await ficheService.ficheToView({ id: ficheId });
});

export default router;
