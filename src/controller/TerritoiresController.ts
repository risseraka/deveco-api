import * as Koa from 'koa';
import Router from 'koa-router';
import { ILike, In } from 'typeorm';

import AppDataSource from '../data-source';
import { Commune } from '../entity/Commune';
import { Epci } from '../entity/Epci';
import { EpciCommune } from '../entity/EpciCommune';
import { Territory } from '../entity/Territory';
import AuthMiddleware from '../middleware/AuthMiddleware';
import RoleMiddleware from '../middleware/RoleMiddleware';

const prefix = '/territoires';
const routerOpts: Router.IRouterOptions = {
	prefix,
};

const router: Router = new Router(routerOpts);
router.use(AuthMiddleware);

const superAdminOnly = RoleMiddleware('superadmin');

const protectAllButRoot = (ctx: Koa.Context, next: (ctx: Koa.Context) => Promise<unknown>) => {
	if (ctx.path === `${prefix}` || ctx.path === `${prefix}/`) {
		return next(ctx);
	}

	if (ctx.path.match(new RegExp(`${prefix}/\\d*/communes`))) {
		return next(ctx);
	}

	return superAdminOnly(ctx, next);
};
router.use(protectAllButRoot);

const formatCommune = (commune: Commune) => ({
	id: commune.id,
	type: 'commune',
	name: `${commune.libCom} (${commune.inseeDep})`,
});

const formatEpci = (epci: Epci) => ({
	id: epci.id,
	type: 'epci',
	name: epci.libEpci,
});

router.get('/search', async (ctx: Koa.Context) => {
	const query = ctx.query;
	const search = query.search as string;
	const epcis = (
		await AppDataSource.getRepository(Epci).find({
			where: {
				libEpci: ILike(`%${search}%`),
			},
		})
	).map(formatEpci);
	const communes = (
		await AppDataSource.getRepository(Commune).find({
			where: {
				libCom: ILike(`%${search}%`),
			},
		})
	).map(formatCommune);
	ctx.body = epcis.concat(communes).sort((a, b) => a.name.localeCompare(b.name));
});

router.get('/:id/communes', async (ctx: Koa.Context) => {
	const id = ctx.params.id;
	const territory = await AppDataSource.getRepository(Territory).findOne({
		where: { id },
		relations: { epci: true, commune: true },
	});
	const epciId = territory?.epci?.id;
	const communeId = territory?.commune?.id;
	let communes: Commune[] = [];
	if (epciId) {
		const epciCommunes = await AppDataSource.getRepository(EpciCommune).find({
			where: { insee_epci: epciId },
		});
		communes = await AppDataSource.getRepository(Commune).find({
			where: { id: In(epciCommunes.map(({ insee_com }) => insee_com)) },
		});
	} else if (communeId) {
		communes = await AppDataSource.getRepository(Commune).find({
			where: { id: communeId },
		});
	}
	ctx.body = communes
		.map(({ id, libCom, inseeDep }) => ({
			id,
			label: libCom,
			departement: inseeDep,
		}))
		.sort((a, b) => a.label.localeCompare(b.label, 'fr', { ignorePunctuation: true }));
});

export default router;
