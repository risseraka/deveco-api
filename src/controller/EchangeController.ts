import { StatusCodes } from 'http-status-codes';
import * as Koa from 'koa';
import Router from 'koa-router';

import AuthMiddleware from '../middleware/AuthMiddleware';
import { Echange } from '../entity/Echange';
import { Fiche } from '../entity/Fiche';
import ficheService from '../service/FicheService';
import AppDataSource from '../data-source';
import eventLogService from '../service/EventLogService';
import { DevEco } from '../entity/DevEco';
import { EventLogActionType } from '../entity/EventLog';
import { getLogEntityType } from '../lib/utils';

const routerOpts: Router.IRouterOptions = {
	prefix: '/echanges',
};

const router: Router = new Router(routerOpts);
router.use(AuthMiddleware);

// Create Echange
router.post('/creer', async (ctx: Koa.Context) => {
	const createur = ctx.state.deveco as DevEco;
	const { compteRendu, dateEchange, typeEchange, themes, titre } = ctx.request.body;

	const ficheIdAsString = (ctx.query.ficheId as string) || '';
	const ficheId = parseInt(ficheIdAsString);
	if (!ficheId) {
		ctx.throw(StatusCodes.BAD_REQUEST, 'ficheId is required');
	}
	const fiche = await AppDataSource.getRepository(Fiche).findOneOrFail({
		where: {
			id: ficheId,
			territoire: { id: createur.territory.id },
		},
	});

	const echangeRepository = AppDataSource.getRepository(Echange);

	const now = new Date();
	const echange = {
		createur,
		titre: titre as string,
		typeEchange: typeEchange as string,
		dateEchange: dateEchange as Date,
		compteRendu: compteRendu as string,
		themes: (themes as string[]) || [],
		fiche,
		updatedAt: null,
	};
	await echangeRepository.insert(echange);
	await AppDataSource.getRepository(Fiche).update(ficheId, {
		updatedAt: now,
		auteurModification: createur,
	});

	await eventLogService.addEventLog({
		action: EventLogActionType.UPDATE,
		deveco: createur,
		entityType: getLogEntityType(fiche),
		entityId: ficheId,
	});

	await ficheService.createDemandesIfNecessary(themes, ficheId);
	await ficheService.removeDemandesIfNecessary(ficheId);

	ctx.body = await ficheService.ficheToView({ id: ficheId });
});

router.post('/:id/modifier', async (ctx: Koa.Context) => {
	const deveco = ctx.state.deveco as DevEco;

	const ficheIdAsString = (ctx.query.ficheId as string) || '';
	const ficheId = parseInt(ficheIdAsString);
	if (!ficheId) {
		ctx.throw(StatusCodes.BAD_REQUEST, 'ficheId is required');
	}

	const fiche = await AppDataSource.getRepository(Fiche).findOneOrFail({
		where: {
			id: ficheId,
			territoire: { id: deveco.territory.id },
		},
	});

	const id = parseInt(ctx.params.id);
	if (!id) {
		ctx.throw(StatusCodes.BAD_REQUEST, 'id is required');
	}

	const echangeRepository = AppDataSource.getRepository(Echange);
	await echangeRepository.findOneOrFail({ where: { id, fiche: { id: ficheId } } });

	const { echange } = ctx.request.body;

	const now = new Date();
	await echangeRepository.update(id, { ...echange, updatedAt: now, auteurModification: deveco });
	await ficheService.createDemandesIfNecessary(echange.themes, ficheId);
	await ficheService.removeDemandesIfNecessary(ficheId);
	await AppDataSource.getRepository(Fiche).update(ficheId, {
		updatedAt: now,
		auteurModification: deveco,
	});

	await eventLogService.addEventLog({
		action: EventLogActionType.UPDATE,
		deveco,
		entityType: getLogEntityType(fiche),
		entityId: ficheId,
	});

	ctx.body = await ficheService.ficheToView({ id: ficheId });
});

router.del('/:id', async (ctx: Koa.Context) => {
	const deveco = ctx.state.deveco as DevEco;

	const ficheIdAsString = (ctx.query.ficheId as string) || '';
	const ficheId = parseInt(ficheIdAsString);
	if (!ficheId) {
		ctx.throw(StatusCodes.BAD_REQUEST, 'ficheId is required');
	}

	const fiche = await AppDataSource.getRepository(Fiche).findOneOrFail({
		where: {
			id: ficheId,
			territoire: { id: deveco.territory.id },
		},
	});

	const id = parseInt(ctx.params.id);
	if (!id) {
		ctx.throw(StatusCodes.BAD_REQUEST, 'id is required');
	}

	await AppDataSource.getRepository(Echange).delete({ id: id, fiche: { id: ficheId } });
	await AppDataSource.getRepository(Fiche).update(ficheId, {
		updatedAt: new Date(),
		auteurModification: deveco,
	});

	await eventLogService.addEventLog({
		action: EventLogActionType.UPDATE,
		deveco,
		entityType: getLogEntityType(fiche),
		entityId: ficheId,
	});

	ctx.status = StatusCodes.OK;
	ctx.body = await ficheService.ficheToView({ id: ficheId });
});

export default router;
