import { MigrationInterface, QueryRunner } from "typeorm";

export class ficheSplit1658418376009 implements MigrationInterface {
    name = 'ficheSplit1658418376009'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "entite" ("id" SERIAL NOT NULL, "fiche_id" SERIAL NOT NULL, "entite_type" character varying NOT NULL, "activites_reelles" text, "entreprise_localisations" text, "activite_autre" character varying, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "territoire_id" integer, "deveco_id" integer, "auteur_modification_id" integer, "entreprise_id" character varying, "particulier_id" integer, CONSTRAINT "UNIQ_entite_territoire_entreprise" UNIQUE ("territoire_id", "entreprise_id"), CONSTRAINT "REL_ef26bc1487b0f1073e2ed7f4cc" UNIQUE ("particulier_id"), CONSTRAINT "PK_f1e2b4f5d735775ab3dc2cee5c9" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "fiche" ADD "entite_id" integer`);
        await queryRunner.query(`ALTER TABLE "fiche" ADD CONSTRAINT "UQ_492ff1c33c8ad967f4bf15f4849" UNIQUE ("entite_id")`);
        await queryRunner.query(`ALTER TABLE "fiche" ADD CONSTRAINT "UNIQ_fiche_territoire_entite" UNIQUE ("territoire_id", "entite_id")`);
        await queryRunner.query(`INSERT INTO entite (entite_type, activites_reelles, entreprise_localisations, activite_autre, created_at, updated_at, territoire_id, deveco_id, auteur_modification_id, entreprise_id, particulier_id, fiche_id) SELECT fiche_type, activites_reelles, entreprise_localisations, activite_autre, created_at, updated_at, territoire_id, deveco_id, auteur_modification_id, entreprise_id, particulier_id, id from fiche`);
        await queryRunner.query(`ALTER TABLE "contact" ADD "entite_id" integer`);
        await queryRunner.query(`UPDATE fiche SET entite_id = entite.id FROM entite where fiche.id = entite.fiche_id`);
        await queryRunner.query(`UPDATE contact SET entite_id = fiche.entite_id FROM fiche where fiche.id = fiche_id`);
        await queryRunner.query(`ALTER TABLE "contact" DROP CONSTRAINT "FK_3fe9e8fc8add9973e5745beda77"`);
        await queryRunner.query(`ALTER TABLE "contact" DROP COLUMN "fiche_id"`);
        await queryRunner.query(`ALTER TABLE "entite" DROP COLUMN "fiche_id"`);
        await queryRunner.query(`ALTER TABLE "fiche" DROP CONSTRAINT "FK_5304848ebec35665bcadac8d00a"`);
        await queryRunner.query(`ALTER TABLE "fiche" DROP CONSTRAINT "FK_de7cf33e3a99922fbe58ad11ded"`);
        await queryRunner.query(`ALTER TABLE "fiche" DROP CONSTRAINT "UNIQ_fiche_territoire_entreprise"`);
        await queryRunner.query(`ALTER TABLE "contact" ADD "particulier_id" integer`);
        await queryRunner.query(`ALTER TABLE "contact" ADD CONSTRAINT "UQ_5d74910608cb76beb22cfa47766" UNIQUE ("particulier_id")`);
        await queryRunner.query(`ALTER TABLE "particulier" ADD "contact_id" integer`);
        await queryRunner.query(`INSERT INTO particulier (nom, prenom, email, telephone, contact_id) SELECT nom, prenom, email, telephone, id from contact`);
        await queryRunner.query(`UPDATE "contact" SET particulier_id = particulier.id FROM particulier where particulier.contact_id = contact.id`);
        await queryRunner.query(`ALTER TABLE "particulier" DROP COLUMN "contact_id"`);
        await queryRunner.query(`ALTER TABLE "contact" DROP COLUMN "nom"`);
        await queryRunner.query(`ALTER TABLE "contact" DROP COLUMN "prenom"`);
        await queryRunner.query(`ALTER TABLE "contact" DROP COLUMN "email"`);
        await queryRunner.query(`ALTER TABLE "contact" DROP COLUMN "telephone"`);
        await queryRunner.query(`CREATE TABLE "local_proprietaires_entite" ("localId" integer NOT NULL, "entiteId" integer NOT NULL, CONSTRAINT "PK_87f8ff590b4fa37f72663f8b885" PRIMARY KEY ("localId", "entiteId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_d9b7af2899bf449d1795063761" ON "local_proprietaires_entite" ("localId") `);
        await queryRunner.query(`CREATE INDEX "IDX_f7152c7cfd068eb5b9bdc7cc50" ON "local_proprietaires_entite" ("entiteId") `);
        await queryRunner.query(`ALTER TABLE "fiche" DROP COLUMN "fiche_type"`);
        await queryRunner.query(`ALTER TABLE "fiche" DROP COLUMN "entreprise_id"`);
        await queryRunner.query(`ALTER TABLE "fiche" DROP CONSTRAINT "REL_de7cf33e3a99922fbe58ad11de"`);
        await queryRunner.query(`ALTER TABLE "fiche" DROP COLUMN "particulier_id"`);
        await queryRunner.query(`ALTER TABLE "fiche" DROP COLUMN "activites_reelles"`);
        await queryRunner.query(`ALTER TABLE "fiche" DROP COLUMN "activite_autre"`);
        await queryRunner.query(`ALTER TABLE "fiche" DROP COLUMN "entreprise_localisations"`);
        await queryRunner.query(`ALTER TABLE "fiche" ADD CONSTRAINT "FK_492ff1c33c8ad967f4bf15f4849" FOREIGN KEY ("entite_id") REFERENCES "entite"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "entite" ADD CONSTRAINT "FK_447fd3fcc7588fd0b092a487f11" FOREIGN KEY ("territoire_id") REFERENCES "territory"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "entite" ADD CONSTRAINT "FK_9c20417e1801e0ab59a181b2a44" FOREIGN KEY ("deveco_id") REFERENCES "deveco"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "entite" ADD CONSTRAINT "FK_cfe7b64feb9f4681754044eb5ba" FOREIGN KEY ("auteur_modification_id") REFERENCES "deveco"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "entite" ADD CONSTRAINT "FK_5c18958aaa72818d592552105bd" FOREIGN KEY ("entreprise_id") REFERENCES "entreprise"("siret") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "entite" ADD CONSTRAINT "FK_ef26bc1487b0f1073e2ed7f4cc7" FOREIGN KEY ("particulier_id") REFERENCES "particulier"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "contact" ADD CONSTRAINT "FK_8c5bd0ee6928cbe0ffa8e9a4021" FOREIGN KEY ("entite_id") REFERENCES "entite"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "local_proprietaires_entite" ADD CONSTRAINT "FK_d9b7af2899bf449d1795063761a" FOREIGN KEY ("localId") REFERENCES "local"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "local_proprietaires_entite" ADD CONSTRAINT "FK_f7152c7cfd068eb5b9bdc7cc50e" FOREIGN KEY ("entiteId") REFERENCES "entite"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "local_proprietaires_entite" DROP CONSTRAINT "FK_f7152c7cfd068eb5b9bdc7cc50e"`);
        await queryRunner.query(`ALTER TABLE "local_proprietaires_entite" DROP CONSTRAINT "FK_d9b7af2899bf449d1795063761a"`);
        await queryRunner.query(`ALTER TABLE "contact" DROP CONSTRAINT "FK_8c5bd0ee6928cbe0ffa8e9a4021"`);
        await queryRunner.query(`ALTER TABLE "entite" DROP CONSTRAINT "FK_ef26bc1487b0f1073e2ed7f4cc7"`);
        await queryRunner.query(`ALTER TABLE "entite" DROP CONSTRAINT "FK_5c18958aaa72818d592552105bd"`);
        await queryRunner.query(`ALTER TABLE "entite" DROP CONSTRAINT "FK_cfe7b64feb9f4681754044eb5ba"`);
        await queryRunner.query(`ALTER TABLE "entite" DROP CONSTRAINT "FK_9c20417e1801e0ab59a181b2a44"`);
        await queryRunner.query(`ALTER TABLE "entite" DROP CONSTRAINT "FK_447fd3fcc7588fd0b092a487f11"`);
        await queryRunner.query(`ALTER TABLE "fiche" DROP CONSTRAINT "FK_492ff1c33c8ad967f4bf15f4849"`);
        await queryRunner.query(`ALTER TABLE "fiche" DROP CONSTRAINT "UNIQ_fiche_territoire_entite"`);
        await queryRunner.query(`ALTER TABLE "contact" DROP COLUMN "entite_id"`);
        await queryRunner.query(`ALTER TABLE "fiche" DROP CONSTRAINT "UQ_492ff1c33c8ad967f4bf15f4849"`);
        await queryRunner.query(`ALTER TABLE "fiche" DROP COLUMN "entite_id"`);
        await queryRunner.query(`ALTER TABLE "contact" ADD "fiche_id" integer`);
        await queryRunner.query(`ALTER TABLE "contact" ADD "telephone" character varying`);
        await queryRunner.query(`ALTER TABLE "contact" ADD "email" character varying`);
        await queryRunner.query(`ALTER TABLE "contact" ADD "prenom" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "contact" ADD "nom" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "fiche" ADD "entreprise_localisations" text`);
        await queryRunner.query(`ALTER TABLE "fiche" ADD "activite_autre" character varying`);
        await queryRunner.query(`ALTER TABLE "fiche" ADD "activites_reelles" text`);
        await queryRunner.query(`ALTER TABLE "fiche" ADD "particulier_id" integer`);
        await queryRunner.query(`ALTER TABLE "fiche" ADD CONSTRAINT "REL_de7cf33e3a99922fbe58ad11de" UNIQUE ("particulier_id")`);
        await queryRunner.query(`ALTER TABLE "fiche" ADD "entreprise_id" character varying`);
        await queryRunner.query(`ALTER TABLE "fiche" ADD "fiche_type" character varying NOT NULL`);
        await queryRunner.query(`DROP INDEX "public"."IDX_f7152c7cfd068eb5b9bdc7cc50"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_d9b7af2899bf449d1795063761"`);
        await queryRunner.query(`DROP TABLE "local_proprietaires_entite"`);
        await queryRunner.query(`DROP TABLE "entite"`);
        await queryRunner.query(`ALTER TABLE "fiche" ADD CONSTRAINT "UNIQ_fiche_territoire_entreprise" UNIQUE ("territoire_id", "entreprise_id")`);
        await queryRunner.query(`ALTER TABLE "contact" ADD CONSTRAINT "FK_3fe9e8fc8add9973e5745beda77" FOREIGN KEY ("fiche_id") REFERENCES "fiche"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "fiche" ADD CONSTRAINT "FK_de7cf33e3a99922fbe58ad11ded" FOREIGN KEY ("particulier_id") REFERENCES "particulier"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "fiche" ADD CONSTRAINT "FK_5304848ebec35665bcadac8d00a" FOREIGN KEY ("entreprise_id") REFERENCES "entreprise"("siret") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
