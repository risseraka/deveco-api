import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateGeolocEntreprise1649923266284 implements MigrationInterface {
    name = 'CreateGeolocEntreprise1649923266284'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "geoloc_entreprise" ("siret" character varying NOT NULL, "x" integer NOT NULL, "y" integer NOT NULL, "qualite_xy" character varying NOT NULL, "epsg" character varying NOT NULL, "plg_qp" character varying NOT NULL, "plg_iris" character varying NOT NULL, "plg_zus" character varying NOT NULL, "plg_zfu" character varying NOT NULL, "plg_qva" character varying NOT NULL, "plg_code_commune" character varying NOT NULL, "distance_precision" integer NOT NULL, "qualite_qp" character varying NOT NULL, "qualite_iris" character varying NOT NULL, "qualite_zus" character varying NOT NULL, "qualite_zfu" character varying NOT NULL, "qualite_qva" character varying NOT NULL, "y_latitude" integer NOT NULL, "x_longitude" integer NOT NULL, CONSTRAINT "PK_96bcdd56eb7cf06893cebc0cabe" PRIMARY KEY ("siret", "x"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "geoloc_entreprise"`);
    }

}
