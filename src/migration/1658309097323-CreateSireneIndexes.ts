import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateSireneIndexes1658309097323 implements MigrationInterface {
    name = 'CreateSireneIndexes1658309097323'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE INDEX "IDX_39bde3c6f4efb0b5619c66bb21" ON "sirene_etablissement" ("siren") `);
        await queryRunner.query(`CREATE INDEX "IDX_d96ab8ac3139e431b0b144d81f" ON "sirene_etablissement" ("nic") `);
        await queryRunner.query(`CREATE INDEX "IDX_4e7fe9960ac8949d7e28724a25" ON "sirene_etablissement" ("siret") `);
        await queryRunner.query(`CREATE INDEX "IDX_7eef7f1f8853920ba288f62bd9" ON "sirene_unite_legale" ("siren") `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "public"."IDX_7eef7f1f8853920ba288f62bd9"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_4e7fe9960ac8949d7e28724a25"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_d96ab8ac3139e431b0b144d81f"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_39bde3c6f4efb0b5619c66bb21"`);
    }

}
