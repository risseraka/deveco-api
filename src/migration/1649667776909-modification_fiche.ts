import {MigrationInterface, QueryRunner} from "typeorm";

export class modificationFiche1649667776909 implements MigrationInterface {
    name = 'modificationFiche1649667776909'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "fiche" ADD "date_modification" TIMESTAMP WITH TIME ZONE NOT NULL`);
        await queryRunner.query(`ALTER TABLE "fiche" ADD "auteur_modification" character varying NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "fiche" DROP COLUMN "auteur_modification"`);
        await queryRunner.query(`ALTER TABLE "fiche" DROP COLUMN "date_modification"`);
    }

}
