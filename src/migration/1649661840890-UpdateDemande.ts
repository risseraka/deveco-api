import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateDemande1649661840890 implements MigrationInterface {
    name = 'UpdateDemande1649661840890'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "demande" ADD "fiche_id" integer`);
        await queryRunner.query(`ALTER TABLE "demande" ADD CONSTRAINT "FK_a15b3bf3f0f25886412f1a59138" FOREIGN KEY ("fiche_id") REFERENCES "fiche"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "demande" DROP CONSTRAINT "FK_a15b3bf3f0f25886412f1a59138"`);
        await queryRunner.query(`ALTER TABLE "demande" DROP COLUMN "fiche_id"`);
    }

}
