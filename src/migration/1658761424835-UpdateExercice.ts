import { MigrationInterface, QueryRunner } from "typeorm";

export class UpdateExercice1658761424835 implements MigrationInterface {
    name = 'UpdateExercice1658761424835'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "exercice" DROP CONSTRAINT "FK_6440766f4b0c9edb58271cd298a"`);
        await queryRunner.query(`ALTER TABLE "exercice" DROP COLUMN "ca"`);
        await queryRunner.query(`ALTER TABLE "exercice" ADD "ca" bigint NOT NULL`);
        await queryRunner.query(`ALTER TABLE "entreprise" ALTER COLUMN "migrated" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "exercice" ADD CONSTRAINT "FK_66db31cf83320f8608091ecf6f3" FOREIGN KEY ("entreprise_id") REFERENCES "entreprise"("siret") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "exercice" DROP CONSTRAINT "FK_66db31cf83320f8608091ecf6f3"`);
        await queryRunner.query(`ALTER TABLE "entreprise" ALTER COLUMN "migrated" SET DEFAULT false`);
        await queryRunner.query(`ALTER TABLE "exercice" DROP COLUMN "ca"`);
        await queryRunner.query(`ALTER TABLE "exercice" ADD "ca" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "exercice" ADD CONSTRAINT "FK_6440766f4b0c9edb58271cd298a" FOREIGN KEY ("entreprise_id") REFERENCES "entreprise"("siret") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
