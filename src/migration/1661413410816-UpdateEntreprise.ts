import { MigrationInterface, QueryRunner } from "typeorm";

export class UpdateEntreprise1661413410816 implements MigrationInterface {
    name = 'UpdateEntreprise1661413410816'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "siren"`);
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "nic"`);
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "denomination_unite_legale"`);
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "activite_principale_unite_legale"`);
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "numero_voie_etablissement"`);
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "type_voie_etablissement"`);
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "libelle_voie_etablissement"`);
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "code_postal_etablissement"`);
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "libelle_commune_etablissement"`);
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "distribution_speciale_etablissement"`);
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "code_commune_etablissement"`);
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "date_creation_etablissement"`);
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "categorie_juridique"`);
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "migrated"`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "date_creation" date`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "nom" character varying`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "code_naf" character varying`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "libelle_naf" character varying`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "libelle_categorie_naf" character varying`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "adresse" character varying`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "code_commune" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "code_commune"`);
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "adresse"`);
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "libelle_categorie_naf"`);
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "libelle_naf"`);
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "code_naf"`);
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "nom"`);
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "date_creation"`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "migrated" boolean NOT NULL DEFAULT false`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "categorie_juridique" character varying`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "date_creation_etablissement" date`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "code_commune_etablissement" character varying`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "distribution_speciale_etablissement" character varying`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "libelle_commune_etablissement" character varying`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "code_postal_etablissement" character varying`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "libelle_voie_etablissement" character varying`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "type_voie_etablissement" character varying`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "numero_voie_etablissement" character varying`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "activite_principale_unite_legale" character varying`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "denomination_unite_legale" character varying`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "nic" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "siren" character varying NOT NULL`);
    }

}
