import { MigrationInterface, QueryRunner } from "typeorm";

export class motsCles1663665445532 implements MigrationInterface {
    name = 'motsCles1663665445532'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entite" ADD "mots_cles" text`);
        await queryRunner.query(`CREATE TABLE "mot_cle" ("id" SERIAL NOT NULL, "motCle" character varying NOT NULL, "territory_id" integer, CONSTRAINT "PK_4a3b8856e9f57c526818706c27e" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "mot_cle" ADD CONSTRAINT "FK_b5215ebfa132885a10d6c63d6ad" FOREIGN KEY ("territory_id") REFERENCES "territory"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entite" DROP COLUMN "mots_cles"`);
        await queryRunner.query(`ALTER TABLE "mot_cle" DROP CONSTRAINT "FK_b5215ebfa132885a10d6c63d6ad"`);
        await queryRunner.query(`DROP TABLE "mot_cle"`);
    }

}
