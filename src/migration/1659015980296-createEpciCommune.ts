import { MigrationInterface, QueryRunner } from "typeorm";

export class createEpciCommune1659015980296 implements MigrationInterface {
    name = 'createEpciCommune1659015980296'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "epci_commune" ("insee_com" character varying NOT NULL, "insee_epci" character varying NOT NULL, "insee_dep" character varying NOT NULL, "insee_reg" character varying NOT NULL, CONSTRAINT "PK_3deaaf23dc04ad676ad73dce881" PRIMARY KEY ("insee_com"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "epci_commune"`);
    }

}
