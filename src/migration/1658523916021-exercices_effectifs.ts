import { MigrationInterface, QueryRunner } from "typeorm";

export class exercicesEffectifs1658523916021 implements MigrationInterface {
    name = 'exercicesEffectifs1658523916021'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "exercice" ("id" SERIAL NOT NULL, "ca" integer NOT NULL, "date_cloture" date NOT NULL, "entreprise_id" character varying, CONSTRAINT "UNIQ_exercice_entreprise_date_cloture" UNIQUE ("date_cloture", "entreprise_id"), CONSTRAINT "PK_cccc27704e4bd3606a5068719ed" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "effectifs" character varying`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "date_effectifs" date`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "migrated" boolean NOT NULL DEFAULT false`);
        await queryRunner.query(`ALTER TABLE "exercice" ADD CONSTRAINT "FK_6440766f4b0c9edb58271cd298a" FOREIGN KEY ("entreprise_id") REFERENCES "entreprise"("siret") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "exercice" DROP CONSTRAINT "FK_6440766f4b0c9edb58271cd298a"`);
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "date_effectifs"`);
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "effectifs"`);
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "migrated"`);
        await queryRunner.query(`DROP TABLE "exercice"`);
    }

}
