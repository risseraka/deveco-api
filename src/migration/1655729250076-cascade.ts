import {MigrationInterface, QueryRunner} from "typeorm";

export class cascade1655729250076 implements MigrationInterface {
    name = 'cascade1655729250076'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "consultation" DROP CONSTRAINT "FK_28e5ea064bc941ff3fe682886f7"`);
        await queryRunner.query(`ALTER TABLE "consultation" DROP CONSTRAINT "FK_a6ad867ecdda7c4ac286b8f12b7"`);
        await queryRunner.query(`ALTER TABLE "demande" DROP CONSTRAINT "FK_a15b3bf3f0f25886412f1a59138"`);
        await queryRunner.query(`ALTER TABLE "echange" DROP CONSTRAINT "FK_45e6b16a9ece55a4d610dd11049"`);
        await queryRunner.query(`ALTER TABLE "rappel" DROP CONSTRAINT "FK_0383d1ba9fe54849cc1a1db8083"`);
        await queryRunner.query(`ALTER TABLE "contact" DROP CONSTRAINT "FK_3fe9e8fc8add9973e5745beda77"`);
        await queryRunner.query(`ALTER TABLE "consultation" ADD CONSTRAINT "FK_a6ad867ecdda7c4ac286b8f12b7" FOREIGN KEY ("deveco_id") REFERENCES "deveco"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "consultation" ADD CONSTRAINT "FK_28e5ea064bc941ff3fe682886f7" FOREIGN KEY ("fiche_id") REFERENCES "fiche"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "demande" ADD CONSTRAINT "FK_a15b3bf3f0f25886412f1a59138" FOREIGN KEY ("fiche_id") REFERENCES "fiche"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "echange" ADD CONSTRAINT "FK_45e6b16a9ece55a4d610dd11049" FOREIGN KEY ("fiche_id") REFERENCES "fiche"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "rappel" ADD CONSTRAINT "FK_0383d1ba9fe54849cc1a1db8083" FOREIGN KEY ("fiche_id") REFERENCES "fiche"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "contact" ADD CONSTRAINT "FK_3fe9e8fc8add9973e5745beda77" FOREIGN KEY ("fiche_id") REFERENCES "fiche"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "contact" DROP CONSTRAINT "FK_3fe9e8fc8add9973e5745beda77"`);
        await queryRunner.query(`ALTER TABLE "rappel" DROP CONSTRAINT "FK_0383d1ba9fe54849cc1a1db8083"`);
        await queryRunner.query(`ALTER TABLE "echange" DROP CONSTRAINT "FK_45e6b16a9ece55a4d610dd11049"`);
        await queryRunner.query(`ALTER TABLE "demande" DROP CONSTRAINT "FK_a15b3bf3f0f25886412f1a59138"`);
        await queryRunner.query(`ALTER TABLE "consultation" DROP CONSTRAINT "FK_28e5ea064bc941ff3fe682886f7"`);
        await queryRunner.query(`ALTER TABLE "consultation" DROP CONSTRAINT "FK_a6ad867ecdda7c4ac286b8f12b7"`);
        await queryRunner.query(`ALTER TABLE "contact" ADD CONSTRAINT "FK_3fe9e8fc8add9973e5745beda77" FOREIGN KEY ("fiche_id") REFERENCES "fiche"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "rappel" ADD CONSTRAINT "FK_0383d1ba9fe54849cc1a1db8083" FOREIGN KEY ("fiche_id") REFERENCES "fiche"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "echange" ADD CONSTRAINT "FK_45e6b16a9ece55a4d610dd11049" FOREIGN KEY ("fiche_id") REFERENCES "fiche"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "demande" ADD CONSTRAINT "FK_a15b3bf3f0f25886412f1a59138" FOREIGN KEY ("fiche_id") REFERENCES "fiche"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "consultation" ADD CONSTRAINT "FK_a6ad867ecdda7c4ac286b8f12b7" FOREIGN KEY ("deveco_id") REFERENCES "deveco"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "consultation" ADD CONSTRAINT "FK_28e5ea064bc941ff3fe682886f7" FOREIGN KEY ("fiche_id") REFERENCES "fiche"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
