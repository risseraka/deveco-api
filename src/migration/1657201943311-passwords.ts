import {MigrationInterface, QueryRunner} from "typeorm";

export class passwords1657201943311 implements MigrationInterface {
    name = 'passwords1657201943311'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "password" ("id" SERIAL NOT NULL, "password" character varying NOT NULL, "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "account_id" integer, CONSTRAINT "REL_ad6708d47d7045166fab9c7ea3" UNIQUE ("account_id"), CONSTRAINT "PK_cbeb55948781be9257f44febfa0" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "password" ADD CONSTRAINT "FK_ad6708d47d7045166fab9c7ea34" FOREIGN KEY ("account_id") REFERENCES "account"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "password" DROP CONSTRAINT "FK_ad6708d47d7045166fab9c7ea34"`);
        await queryRunner.query(`DROP TABLE "password"`);
    }

}
