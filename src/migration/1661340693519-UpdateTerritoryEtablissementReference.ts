import { MigrationInterface, QueryRunner } from "typeorm";

export class UpdateTerritoryEtablissementReference1661340693519 implements MigrationInterface {
    name = 'UpdateTerritoryEtablissementReference1661340693519'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "territory_etablissement_reference"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
