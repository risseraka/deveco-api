import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateEntreprise1648562652733 implements MigrationInterface {
    name = 'UpdateEntreprise1648562652733'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entreprise" ADD CONSTRAINT "FK_49d8450adf6a1fe903fb53eb99a" FOREIGN KEY ("activite_principale_unite_legale") REFERENCES "naf"("id_5") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entreprise" DROP CONSTRAINT "FK_49d8450adf6a1fe903fb53eb99a"`);
    }

}
