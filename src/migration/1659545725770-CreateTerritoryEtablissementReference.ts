import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateTerritoryEtablissementReference1659545725770 implements MigrationInterface {
    name = 'CreateTerritoryEtablissementReference1659545725770'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "territory_etablissement_reference" ("id" SERIAL NOT NULL, CONSTRAINT "PK_11e7290ea493d892783abb1a5b1" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "territory_etablissement_reference"`);
    }

}
