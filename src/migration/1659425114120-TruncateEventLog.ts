import { MigrationInterface, QueryRunner } from "typeorm"

export class TruncateEventLog1659425114120 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`TRUNCATE TABLE "event_log"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
