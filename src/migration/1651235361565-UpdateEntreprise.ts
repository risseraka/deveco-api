import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateEntreprise1651235361565 implements MigrationInterface {
    name = 'UpdateEntreprise1651235361565'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "forme_juridique" character varying`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "mandataires_sociaux" jsonb DEFAULT '[]'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "forme_juridique"`);
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "mandataires_sociaux"`);
    }

}
