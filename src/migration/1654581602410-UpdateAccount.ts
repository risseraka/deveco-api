import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateAccount1654581602410 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "deveco" ALTER COLUMN territory_id SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "deveco" ALTER COLUMN account_id SET NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "deveco" ALTER COLUMN territory_id DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "deveco" ALTER COLUMN account_id  DROP NOT NULL`);
    }

}
