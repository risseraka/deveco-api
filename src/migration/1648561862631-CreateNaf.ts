import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateNaf1648561862631 implements MigrationInterface {
    name = 'CreateNaf1648561862631'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "naf" ("id_1" character varying NOT NULL, "label_1" character varying NOT NULL, "id_2" character varying NOT NULL, "label_2" character varying NOT NULL, "id_3" character varying NOT NULL, "label_3" character varying NOT NULL, "id_4" character varying NOT NULL, "label_4" character varying NOT NULL, "id_5" character varying NOT NULL, "label_5" character varying NOT NULL, CONSTRAINT "PK_0439ce8419e76a5ecb991cbefb3" PRIMARY KEY ("id_5"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "naf"`);
    }

}
