import { MigrationInterface, QueryRunner } from "typeorm";

export class UpdateEventLog1658055901974 implements MigrationInterface {
    name = 'UpdateEventLog1658055901974'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "event_log" DROP CONSTRAINT "FK_afe31166469098b0ab784863b00"`);
        await queryRunner.query(`ALTER TABLE "event_log" DROP COLUMN "fiche_id"`);
        await queryRunner.query(`ALTER TABLE "event_log" ADD "entity_type" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "event_log" ADD "entity_id" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "event_log" ADD "action" character varying NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "event_log" DROP COLUMN "action"`);
        await queryRunner.query(`ALTER TABLE "event_log" DROP COLUMN "entity_id"`);
        await queryRunner.query(`ALTER TABLE "event_log" DROP COLUMN "entity_type"`);
        await queryRunner.query(`ALTER TABLE "event_log" ADD "fiche_id" integer`);
        await queryRunner.query(`ALTER TABLE "event_log" ADD CONSTRAINT "FK_afe31166469098b0ab784863b00" FOREIGN KEY ("fiche_id") REFERENCES "fiche"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

}
