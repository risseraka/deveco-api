import {MigrationInterface, QueryRunner} from "typeorm";

export class InitialMigration1648050386596 implements MigrationInterface {
    name = 'UpdateEntreprise1648050386596'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "date_creation_etablissement"`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "date_creation_etablissement" date`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "date_creation_etablissement"`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "date_creation_etablissement" TIMESTAMP`);
    }

}
