import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateDemande1650782255041 implements MigrationInterface {
    name = 'UpdateDemande1650782255041'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "demande" ADD "cloture" boolean`);
        await queryRunner.query(`ALTER TABLE "demande" ADD "motif" character varying`);
        await queryRunner.query(`ALTER TABLE "demande" ADD "date_cloture" date`);
        await queryRunner.query(`ALTER TABLE "rappel" ADD "cloture" boolean`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "rappel" DROP COLUMN "cloture"`);
        await queryRunner.query(`ALTER TABLE "demande" DROP COLUMN "date_cloture"`);
        await queryRunner.query(`ALTER TABLE "demande" DROP COLUMN "motif"`);
        await queryRunner.query(`ALTER TABLE "demande" DROP COLUMN "cloture"`);
    }

}
