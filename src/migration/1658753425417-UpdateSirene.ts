import { MigrationInterface, QueryRunner } from "typeorm";

export class UpdateSirene1658753425417 implements MigrationInterface {
    name = 'UpdateSirene1658753425417'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "public"."IDX_4e7fe9960ac8949d7e28724a25"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_7eef7f1f8853920ba288f62bd9"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_d96ab8ac3139e431b0b144d81f"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_39bde3c6f4efb0b5619c66bb21"`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" DROP CONSTRAINT "PK_f29668b522857283f9a5c2de356"`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "sirene_unite_legale" DROP CONSTRAINT "PK_87dd60fd0cc01d658f56f9dbf73"`);
        await queryRunner.query(`ALTER TABLE "sirene_unite_legale" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ADD CONSTRAINT "PK_4e7fe9960ac8949d7e28724a255" PRIMARY KEY ("siret")`);
        await queryRunner.query(`ALTER TABLE "sirene_unite_legale" ADD CONSTRAINT "PK_7eef7f1f8853920ba288f62bd9e" PRIMARY KEY ("siren")`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ALTER COLUMN "siren" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ADD CONSTRAINT "FK_39bde3c6f4efb0b5619c66bb214" FOREIGN KEY ("siren") REFERENCES "sirene_unite_legale"("siren") ON DELETE NO ACTION ON UPDATE NO ACTION`);

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" DROP CONSTRAINT "FK_39bde3c6f4efb0b5619c66bb214"`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ALTER COLUMN "siren" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "sirene_unite_legale" DROP CONSTRAINT "PK_7eef7f1f8853920ba288f62bd9e"`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" DROP CONSTRAINT "PK_4e7fe9960ac8949d7e28724a255"`);
        await queryRunner.query(`ALTER TABLE "sirene_unite_legale" ADD "id" SERIAL NOT NULL`);
        await queryRunner.query(`ALTER TABLE "sirene_unite_legale" ADD CONSTRAINT "PK_87dd60fd0cc01d658f56f9dbf73" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ADD "id" SERIAL NOT NULL`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ADD CONSTRAINT "PK_f29668b522857283f9a5c2de356" PRIMARY KEY ("id")`);
        await queryRunner.query(`CREATE INDEX "IDX_7eef7f1f8853920ba288f62bd9" ON "sirene_unite_legale" ("siren") `);
        await queryRunner.query(`CREATE INDEX "IDX_4e7fe9960ac8949d7e28724a25" ON "sirene_etablissement" ("siret") `);
        await queryRunner.query(`CREATE INDEX "IDX_39bde3c6f4efb0b5619c66bb21" ON "sirene_etablissement" ("siren") `);
        await queryRunner.query(`CREATE INDEX "IDX_d96ab8ac3139e431b0b144d81f" ON "sirene_etablissement" ("nic") `);
    }

}
