import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateQpv1650806005013 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE EXTENSION IF NOT EXISTS postgis`);
        await queryRunner.query(`CREATE TABLE "qp_metropoleoutremer_wgs84_epsg4326" (gid serial,
            "code_qp" varchar(8),
            "nom_qp" varchar(254),
            "commune_qp" varchar(254));`);
        await queryRunner.query(`ALTER TABLE "qp_metropoleoutremer_wgs84_epsg4326" ADD PRIMARY KEY (gid)`);
        await queryRunner.query(`SELECT AddGeometryColumn('','qp_metropoleoutremer_wgs84_epsg4326','geom','0','MULTIPOLYGON',2)`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
