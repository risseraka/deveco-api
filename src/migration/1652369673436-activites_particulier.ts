import {MigrationInterface, QueryRunner} from "typeorm";

export class activitesParticulier1652369673436 implements MigrationInterface {
    name = 'activitesParticulier1652369673436'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "particulier" ADD "activites" text`);
        await queryRunner.query(`ALTER TABLE "particulier" ADD "description" character varying`);
        await queryRunner.query(`UPDATE "particulier" SET description=particulier.activite`);
        await queryRunner.query(`ALTER TABLE "particulier" DROP COLUMN "activite"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "particulier" ADD "activite" character varying`);
        await queryRunner.query(`UPDATE "particulier" SET activite=particulier.description`);
        await queryRunner.query(`ALTER TABLE "particulier" DROP COLUMN "description"`);
        await queryRunner.query(`ALTER TABLE "particulier" DROP COLUMN "activites"`);
    }

}
