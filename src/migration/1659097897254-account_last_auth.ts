import { MigrationInterface, QueryRunner } from "typeorm";

export class accountLastAuth1659097897254 implements MigrationInterface {
    name = 'accountLastAuth1659097897254'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "account" ADD "last_auth" TIMESTAMP WITH TIME ZONE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "account" DROP COLUMN "last_auth"`);
    }

}
