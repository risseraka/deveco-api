import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateEntreprise1654522603995 implements MigrationInterface {
    name = 'UpdateEntreprise1654522603995'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "categorie_juridique" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "categorie_juridique"`);
    }

}
