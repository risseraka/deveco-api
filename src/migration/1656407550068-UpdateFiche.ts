import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateFiche1656407550068 implements MigrationInterface {
    name = 'UpdateFiche1656407550068'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`UPDATE "fiche" set "entreprise_activites" = (select "activites" from "particulier" where "fiche"."particulier_id" = "particulier"."id") WHERE "fiche"."particulier_id" is NOT NULL`);
        await queryRunner.query(`ALTER TABLE "fiche" RENAME COLUMN "entreprise_activite_autre" TO "activite_autre"`);
        await queryRunner.query(`ALTER TABLE "fiche" RENAME COLUMN "entreprise_activites" TO "activites_reelles"`);
        await queryRunner.query(`ALTER TABLE "particulier" DROP COLUMN "activites"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "particulier" ADD "activites" text`);
        await queryRunner.query(`ALTER TABLE "fiche" RENAME COLUMN "activite_autre" TO "entreprise_activite_autre"`);
        await queryRunner.query(`ALTER TABLE "fiche" RENAME COLUMN "activites_reelles" TO "entreprise_activites"`);
    }

}
