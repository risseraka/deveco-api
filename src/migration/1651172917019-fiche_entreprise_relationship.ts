import {MigrationInterface, QueryRunner} from "typeorm";

export class ficheEntrepriseRelationship1651172917019 implements MigrationInterface {
    name = 'ficheEntrepriseRelationship1651172917019'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "fiche" DROP CONSTRAINT "FK_5304848ebec35665bcadac8d00a"`);
        await queryRunner.query(`ALTER TABLE "fiche" DROP CONSTRAINT "REL_5304848ebec35665bcadac8d00"`);
        await queryRunner.query(`ALTER TABLE "fiche" ADD CONSTRAINT "FK_5304848ebec35665bcadac8d00a" FOREIGN KEY ("entreprise_id") REFERENCES "entreprise"("siret") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "fiche" DROP CONSTRAINT "FK_5304848ebec35665bcadac8d00a"`);
        await queryRunner.query(`ALTER TABLE "fiche" ADD CONSTRAINT "REL_5304848ebec35665bcadac8d00" UNIQUE ("entreprise_id")`);
        await queryRunner.query(`ALTER TABLE "fiche" ADD CONSTRAINT "FK_5304848ebec35665bcadac8d00a" FOREIGN KEY ("entreprise_id") REFERENCES "entreprise"("siret") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
