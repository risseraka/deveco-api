import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateFiche1655458368247 implements MigrationInterface {
    name = 'UpdateFiche1655458368247'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "fiche" ADD CONSTRAINT "UNIQ_fiche_territoire_entreprise" UNIQUE ("territoire_id", "entreprise_id")`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "fiche" DROP CONSTRAINT "UNIQ_fiche_territoire_entreprise"`);
    }

}
