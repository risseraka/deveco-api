import { MigrationInterface, QueryRunner } from "typeorm";

export class echangeAuteurModification1658335216266 implements MigrationInterface {
    name = 'echangeAuteurModification1658335216266'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "echange" ADD "auteur_modification_id" integer`);
        await queryRunner.query(`ALTER TABLE "echange" ALTER COLUMN "updated_at" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "echange" ADD CONSTRAINT "FK_bb74f609936b973915804240e55" FOREIGN KEY ("auteur_modification_id") REFERENCES "deveco"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "echange" DROP CONSTRAINT "FK_bb74f609936b973915804240e55"`);
        await queryRunner.query(`ALTER TABLE "echange" ALTER COLUMN "updated_at" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "echange" DROP COLUMN "auteur_modification_id"`);
    }

}
