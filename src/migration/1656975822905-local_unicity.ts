import {MigrationInterface, QueryRunner} from "typeorm";

export class localUnicity1656975822905 implements MigrationInterface {
    name = 'localUnicity1656975822905'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "local" DROP CONSTRAINT "PK_a3ed97b4eb569451a652fc8778b"`);
        await queryRunner.query(`ALTER TABLE "local" ADD CONSTRAINT "PK_d2e165069016dd2da1849c0390b" PRIMARY KEY ("id", "adresse")`);
        await queryRunner.query(`ALTER TABLE "local" DROP CONSTRAINT "PK_d2e165069016dd2da1849c0390b"`);
        await queryRunner.query(`ALTER TABLE "local" ADD CONSTRAINT "PK_0fb290786865912848b7a60dd90" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "local" ADD CONSTRAINT "titre+adresse" UNIQUE ("titre", "adresse")`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "local" DROP CONSTRAINT "titre+adresse"`);
        await queryRunner.query(`ALTER TABLE "local" DROP CONSTRAINT "PK_0fb290786865912848b7a60dd90"`);
        await queryRunner.query(`ALTER TABLE "local" ADD CONSTRAINT "PK_d2e165069016dd2da1849c0390b" PRIMARY KEY ("id", "adresse")`);
        await queryRunner.query(`ALTER TABLE "local" DROP CONSTRAINT "PK_d2e165069016dd2da1849c0390b"`);
        await queryRunner.query(`ALTER TABLE "local" ADD CONSTRAINT "PK_a3ed97b4eb569451a652fc8778b" PRIMARY KEY ("id", "titre", "adresse")`);
    }

}
