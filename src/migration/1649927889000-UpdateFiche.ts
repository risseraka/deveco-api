import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateFiche1649927889000 implements MigrationInterface {
    name = 'UpdateFiche1649927889000'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "fiche" ADD "auteur_modification_id" integer`);
        await queryRunner.query(`ALTER TABLE "fiche" ADD CONSTRAINT "FK_57ea10f9eae2d206e74485aeb94" FOREIGN KEY ("auteur_modification_id") REFERENCES "deveco"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`UPDATE "fiche" SET "auteur_modification_id" = 
                                            (
                                                SELECT d.id FROM deveco d, account a
                                                    WHERE a.id = d.account_id
                                                      AND a.email = auteur_modification
                                            )`);
        await queryRunner.query(`ALTER TABLE "fiche" DROP COLUMN "auteur_modification"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "fiche" DROP CONSTRAINT "FK_57ea10f9eae2d206e74485aeb94"`);
        await queryRunner.query(`ALTER TABLE "fiche" DROP COLUMN "auteur_modification_id"`);
        await queryRunner.query(`ALTER TABLE "fiche" ADD "auteur_modification" character varying NOT NULL`);
    }

}
