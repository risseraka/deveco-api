import {MigrationInterface, QueryRunner} from "typeorm";

export class LocalArchive1656706704975 implements MigrationInterface {
    name = 'LocalArchive1656706704975'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "local_archive" ("id" SERIAL NOT NULL, "content" jsonb NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_34c30c741daf60ad7a0569caada" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "local" ALTER COLUMN "local_types" DROP NOT NULL`);
    }

}
