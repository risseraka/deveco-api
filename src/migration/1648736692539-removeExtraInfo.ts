import {MigrationInterface, QueryRunner} from "typeorm";

export class removeExtraInfo1648736692539 implements MigrationInterface {
    name = 'removeExtraInfo1648736692539'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "fiche" DROP COLUMN "extra_info"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "fiche" ADD "extra_info" character varying`);
    }

}
