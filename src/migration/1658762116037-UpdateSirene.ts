import { MigrationInterface, QueryRunner } from "typeorm";

export class UpdateSirene1658762116037 implements MigrationInterface {
    name = 'UpdateSirene1658762116037'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" DROP CONSTRAINT "FK_39bde3c6f4efb0b5619c66bb214"`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ALTER COLUMN "siren" SET NOT NULL`);
        await queryRunner.query(`CREATE INDEX "IDX_39bde3c6f4efb0b5619c66bb21" ON "sirene_etablissement" ("siren") `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "public"."IDX_39bde3c6f4efb0b5619c66bb21"`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ALTER COLUMN "siren" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ADD CONSTRAINT "FK_39bde3c6f4efb0b5619c66bb214" FOREIGN KEY ("siren") REFERENCES "sirene_unite_legale"("siren") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
