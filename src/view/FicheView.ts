import { Demande } from '../entity/Demande';
import { Echange } from '../entity/Echange';
import { Entreprise } from '../entity/Entreprise';
import { Fiche } from '../entity/Fiche';
import { Particulier } from '../entity/Particulier';
import { Qpv } from '../entity/Qpv';
import { Rappel } from '../entity/Rappel';
import { Territory } from '../entity/Territory';
import { displayIdentity } from '../lib/utils';

type EchangeView = Echange & { auteurModif?: string };

interface FicheView {
	id?: number;

	territoire: Territory;

	auteurModification: string;

	echanges: EchangeView[];

	rappels?: Rappel[];

	demandes?: Demande[];

	contacts?: ContactView[];

	entiteType: string;

	dateModification?: Date | null;

	activitesReelles?: string[];

	localisations?: string[];

	motsCles?: string[];

	activiteAutre?: string;

	entreprise?: Entreprise;

	particulier?: Particulier;

	futureEnseigne?: string;

	zonagesPrioritaires?: string[];

	nbProprietes?: number[];
}

type ContactView = {
	nom: string;
	prenom: string;
	fonction?: string;
	email?: string;
	telephone?: string;
};

function toView(fiche: Fiche, zonagesPrioritaires?: Qpv[]): FicheView {
	return {
		id: fiche.id,
		territoire: fiche.territoire,
		auteurModification: displayIdentity(fiche.auteurModification?.account),
		echanges: fiche.echanges?.map((e) => ({
			...e,
			auteurModif: displayIdentity(e.auteurModification?.account),
		})),
		rappels: fiche.rappels,
		demandes: fiche.demandes,
		contacts: fiche.entite.contacts.map(({ fonction, particulier, id }) => ({
			fonction,
			...particulier,
			id,
		})),
		entiteType: fiche.entite.entiteType,
		dateModification: fiche.updatedAt,
		activitesReelles: fiche.entite.activitesReelles,
		localisations: fiche.entite.entrepriseLocalisations || [],
		motsCles: fiche.entite.motsCles,
		activiteAutre: fiche.entite.activiteAutre,
		entreprise: fiche.entite.entreprise
			? { ...fiche.entite.entreprise, nom: fiche.entite.entreprise?.nom?.toLocaleUpperCase() }
			: undefined,
		particulier: fiche.entite.particulier,
		futureEnseigne: fiche.entite.futureEnseigne,
		zonagesPrioritaires: zonagesPrioritaires?.map(({ nomQp }) => 'QPV ' + nomQp),
		nbProprietes: fiche.entite.proprietes?.map((e) => e.id || 0),
	};
}

export { FicheView, toView };
